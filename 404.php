<?php 
global $post;

get_header();
?>

<section class="view header-banner section has-bg-image" data-view="header-banner">
    <div class="abs-full bg-img-cover bg-image" style="background-image: url(/wp-content/uploads/2021/11/hero-background-investInProperties-landscape.jpg)"></div>  
    <div class="banner-content">
        <div class="container-lg">
            <h1 class="banner-title">Not found <strong>404</strong></h1>
        </div>
    </div>
</section>
<section class="view cta-type-1 bg-dark-blue section is-bg-dark">
    <div class="container-lg">
        <div class="cta-content">
            <div class="view title ta-center variant-section-title">
                <div class="cont-narrow">
                    <h2 class="title-tag">It seems that where you were looking for is not longer here.</h2>
                </div>
            </div>
            <div class="more-info">
                <a class="btn-t btn-large btn-soft-grey" href="<?php echo site_url(); ?>">GO BACK TO THE HOMEPAGE</a>
            </div>
        </div>
    </div><!-- .container -->
</section>
<?php
get_footer();