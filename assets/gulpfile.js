var gulp = require('gulp');
var sass = require('gulp-sass')(require('sass'));
var watch = require('gulp-watch');

gulp.task('sass:watch', function(){
    return watch('scss/**/*.scss', function(){
        gulp.src('scss/**/*.scss')
        .pipe(sass())
        .pipe(gulp.dest('css/'));
    });
});
