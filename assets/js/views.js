jQuery(function($)
{
    if(typeof hsViewsVars === 'undefined')
    {
        return;
    }

    /*
    * Helper functions
    * --------------------------------------------------
    */
    function viewExists(view)
    {
        return $.contains(document.body, view.get(0));
    };
    
    function checkViewRequiredInputs(view, requiredClass='required-error')
    {
        if(!viewExists(view)) return;
        
        view.find("input[type='text'], input[type='number'], input[type='email']").each(function(){
            
            const input = $(this);
            
            if(input.prop("required"))
            {
                if(input.val() === '')
                {
                    input.addClass(requiredClass);
                }
                else{
                    input.removeClass(requiredClass);
                };
            };
            
        });
    };
    
    function triggerViewLoadedEvents(view, triggerChildren=false)
    {
        view.each(function(){
            const _view = $(this);
            const viewName = _view.data("view");
            if(viewName)
            {
                const triggerName = "view_loaded_" + viewName;
                $(document.body).triggerHandler(triggerName, [_view]);
                
                if(triggerChildren)
                {
                    triggerViewLoadedEvents(_view.find(".view"));
                }
            }
        });
    };
    
    function replaceView(view, html, triggerLoadedEvent=true, triggerChildren=false)
    {
        view.html(html);
        const _view = view.children();
        view.replaceWith(_view);
        
        if(triggerLoadedEvent)
        {
            triggerViewLoadedEvents(_view, triggerChildren);
        }

        return _view;
    }
    
    function isIE()
    {
        const ua = navigator.userAgent;
        return (ua.indexOf("MSIE ") > -1 || ua.indexOf("Trident/") > -1);
    }    
    
    function isMobile()
    {
        return ($(window).width() < 992);
    }
    
    function parseCssNum(num)
    {
        return (num.indexOf("px") > -1) ? parseFloat(num.substring(0, num.indexOf("px"))) : num;
    }
    
    function scrollWindowTo(idHash)
    {
        const contentElem = $(idHash);
        
        if(contentElem.length)
        {
            let to = parseInt(contentElem.offset().top);

//            to = to - parseInt(parseCssNum($("#site-content").css("padding-top"))) - 50;
            to = to - parseInt($("#nav-sticky-offset").height()) - 50;

            if(to < 0)
            {
                to = 0;
            }

            if(isIE())
            {
                window.scrollTo(0, to);
            }
            else{
                window.scrollTo({
                    top: to,
                    behavior: "smooth"
                });
            }
        }
    }

    function rotateDisplay(items, timeout=1000, i=0)
    {
        if(items.length)
        {
            items.each(function(_i){
                const _item = $(this);
                if(_i === i)
                {
                    _item.addClass("active");
                }else{
                    _item.removeClass("active");
                }
            });
            i++;
            if(i > (items.length-1)) i = 0;

            setTimeout(function(){
                rotateDisplay(items, timeout, i);
            }, timeout);
        }
    }
    
    /*
    * Trigger custom window resize_x event
    * --------------------------------------------------
    */
    window.origWidth = $(window).width();
    $(window).resize(function()
    {
        if($(window).width() !== window.origWidth)
        {
            $(window).trigger("resize_x");
            window.origWidth = $(window).width();
        }
    });
    
    /*
    * Hyperlinks behavior
    * --------------------------------------------------
    */
    // $("a").on("click", function(ev){
        
    //     const a = $(this);
    //     if(a.hasClass("prevent-default"))
    //     {
    //         ev.preventDefault();
    //         return;
    //     }
        
    //     const aUrl = (window.location.href.indexOf("#") > -1) ? window.location.href.substring(0, window.location.href.indexOf("#")) : window.location.href;
    //     const aHref = a.attr("href");
        
    //     if((aHref.indexOf("http") === 0 || aHref.indexOf("/") === 0) && aHref.indexOf(aUrl) < 0)
    //     {
    //         return;
    //     }
        
    //     const contentId = (typeof aHref !== "undefined" && aHref.indexOf("#") > -1) ? aHref.substring(aHref.indexOf("#")) : "";
        
    //     if(contentId)
    //     {
    //         ev.preventDefault();
    //         window.location.hash = contentId;
    //         scrollWindowTo(contentId);
    //     }
    // });
    
    // const loadedHash = window.location.hash;
    // if(loadedHash)
    // {
    //     setTimeout(function(){
    //         scrollWindowTo(loadedHash);
    //     }, 1000);
    // }

    /*
    * .view.header-banner
    * --------------------------------------------------
    */
    function initHeaderBanner(e, view) 
    {
        if(!viewExists(view)) return;
        
        const titleRotateItems = view.find(".banner-title .rotate");
        // setTimeout(function(){
            rotateDisplay(titleRotateItems, 2000);
        // }, 4000);
    };
    $(document.body).on("view_loaded_header-banner", initHeaderBanner);

    /*
    * .view.header-banner-search
    * --------------------------------------------------
    */
    function initHeaderBannerSearch(e, view) 
    {
        if(!viewExists(view)) return;
        
        const titleRotateItems = view.find(".banner-title .rotate");
        // setTimeout(function(){
            rotateDisplay(titleRotateItems, 2000);
        // }, 4000);
    };
    $(document.body).on("view_loaded_header-banner-search", initHeaderBannerSearch);

    /*
    * .view.header
    * --------------------------------------------------
    */
    function initHeader(e, view) 
    {
        if(!viewExists(view)) return;
        
        function setFixedClass()
        {
            if($(window).scrollTop() > view.height())
            {
                view.addClass("fixed");
            }else{
                view.removeClass("fixed");
            }
        }
        setFixedClass();
        
        $(window).on("scroll", function(event){
            setFixedClass();
        });

        view.find(".nav-toggle-btn").on("click", function(){
            view.toggleClass("nav-opened");
        });
        view.find(".navbar-mobile .nav-close-area").on("click", function(){
            view.removeClass("nav-opened");
        });

        view.find(".navbar-mobile li.menu-item-has-children").on("click", function(){
            $(this).toggleClass("submenu-opened");
        });
        view.find(".navbar-mobile li.menu-item-has-children > a").on("click", function(ev){
            ev.preventDefault();
        });
    };
    $(document.body).on("view_loaded_header", initHeader);

    /*
    * .view.nice-dropdown
    * --------------------------------------------------
    */
    function initNiceDropdown(e, view) 
    {
        if(!viewExists(view)) return;

        const selectedLabel = view.find(".selected-label");
        const labelText = selectedLabel.find(".label-text");
        
        function updateSelected(optLabel)
        {
            if(!optLabel.length) return;

            const optText = optLabel.data("text");
            
            if(optText)
            {
                labelText.text(optText);
            }else{
                labelText.text(labelText.data("orig"));
            }
        }
        
        function toggleSelectedLabel(optLabel)
        {
            if(!optLabel.length) return;
            
            view.toggleClass("opened");

            if(view.hasClass("opened"))
            {
//                optLabel.focus();
            }else{
                updateSelected(optLabel);
            }
        }
        
        selectedLabel.on("click", function()
        {
            view.toggleClass("opened");
        });

        view.on("click", "ul li label", function()
        {
            toggleSelectedLabel($(this));
        });

        // Default option
        const inputActiveLabel = view.find("ul li input:checked ~ label");
        const linkActiveLabel = view.find("ul li.active label");
        const inputFirstLabel = view.find("ul li:first-child input ~ label");

        if(inputActiveLabel.length){
            updateSelected(inputActiveLabel);
        }
        else if(linkActiveLabel.length){
            updateSelected(linkActiveLabel);
        }
        else if(inputFirstLabel.length && !selectedLabel.hasClass("has-label")){
            updateSelected(inputFirstLabel);
        }
        
    };
    $(document.body).on("view_loaded_nice-dropdown", initNiceDropdown);
    
    /*
    * .view.property-filters-simple
    * --------------------------------------------------
    */
    function initPropertyFiltersSimple(e, view) 
    {
        if(!viewExists(view)) return;
        
        const form = view.find("form.filters-form");
        
        form.find(".btn-submit").on("click", function(ev)
        {
            ev.preventDefault();
            form.submit();
        });

        // Update parish options
        form.find("input[name='city']").on("change", function(){
            let reqArgs = form.serialize() + '&action=filter_properties';
            $.post(hsViewsVars.ajaxurl, reqArgs, function(resp){

                Object.keys(resp.filters_dyn_opts).forEach((key) => {
                    const filterCont = view.find(".filter."+key);
                    const filterView = filterCont.find(".view");
                    replaceView(filterView, resp.filters_dyn_opts[key], true);
                });

            }, "json");
        });
    };
    $(document.body).on("view_loaded_property-filters-simple", initPropertyFiltersSimple);

    /*
    * .view.property-filters-advanced
    * --------------------------------------------------
    */
    function initPropertyFiltersAdvanced(e, view) 
    {
        if(!viewExists(view)) return;
        
        const searchForm = view.find("form.filters-form");
        const submitBtn = searchForm.find("[type='submit']");

        searchForm.on("submit", function(ev){
            ev.preventDefault();

            const reqArgs = searchForm.serialize();
            const reqUri = window.location.pathname + "?" + reqArgs;

            window.history.pushState({
                additionalInformation: 'Updated the URL with JS'
            }, document.title, reqUri);

            submitBtn.prop("disabled", true);

            $(document.body).triggerHandler("property_list_loading", [reqArgs]);
    
            $.post(searchForm.attr("action"), reqArgs, function(resp){

                Object.keys(resp.filters_dyn_opts).forEach((key) => {
                    const filterCont = view.find(".filter."+key);
                    const filterView = filterCont.find(".view");
                    replaceView(filterView, resp.filters_dyn_opts[key], true);
                });

                // Update property items
                const listCont = $(view.data("list_cont"));
                const propertyListView = listCont.closest(".view.property-list");

                if(listCont.length)
                {
                    listCont.html(resp.property_list);
                    triggerViewLoadedEvents(listCont.find(".view"));
                }

                // Update pager
                if(typeof resp.property_list_pager !== 'undefined')
                {
                    replaceView(propertyListView.find(".view.list-pager"), resp.property_list_pager);
                }

                // Update found summary
                if(typeof resp.found_summary !== 'undefined')
                {
                    view.find(".found-summary").html(resp.found_summary);
                }

                submitBtn.prop("disabled", false);

                $(document.body).triggerHandler("property_list_loaded", [resp, propertyListView]);

            }, "json");
        });

        // view.find(".change-submit").on("change", function(){
        searchForm.on("change", ".change-submit", function(){
            searchForm.submit();
        });
        //Default form submit
        searchForm.submit();

        const pageInput = searchForm.find("input[name='paged']");
        $(document.body).on("list_pager_updated", function(ev, args){
            pageInput.val(args.page);
            pageInput.change();
        });

        const advancedBlock = view.find(".filters-block.advanced");
        view.find("button.btn-show-advanced").on("click", function(){
            // advancedBlock.removeClass("d-none");
            view.addClass("advanced-opened");
            setTimeout(() => { 
                view.addClass("advanced-visible"); 
            }, 300);
        });
        view.find("button.btn-hide-advanced").on("click", function(){
            // advancedBlock.addClass("d-none");
            view.removeClass("advanced-opened");
            view.removeClass("advanced-visible");
        });
    };
    $(document.body).on("view_loaded_property-filters-advanced", initPropertyFiltersAdvanced);

    /*
    * .view.property-list
    * --------------------------------------------------
    */
   
    function initPropertyList(e, view) 
    {
        if(!viewExists(view)) return;

        const renderMap = function(listView)
        {
            if(!(typeof google !== 'undefined' && typeof google.maps !== 'undefined'))
            {
                return;
            }

            const mapData = {
                cont: listView.data("map_cont"),
                zoom: parseInt(listView.data("map_zoom"))
            };
            const mapCont = mapData.cont ? $(mapData.cont) : null;

            if(!(mapCont && mapCont.length))
            {
                return;
            }

            // Clear old markers

            mapCont.html("");
            // if(mapMarkers.length)
            // {
            //     for(let i=0; i<mapMarkers.length; i++){
            //         mapMarkers[i].setMap(null);
            //     };
            //     mapMarkers = [];
            // }

            const propertyItems = listView.find(".view.property-item");
            const markerItems = [];
            propertyItems.each(function(){
                const porpertyItem = $(this);
                markerItems.push({
                    ref: porpertyItem.data("ref"),
                    // price: porpertyItem.data("price"),
                    // location: porpertyItem.data("location"),
                    lat: porpertyItem.data("lat"),
                    lon: porpertyItem.data("lon"),
                    infoHtml: porpertyItem.find(".view.property-item-map-info").get(0).outerHTML
                });
            });

            if(!markerItems.length)
            {
                return;
            }

            const mapArgs = {
                // center: {
                //     lat: 40.42846209137688, 
                //     lng: -3.7035798542703193
                // },
                zoom: 10,
                styles: [
                    {
                        featureType: "poi",
                        stylers: [{ visibility: "off" }]
                    },
                    {
                        featureType: "all",
                        elementType: "all",
                        stylers: [{ saturation: -100 }]
                    }                    
                ]
            };
            if(mapData.zoom)
            {
                mapArgs.zoom = mapData.zoom;
            }

            const map = new google.maps.Map(mapCont.get(0), mapArgs);
            const mapBounds = new google.maps.LatLngBounds();
            const mapMarkers = [];
            const mapWindows = [];

            // Add new markers
            markerItems.forEach((markerItem, i) => {

                if(!(markerItem.lat && markerItem.lon))
                {
                    return;
                }

                const markerArgs = {
                    title: markerItem.ref,
                    position: {
                        lat: markerItem.lat,
                        lng: markerItem.lon
                    },
                    map: map
                };
                if(hsViewsVars.googlemaps_marker_image)
                {
                    markerArgs.icon = hsViewsVars.googlemaps_marker_image;
                }
                mapMarkers[i] = new google.maps.Marker(markerArgs)
                mapWindows[i] = new google.maps.InfoWindow({
                    content: markerItem.infoHtml
                });
                mapMarkers[i].addListener("click", function(){
                    mapWindows[i].open({
                        anchor: mapMarkers[i],
                        map: map,
                        shouldFocus: false
                    });
                });
                // mapMarkers[i].addListener("mouseout", function(){
                //     mapWindows[i].close();
                // });

                mapBounds.extend(mapMarkers[i].position);
            });

            propertyItems.each(function(i){

                const propertyItem = $(this);
                const propertyCenter = {
                    lat: propertyItem.data("lat"),
                    lng: propertyItem.data("lon"),
                };
                if(propertyCenter.lat && propertyCenter.lng)
                {
                    propertyItem.on("mouseover", function(){
                        // map.setCenter(propertyCenter);
                        map.panTo(propertyCenter);

                        mapWindows[i].open({
                            anchor: mapMarkers[i],
                            map: map,
                            shouldFocus: false
                        });
                    });
                    propertyItem.on("mouseout", function(){
                        mapWindows[i].close();
                    });
                }
            });

            map.fitBounds(mapBounds);
        }

        $(document.body).one("googlemaps-loaded", function(){
            renderMap(view);
        });

        $(document.body).on("property_list_loading", function(){
            view.addClass("loading");
        });

        $(document.body).on("property_list_loaded", function(ev, resp, propertyListView){
            view.removeClass("loading");
            renderMap(propertyListView);
        });

        view.find("button.btn-map-expand").on("click", function(){
            view.toggleClass("map-expanded");
        });
    }
    $(document.body).on("view_loaded_property-list", initPropertyList);

    /*
    * .view.property-filter-price-slider
    * --------------------------------------------------
    */
    function initPropertyFilterPriceSlider(e, view) 
    {
        if(!viewExists(view)) return;
        
        if(typeof $.fn.ionRangeSlider !== 'undefined')
        {
            view.find("input.range-slider").each(function(){

                const input = $(this);
                const form = input.closest("form");
                const inputVal = input.val();

                let args = {
                    // type: "double",
                    // min: input.data("min"),
                    // max: input.data("max"),
                    // grid: false,
                    input_values_separator: '-',
                    onFinish: function(){
                        form.submit()
                    }
                };
                // const inputValRange = (inputVal.indexOf("-") > -1) ? inputVal.split("-") : null;
                // if(inputValRange)
                // {
                //     args.from = parseInt(inputValRange[0]);
                //     args.to = parseInt(inputValRange[1]);
                // }
                input.ionRangeSlider(args);
            });
        }
    };
    $(document.body).on("view_loaded_property-filter-price-slider", initPropertyFilterPriceSlider);

    /*
    * .view.list-pager
    * --------------------------------------------------
    */
    function initListPager(e, view) 
    {
        if(!viewExists(view)) return;

        view.find("li.page a").on("click", function(ev){
            ev.preventDefault();

            const aElem = $(this);
            const aPage = parseInt(aElem.data("page"));

            $(document.body).triggerHandler("list_pager_updated", {
                page: aPage
            });
        });
    };
    $(document.body).on("view_loaded_list-pager", initListPager);
    
    /*
    * .view.content-tabs
    * --------------------------------------------------
    */
    function initContentTabs(e, view) 
    {
        if(!viewExists(view)) return;
        
        const tabItems = view.find("ul.tabs li.tab");
        const tabContents = view.find(".tabs-content .tab-content");
        
        tabItems.on("click", function(){
            
            const tabItem = $(this);
            const tabIndex = tabItem.index();
            
            tabItems.filter(".active").removeClass("active");
            tabItem.addClass("active");
            
            tabContents.filter(".active").removeClass("active visible");
            const tabContent = tabContents.eq(tabIndex);
            tabContent.addClass("active");
            setTimeout(function(){
                tabContent.addClass("visible");
            }, 200);
        });
    };
    $(document.body).on("view_loaded_content-tabs", initContentTabs);
    
    /*
    * .view.banner-slider
    * --------------------------------------------------
    */
    function initBannerSlider(e, view) 
    {
        if(!viewExists(view)) return;
        
        let swiperConfig = {
            loop: true
        };
        if(view.data("show_pagination"))
        {
            swiperConfig.pagination = {
                el: view.find(".banner-slider-cont > .swiper-pagination").get(0)
            };
        }
        if(view.data("show_nav"))
        {
            swiperConfig.allowSlideNext = true;
            swiperConfig.allowSlidePrev = true;
            swiperConfig.navigation = {
                nextEl: view.find(".banner-slider-cont > .swiper-button-next").get(0),
                prevEl: view.find(".banner-slider-cont > .swiper-button-prev").get(0)
            };
        }
        
        const swiperCont = view.find(".banner-slider-cont > .swiper").get(0);
        if(swiperCont)
        {
            const swiper = new Swiper(swiperCont, swiperConfig);
        }
    };
    $(document.body).on("view_loaded_banner-slider", initBannerSlider);
    
    /*
    * .view.property-slider
    * --------------------------------------------------
    */
    function initPropertySlider(e, view) 
    {
        if(!viewExists(view)) return;
        
        let swiperConfig = {
            loop: true,
            breakpoints: {
                992: {
                    slidesPerView: 2
                }
            }
        };
        if(view.data("show_pagination"))
        {
            swiperConfig.pagination = {
                el: view.find(".property-slider-cont > .swiper-pagination").get(0)
            };
        }
        if(view.data("show_nav"))
        {
            swiperConfig.allowSlideNext = true;
            swiperConfig.allowSlidePrev = true;
            swiperConfig.navigation = {
                nextEl: view.find(".property-slider-cont > .swiper-button-next").get(0),
                prevEl: view.find(".property-slider-cont > .swiper-button-prev").get(0)
            };
        }
        
        const swiperCont = view.find(".property-slider-cont > .swiper").get(0);
        if(swiperCont)
        {
            const swiper = new Swiper(swiperCont, swiperConfig);
        }
    };
    $(document.body).on("view_loaded_property-slider", initPropertySlider);
    
    /*
    * .view.property-item
    * --------------------------------------------------
    */
    function initPropertyItem(e, view) 
    {
        if(!viewExists(view)) return;
        
        let swiperConfig = {
            loop: false,
            navigation: {
                nextEl: view.find(".swiper-button-next").get(0),
                prevEl: view.find(".swiper-button-prev").get(0)
            }
        };
        
        const swiperCont = view.find(".swiper").get(0);
        if(swiperCont)
        {
            const swiper = new Swiper(swiperCont, swiperConfig);
        }
    };
    $(document.body).on("view_loaded_property-item", initPropertyItem);

    /*
    * .view.property-nav
    * --------------------------------------------------
    */
    function initPropertyNav(e, view) 
    {
        if(!viewExists(view)) return;

        const fixedCont = view.find(".fixed-cont");

        function setFixedClass()
        {
            const fixedPoint = isMobile() ? 100 : 400;

            if($(window).scrollTop() > fixedPoint)
            {
                view.height(fixedCont.outerHeight());
                view.addClass("fixed");
            }else{
                view.height("auto");
                view.removeClass("fixed");
            }
        }
        setFixedClass();

        $(window).on("scroll", function(event){
            setFixedClass();
        });

        const navCont = view.find(".nav-links");
        const navLinks = navCont.find("li a");
        let anConts = null;

        navLinks.each(function(){
            const link = $(this);
            const anch = link.attr("href");
            if(anch.indexOf("#") === 0)
            {
                if(anConts === null)
                {
                    anConts = $(anch);
                }
                else{
                    anConts = anConts.add(anch);
                }
            }
        });

        function setActiveLinks()
        {
            if(!(navLinks.length && anConts && anConts.length))
            {
                return;
            }

            const navContY = navCont.offset().top;

            navLinks.each(function(){
                const link = $(this);
                const anch = link.attr("href");
                anConts.each(function(){

                    const anCont = $(this);
                    const anId = "#"+anCont.attr("id");

                    if(anch === anId)
                    {
                        const anContY1 = anCont.offset().top;
                        const anContY2 = anContY1 + anCont.outerHeight();
                        if(navContY > anContY1 && navContY < anContY2)
                        {
                            link.addClass("active");
                        }
                        else{
                            link.removeClass("active");
                        }
                    }
                });
            });
        }
        setActiveLinks();
        
        $(window).on("scroll", function(event){
            setActiveLinks();
        });
    };
    $(document.body).on("view_loaded_property-nav", initPropertyNav);

    /*
    * .view.property-contact
    * --------------------------------------------------
    */
    function initPropertyContact(e, view) 
    {
        if(!viewExists(view)) return;
        
        let swiperConfig = {
            loop: false,
            autoHeight: true,
            navigation: false,
            pagination: false
        };

        const swiperCont = view.find(".swiper");

        if(!swiperCont)
        {
            return;
        }

        const swiper = new Swiper(swiperCont.get(0), swiperConfig);

        const slideItems = view.find(".slide-item");

        view.find(".goto-slide").on("click", function(){

            const btn = $(this);
            const targetSlide = btn.data("slide");

            const slideIndex = slideItems.filter("."+targetSlide).index();

            swiper.slideTo(slideIndex);
        });
    };
    $(document.body).on("view_loaded_property-contact", initPropertyContact);

    /*
    * .view.property-details
    * --------------------------------------------------
    */
    function initPropertyGallery(e, view) 
    {
        if(!viewExists(view)) return;

        view.find(".gallery-thumbnail").on("click", function(){
            const btn = $(this);
            view.find(".view.gallery-modal").triggerHandler("show_modal", [btn.data("i")]);
        });
    };
    $(document.body).on("view_loaded_property-gallery", initPropertyGallery);

    /*
    * .view.project-item
    * --------------------------------------------------
    */
    function initProjectItem(e, view) 
    {
        if(!viewExists(view)) return;

        view.find(".gallery-btn").on("click", function(){
            view.find(".view.gallery-modal").triggerHandler("show_modal");
        });
    }
    $(document.body).on("view_loaded_project-item", initProjectItem);

    /*
    * .view.gallery-modal
    * --------------------------------------------------
    */
    function initGalleryModal(e, view) 
    {
        if(!viewExists(view)) return;

        if(!(typeof bootstrap !== 'undefined' && typeof bootstrap.Modal !== 'undefined' && typeof Swiper !== 'undefined'))
        {
            return;
        }
        
        const modalElem = view.find(".modal");

        if(!modalElem.length)
        {
            return;
        }

        const modal = new bootstrap.Modal(modalElem);

        const swiperContElem = view.find(".gallery-slider-cont");
        const swiperElem = swiperContElem.find(".swiper.gallery-slider");

        var swiper;

        let imagesTotal = 0;
        let imagesLoaded = 0;

        let slideIndex = 0;

        const initGallerySlider = function()
        {
            $(document.body).one("hs_property_gallery_images_loaded", function(){

                swiperContElem.removeClass("loading");

                if(typeof swiper === 'undefined')
                {
                    swiperElem.find(".gallery-image").each(function(){
                        const galleryImage = $(this);
                        galleryImage.find("img").attr("src", galleryImage.data("src"));
                    });

                    swiper = new Swiper(swiperElem.get(0), {
                        navigation: {
                            nextEl: '.swiper-button-next',
                            prevEl: '.swiper-button-prev'
                        }            
                    });
                }
                else{
                    swiper.update();
                }

                // if(slideIndex)
                // {
                    swiper.slideTo(slideIndex);
                // }
            });

            preloadImages(swiperElem.find(".gallery-image"));
        };

        const preloadImages = function(containers)
        {
            imagesTotal = containers.length;

            if(imagesLoaded && imagesLoaded === imagesTotal)
            {
                $(document.body).triggerHandler("hs_property_gallery_images_loaded");
                return;
            }

            containers.each(function(){

                const cont = $(this);
                const imgSrc = cont.data("src");

                // const img = new Image();
                const img = document.createElement("img");
                img.src = imgSrc;
                // document.body.append(img);
                img.onload = function(){
                    imagesLoaded++;
                    if(imagesLoaded === imagesTotal)
                    {
                        $(document.body).triggerHandler("hs_property_gallery_images_loaded");
                    }
                };
            });
        }

        modalElem.on('shown.bs.modal', function(ev){
            swiperContElem.addClass("loading");
            initGallerySlider();
        });

        view.on("show_modal", function(ev, index){
            slideIndex = parseInt(index);
            modal.show();
        });
    };
    $(document.body).on("view_loaded_gallery-modal", initGalleryModal);

    /*
    * .view.property-map
    * --------------------------------------------------
    */
    function initPropertyMap(e, view) 
    {
        if(!viewExists(view)) return;

        const renderMap = function()
        {
            if(!(typeof google !== 'undefined' && typeof google.maps !== 'undefined'))
            {
                return;
            }

            const mapCont = view.find(".map").get(0);

            if(!mapCont)
            {
                return;
            }

            const viewData = {
                lat:  view.data("center_lat"),
                lng: view.data("center_lon"),
                zoom: parseInt(view.data("zoom"))
            };
            const center = {
                lat: viewData.lat,
                lng: viewData.lng
            };
            if(!(center.lat && center.lng))
            {
                return;
            }

            const mapArgs = {
                center: center,
                zoom: 10,
                styles: [
                    {
                        featureType: "poi",
                        stylers: [{ visibility: "off" }]
                    },
                    {
                        featureType: "all",
                        elementType: "all",
                        stylers: [{ saturation: -100 }]
                    }                    
                ]
            };
            if(viewData.zoom)
            {
                mapArgs.zoom = viewData.zoom;
            }
            
            const map = new google.maps.Map(mapCont, mapArgs);

            const markerArgs = {
                position: center,
                map: map
            };
            if(hsViewsVars.googlemaps_marker_image)
            {
                markerArgs.icon = hsViewsVars.googlemaps_marker_image;
            }
            const marker = new google.maps.Marker(markerArgs);
        }

        $(document.body).one("googlemaps-loaded", function()
        {
            renderMap();
        });
    };
    $(document.body).on("view_loaded_property-map", initPropertyMap);

    /*
    * .view.agent-card
    * --------------------------------------------------
    */
    function initAgentCard(e, view) 
    {
        if(!viewExists(view)) return;

        if(!(typeof bootstrap !== 'undefined' && typeof bootstrap.Modal !== 'undefined'))
        {
            return;
        }
        
        const btnContactAgent = view.find(".btn-contact-agent");
        const modalElem = $(btnContactAgent.data("contact_modal"));
        const modal = new bootstrap.Modal(modalElem);

        btnContactAgent.on("click", function(ev){
            ev.preventDefault();
            modal.show();
        });
    };
    $(document.body).on("view_loaded_agent-card", initAgentCard);

    /*
    * Load maps script
    * --------------------------------------------------
    */
    if(typeof hsViewsVars.googlemaps_api_key !== 'undefined' && hsViewsVars.googlemaps_api_key)
    {
        window.initGoogleMaps = function()
        {
            $(document.body).triggerHandler("googlemaps-loaded");
        }

        const mapsScriptElem = document.createElement("script");
        mapsScriptElem.src = "https://maps.googleapis.com/maps/api/js?key="+hsViewsVars.googlemaps_api_key+"&callback=initGoogleMaps";
        mapsScriptElem.async = true;
        document.head.appendChild(mapsScriptElem);
    }

    /*
    * .footer .widget_nav_menu
    * --------------------------------------------------
    */
    $(document.body).on("click", ".footer .widget_nav_menu .widget-title", function(){
        
        const widgetCont = $(this).closest(".widget_nav_menu");
        widgetCont.toggleClass("opened");
    });

    /*
    * Init views on page load
    * --------------------------------------------------
    */
    $(".view").each(function(){
        triggerViewLoadedEvents($(this));
    });
});