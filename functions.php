<?php

define('HS_MAINTENANCE', false);
define('HS_DIR', dirname(__FILE__));
define('HS_INDEX', get_stylesheet_directory_uri());
define('HS_VERSION', wp_get_theme()->get('Version'));
define('HS_NAME', wp_get_theme()->get('Name'));

require HS_DIR . '/vendor/autoload.php';
require HS_DIR . '/src/setup.php';

add_action('after_setup_theme', function(){

    $deps = new \HS\Deps([
        'advanced-custom-fields-pro/acf.php',
        'homeselect/plugin.php'
    ]);
    
    if($deps->check())
    {
        require HS_DIR . '/src/classes/load.php';

        require HS_DIR . '/src/utils.php';
        require HS_DIR . '/src/scripts.php';
        require HS_DIR . '/src/blocks.php';
        require HS_DIR . '/src/widgets.php';
        require HS_DIR . '/src/maintenance.php';
        require HS_DIR . '/src/acf-blocks.php';
        require HS_DIR . '/src/acf-fields-config.php';
        // require HS_DIR . '/src/acf-theme-options.php';
        require HS_DIR . '/src/debug.php';
        require HS_DIR . '/src/midrocket.php';
    }
    
}, 5);
