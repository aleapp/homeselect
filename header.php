<!doctype html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo('charset'); ?>" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <link rel="shortcut icon" href="<?php echo HS_INDEX . '/assets/images/favicon.ico' ?>" type="image/x-icon"/>
    
    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
    
    <?php wp_body_open(); ?>
    
    <?php 
    $transparent_header = !(is_singular(['property']) || is_page_template('page-properties.php'));
    wpseed_print_view('header', [
        'html_class' => $transparent_header ? 'transparent' : ''
    ]); 
    ?>

    <main id="site-main" class="site-main" role="main">
