<?php 
/*
*
* Template Name: Properties list
*/

get_header();

wpseed_print_view('property-list', [
    'map_cont' => '.map-cont .map',
    'top_level' => true,
    'show_list_pager' => true,
    'show_filters' => true,
    'show_map' => true,
    'set_items' => false,
    // 'no_found_text' => __('Loading...', 'hs'),
    'no_found_text' => '&nbsp;'
]);

wpseed_print_view('cta-help', [
    'title' => __('Still need help? Get in touch with us.', 'hs'),
    'more_page' => \HS\Utils::getThemeOption('general_contact_page'),
    'bg_image_url' => HS_INDEX . '/assets/images/banner-contact.jpg',
    'top_level' => true
]);

get_footer();