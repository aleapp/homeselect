<?php 
global $post;

get_header();

echo apply_filters('the_content', $post->post_content);

/*wpseed_print_view('page-header-type-1', [
    'icon_id' => get_field('page_header_type_1_icon', $post->ID),
    'description' => get_field('page_header_type_1_description', $post->ID)
]);*/

get_footer();