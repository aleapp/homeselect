<?php 
global $wp_query;

get_header();
    
    wpseed_print_view('search-form', [
        'top_level' => true,
        'bg_color' => 'bg-green-dark'
    ]);

    wpseed_print_view('search-results', [
        'top_level' => true,
        'posts' => isset($wp_query->posts) ? $wp_query->posts : [],
        'posts_found' => $wp_query->found_posts
    ]);
    
get_footer();