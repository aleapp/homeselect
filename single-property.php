<?php 
global $post;

$property = new \HSP\Type\Property($post);

get_header();
?>

<?php 
wpseed_print_view('property-nav', [
    'back_link' => \HS\Utils_Property::getPropertyListBackLink(),
    'back_label' => __('Back to list', 'hs'),
    'items' => [
        [
            'link' => '#property-summary',
            'label' => __('About the property', 'hs')
        ],
        [
            'link' => '#property-specs',
            'label' => __('Features', 'hs')
        ],
        [
            'link' => '#property-gallery',
            'label' => __('Photos', 'hs')
        ],
        [
            'link' => '#',
            'label' => __('Neighborhood', 'hs')
        ]
    ]
]);
?>

<div class="row g-0">
    <div class="col-lg-9">
        <div class="property-content">

            <?php 
            $feat_image = $property->getFeaturedImageSrc('Original');
            if($feat_image):
                wpseed_print_view('image-banner', [
                    'image' => $feat_image,
                    'image_ratio' => '150-100'
                ]);
            endif;

            wpseed_print_view('property-summary', [
                'id' => 'property-summary',
                'property' => $property
            ]);

            wpseed_print_view('property-contact', [
                'property' => $property,
                // 'agent' => \HSP\Utils_Agent::getRandomAgent($property->getCity(), new \HSP\Type\Agent()),
                'agent' => \HSP\Utils_Agent::getCityAgent($property->getCity(), new \HSP\Type\Agent()),
                'html_class' => 'd-lg-none'
            ]);
    
            wpseed_print_view('property-details', [
                'property' => $property,
                'cont_id_specs' => 'property-specs',
                'cont_id_amenities' => 'property-amenities',
                'cont_id_gallery' => 'property-gallery'
            ]);
            ?>

        </div>
    </div>
    <div class="col-lg-3 property-contact-col">
        <?php 
        wpseed_print_view('property-contact', [
            'property' => $property,
            // 'agent' => \HSP\Utils_Agent::getRandomAgent($property->getCity(), new \HSP\Type\Agent()),
            'agent' => \HSP\Utils_Agent::getCityAgent($property->getCity(), new \HSP\Type\Agent()),
            'html_class' => 'd-none d-lg-block'
        ]);
        ?>
    </div>
</div>

<?php 
wpseed_print_view('property-map', [
    'property' => $property
]);
?>

<?php
get_footer();