<?php

/*
* Register Gutenberg blocks with ACF
* ----------------------------------------
*/

add_action('acf/init', 'hs_register_blocks');

function hs_register_blocks()
{
    if(function_exists('acf_register_block'))
    {
        /*
        * header-banner
        * ------------------------------
        */
        acf_register_block([
            'name'				=> 'header-banner',
            'title'				=> __('HS Header Banner', 'hs'),
            //'description'		=> __('Block description.', 'hs'),
            'render_callback'	=> 'hs_render_block_view',
            'category'			=> 'hs-blocks',
            //'icon'				=> 'admin-comments'
            //'keywords'			=> [''],
            'view_args' => [],
            'mode' => 'auto'
        ]);
        
        /*
        * header-banner-search
        * ------------------------------
        */
        acf_register_block([
            'name'				=> 'header-banner-search',
            'title'				=> __('HS Header Banner Search', 'hs'),
            //'description'		=> __('Block description.', 'hs'),
            'render_callback'	=> 'hs_render_block_view',
            'category'			=> 'hs-blocks',
            //'icon'				=> 'admin-comments'
            //'keywords'			=> [''],
            'view_args' => [],
            'mode' => 'auto'
        ]);

        /*
        * image-banner
        * ------------------------------
        */
        acf_register_block([
            'name'				=> 'image-banner',
            'title'				=> __('HS Image Banner', 'hs'),
            //'description'		=> __('Block description.', 'hs'),
            'render_callback'	=> 'hs_render_block_view',
            'category'			=> 'hs-blocks',
            //'icon'				=> 'admin-comments'
            //'keywords'			=> [''],
            'view_args' => [],
            'mode' => 'auto'
        ]);
        
        /*
        * cta-type-1
        * ------------------------------
        */
        acf_register_block([
            'name'				=> 'cta-type-1',
            'title'				=> __('HS CTA Type 1', 'hs'),
            //'description'		=> __('Block description.', 'hs'),
            'render_callback'	=> 'hs_render_block_view',
            'category'			=> 'hs-blocks',
            //'icon'				=> 'admin-comments'
            //'keywords'			=> [''],
            'view_args' => [],
            'mode' => 'auto'
        ]);

        /*
        * cta-help
        * ------------------------------
        */
        acf_register_block([
            'name'				=> 'cta-help',
            'title'				=> __('HS CTA Help', 'hs'),
            //'description'		=> __('Block description.', 'hs'),
            'render_callback'	=> 'hs_render_block_view',
            'category'			=> 'hs-blocks',
            //'icon'				=> 'admin-comments'
            //'keywords'			=> [''],
            'view_args' => [],
            'mode' => 'auto'
        ]);

        /*
        * cta-quote
        * ------------------------------
        */
        acf_register_block([
            'name'				=> 'cta-quote',
            'title'				=> __('HS CTA Quote', 'hs'),
            //'description'		=> __('Block description.', 'hs'),
            'render_callback'	=> 'hs_render_block_view',
            'category'			=> 'hs-blocks',
            //'icon'				=> 'admin-comments'
            //'keywords'			=> [''],
            'view_args' => [],
            'mode' => 'auto'
        ]);
        
        /*
        * cta-type-2
        * ------------------------------
        */
        acf_register_block([
            'name'				=> 'cta-type-2',
            'title'				=> __('HS CTA Type 2', 'hs'),
            //'description'		=> __('Block description.', 'hs'),
            'render_callback'	=> 'hs_render_block_view',
            'category'			=> 'hs-blocks',
            //'icon'				=> 'admin-comments'
            //'keywords'			=> [''],
            'view_args' => [],
            'mode' => 'auto'
        ]);

        /*
        * cta-type-3
        * ------------------------------
        */
        acf_register_block([
            'name'				=> 'cta-type-3',
            'title'				=> __('HS CTA Type 3', 'hs'),
            //'description'		=> __('Block description.', 'hs'),
            'render_callback'	=> 'hs_render_block_view',
            'category'			=> 'hs-blocks',
            //'icon'				=> 'admin-comments'
            //'keywords'			=> [''],
            'view_args' => [],
            'mode' => 'auto'
        ]);
        
        /*
        * cta-meet
        * ------------------------------
        */
        acf_register_block([
            'name'				=> 'cta-meet',
            'title'				=> __('HS CTA Meet', 'hs'),
            //'description'		=> __('Block description.', 'hs'),
            'render_callback'	=> 'hs_render_block_view',
            'category'			=> 'hs-blocks',
            //'icon'				=> 'admin-comments'
            //'keywords'			=> [''],
            'view_args' => [],
            'mode' => 'auto'
        ]);

        /*
        * cta-button
        * ------------------------------
        */
        acf_register_block([
            'name'				=> 'cta-button',
            'title'				=> __('HS CTA Button', 'hs'),
            //'description'		=> __('Block description.', 'hs'),
            'render_callback'	=> 'hs_render_block_view',
            'category'			=> 'hs-blocks',
            //'icon'				=> 'admin-comments'
            //'keywords'			=> [''],
            'view_args' => [],
            'mode' => 'auto'
        ]);
        
        /*
        * our-services
        * ------------------------------
        */
        acf_register_block([
            'name'				=> 'our-services',
            'title'				=> __('HS Our services', 'hs'),
            //'description'		=> __('Block description.', 'hs'),
            'render_callback'	=> 'hs_render_block_view',
            'category'			=> 'hs-blocks',
            //'icon'				=> 'admin-comments'
            //'keywords'			=> [''],
            'view_args' => [],
            'mode' => 'auto'
        ]);
        
        /*
        * title
        * ------------------------------
        */
        acf_register_block([
            'name'				=> 'title',
            'title'				=> __('HS Title', 'hs'),
            //'description'		=> __('Block description.', 'hs'),
            'render_callback'	=> 'hs_render_block_view',
            'category'			=> 'hs-blocks',
            //'icon'				=> 'admin-comments'
            //'keywords'			=> [''],
            'view_args' => [],
            'mode' => 'auto'
        ]);
        
        /*
        * widget-title
        * ------------------------------
        */
        acf_register_block([
            'name'				=> 'widget-title',
            'title'				=> __('HS Widget Title', 'hs'),
            //'description'		=> __('Block description.', 'hs'),
            'render_callback'	=> 'hs_render_block_view',
            'category'			=> 'hs-blocks',
            //'icon'				=> 'admin-comments'
            //'keywords'			=> [''],
            'view_args' => [],
            'mode' => 'auto'
        ]);
        
        /*
        * content-tabs
        * ------------------------------
        */
        acf_register_block([
            'name'				=> 'content-tabs',
            'title'				=> __('HS Content tabs', 'hs'),
            //'description'		=> __('Block description.', 'hs'),
            'render_callback'	=> 'hs_render_block_view',
            'category'			=> 'hs-blocks',
            //'icon'				=> 'admin-comments'
            //'keywords'			=> [''],
            'view_args' => [],
            'mode' => 'auto'
        ]);
        
        /*
        * banner-slider
        * ------------------------------
        */
        acf_register_block([
            'name'				=> 'banner-slider',
            'title'				=> __('HS Banner slider', 'hs'),
            //'description'		=> __('Block description.', 'hs'),
            'render_callback'	=> 'hs_render_block_view',
            'category'			=> 'hs-blocks',
            //'icon'				=> 'admin-comments'
            //'keywords'			=> [''],
            'view_args' => [],
            'mode' => 'auto'
        ]);
        
        /*
        * property-slider
        * ------------------------------
        */
        acf_register_block([
            'name'				=> 'property-slider',
            'title'				=> __('HS Property slider', 'hs'),
            //'description'		=> __('Block description.', 'hs'),
            'render_callback'	=> 'hs_render_block_view',
            'category'			=> 'hs-blocks',
            //'icon'				=> 'admin-comments'
            //'keywords'			=> [''],
            'view_args' => [],
            'mode' => 'auto'
        ]);

        /*
        * project-list
        * ------------------------------
        */
        acf_register_block([
            'name'				=> 'project-list',
            'title'				=> __('HS Project list', 'hs'),
            //'description'		=> __('Block description.', 'hs'),
            'render_callback'	=> 'hs_render_block_view',
            'category'			=> 'hs-blocks',
            //'icon'				=> 'admin-comments'
            //'keywords'			=> [''],
            'view_args' => [],
            'mode' => 'auto'
        ]);
        
        /*
        * service-type-1-group
        * ------------------------------
        */
        acf_register_block([
            'name'				=> 'service-type-1-group',
            'title'				=> __('HS Service Type 1 Group', 'hs'),
            //'description'		=> __('Block description.', 'hs'),
            'render_callback'	=> 'hs_render_block_view',
            'category'			=> 'hs-blocks',
            //'icon'				=> 'admin-comments'
            //'keywords'			=> [''],
            'view_args' => [],
            'mode' => 'auto'
        ]);
        
        /*
        * service-type-2
        * ------------------------------
        */
        acf_register_block([
            'name'				=> 'service-type-2',
            'title'				=> __('HS Service Type 2', 'hs'),
            //'description'		=> __('Block description.', 'hs'),
            'render_callback'	=> 'hs_render_block_view',
            'category'			=> 'hs-blocks',
            //'icon'				=> 'admin-comments'
            //'keywords'			=> [''],
            'view_args' => [],
            'mode' => 'auto'
        ]);

        /*
        * service-type-3-group
        * ------------------------------
        */
        acf_register_block([
            'name'				=> 'service-type-3-group',
            'title'				=> __('HS Service Type 3 Group', 'hs'),
            //'description'		=> __('Block description.', 'hs'),
            'render_callback'	=> 'hs_render_block_view',
            'category'			=> 'hs-blocks',
            //'icon'				=> 'admin-comments'
            //'keywords'			=> [''],
            'view_args' => [],
            'mode' => 'auto'
        ]);

        /*
        * service-type-4-group
        * ------------------------------
        */
        acf_register_block([
            'name'				=> 'service-type-4-group',
            'title'				=> __('HS Service Type 4 Group', 'hs'),
            //'description'		=> __('Block description.', 'hs'),
            'render_callback'	=> 'hs_render_block_view',
            'category'			=> 'hs-blocks',
            //'icon'				=> 'admin-comments'
            //'keywords'			=> [''],
            'view_args' => [],
            'mode' => 'auto'
        ]);

        /*
        * service-type-5
        * ------------------------------
        */
        acf_register_block([
            'name'				=> 'service-type-5',
            'title'				=> __('HS Service Type 5', 'hs'),
            //'description'		=> __('Block description.', 'hs'),
            'render_callback'	=> 'hs_render_block_view',
            'category'			=> 'hs-blocks',
            //'icon'				=> 'admin-comments'
            //'keywords'			=> [''],
            'view_args' => [],
            'mode' => 'auto'
        ]);

        /*
        * service-type-6-group
        * ------------------------------
        */
        acf_register_block([
            'name'				=> 'service-type-6-group',
            'title'				=> __('HS Service Type 6 Group', 'hs'),
            //'description'		=> __('Block description.', 'hs'),
            'render_callback'	=> 'hs_render_block_view',
            'category'			=> 'hs-blocks',
            //'icon'				=> 'admin-comments'
            //'keywords'			=> [''],
            'view_args' => [],
            'mode' => 'auto'
        ]);

        /*
        * person-card-group
        * ------------------------------
        */
        acf_register_block([
            'name'				=> 'person-card-group',
            'title'				=> __('HS Person Card Group', 'hs'),
            //'description'		=> __('Block description.', 'hs'),
            'render_callback'	=> 'hs_render_block_view',
            'category'			=> 'hs-blocks',
            //'icon'				=> 'admin-comments'
            //'keywords'			=> [''],
            'view_args' => [],
            'mode' => 'auto'
        ]);

        /*
        * testimonial-group
        * ------------------------------
        */
        acf_register_block([
            'name'				=> 'testimonial-group',
            'title'				=> __('HS Testimonial Group', 'hs'),
            //'description'		=> __('Block description.', 'hs'),
            'render_callback'	=> 'hs_render_block_view',
            'category'			=> 'hs-blocks',
            //'icon'				=> 'admin-comments'
            //'keywords'			=> [''],
            'view_args' => [],
            'mode' => 'auto'
        ]);

        /*
        * contact-form
        * ------------------------------
        */
        acf_register_block([
            'name'				=> 'contact-form',
            'title'				=> __('HS Contact Form', 'hs'),
            //'description'		=> __('Block description.', 'hs'),
            'render_callback'	=> 'hs_render_block_view',
            'category'			=> 'hs-blocks',
            //'icon'				=> 'admin-comments'
            //'keywords'			=> [''],
            'view_args' => [],
            'mode' => 'auto'
        ]);

        /*
        * contact-address
        * ------------------------------
        */
        acf_register_block([
            'name'				=> 'contact-address',
            'title'				=> __('HS Contact Address', 'hs'),
            //'description'		=> __('Block description.', 'hs'),
            'render_callback'	=> 'hs_render_block_view',
            'category'			=> 'hs-blocks',
            //'icon'				=> 'admin-comments'
            //'keywords'			=> [''],
            'view_args' => [],
            'mode' => 'auto'
        ]);

        /*
        * process-icons
        * ------------------------------
        */
        acf_register_block([
            'name'				=> 'process-icons',
            'title'				=> __('HS Process Icons', 'hs'),
            //'description'		=> __('Block description.', 'hs'),
            'render_callback'	=> 'hs_render_block_view',
            'category'			=> 'hs-blocks',
            //'icon'				=> 'admin-comments'
            //'keywords'			=> [''],
            'view_args' => [],
            'mode' => 'auto'
        ]);

        /*
        * value-table
        * ------------------------------
        */
        acf_register_block([
            'name'				=> 'value-table',
            'title'				=> __('HS Value Table', 'hs'),
            //'description'		=> __('Block description.', 'hs'),
            'render_callback'	=> 'hs_render_block_view',
            'category'			=> 'hs-blocks',
            //'icon'				=> 'admin-comments'
            //'keywords'			=> [''],
            'view_args' => [],
            'mode' => 'auto'
        ]);

        /*
        * client-logos
        * ------------------------------
        */
        acf_register_block([
            'name'				=> 'client-logos',
            'title'				=> __('HS Client Logos', 'hs'),
            //'description'		=> __('Block description.', 'hs'),
            'render_callback'	=> 'hs_render_block_view',
            'category'			=> 'hs-blocks',
            //'icon'				=> 'admin-comments'
            //'keywords'			=> [''],
            'view_args' => [],
            'mode' => 'auto'
        ]);

        /*
        * steps-line
        * ------------------------------
        */
        acf_register_block([
            'name'				=> 'steps-line',
            'title'				=> __('HS Steps Line', 'hs'),
            //'description'		=> __('Block description.', 'hs'),
            'render_callback'	=> 'hs_render_block_view',
            'category'			=> 'hs-blocks',
            //'icon'				=> 'admin-comments'
            //'keywords'			=> [''],
            'view_args' => [],
            'mode' => 'auto'
        ]);

        /*
        * lang-selector
        * ------------------------------
        */
        acf_register_block([
            'name'				=> 'lang-selector',
            'title'				=> __('HS Lang Selector', 'hs'),
            //'description'		=> __('Block description.', 'hs'),
            'render_callback'	=> 'hs_render_block_view',
            'category'			=> 'hs-blocks',
            //'icon'				=> 'admin-comments'
            //'keywords'			=> [''],
            'view_args' => [],
            'mode' => 'auto'
        ]);

        /*
        * social-links-footer-widget
        * ------------------------------
        */
        acf_register_block([
            'name'				=> 'social-links-footer-widget',
            'title'				=> __('HS Social Links Footer Widget', 'hs'),
            //'description'		=> __('Block description.', 'hs'),
            'render_callback'	=> 'hs_render_block_view',
            'category'			=> 'hs-blocks',
            //'icon'				=> 'admin-comments'
            //'keywords'			=> [''],
            'view_args' => [],
            'mode' => 'auto'
        ]);

        /*
        * related-sites-footer-widget
        * ------------------------------
        */
        acf_register_block([
            'name'				=> 'related-sites-footer-widget',
            'title'				=> __('HS Related Sites Footer Widget', 'hs'),
            //'description'		=> __('Block description.', 'hs'),
            'render_callback'	=> 'hs_render_block_view',
            'category'			=> 'hs-blocks',
            //'icon'				=> 'admin-comments'
            //'keywords'			=> [''],
            'view_args' => [],
            'mode' => 'auto'
        ]);

        /*
        * copyright-footer-widget
        * ------------------------------
        */
        acf_register_block([
            'name'				=> 'copyright-footer-widget',
            'title'				=> __('HS Copyright Footer Widget', 'hs'),
            //'description'		=> __('Block description.', 'hs'),
            'render_callback'	=> 'hs_render_block_view',
            'category'			=> 'hs-blocks',
            //'icon'				=> 'admin-comments'
            //'keywords'			=> [''],
            'view_args' => [],
            'mode' => 'auto'
        ]);
    }
}

/*
* Register callback wrapper function
* ----------------------------------------
*/
function hs_render_block_view($block)
{
    $acf_prefix = 'acf/';
    $view = (strpos($block['name'], $acf_prefix) === 0) ? substr($block['name'], strlen($acf_prefix)) : $block['name'];
    $view_args = isset($block['view_args']) ? $block['view_args'] : [];
    $view_args['html_class'] = isset($block['className']) ? $block['className'] : '';

    echo wpseed_get_view($view, $view_args);
}
