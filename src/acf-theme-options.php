<?php 

if(function_exists('acf_add_options_sub_page'))
{
    $langs = \HS\Utils::getLanguages();

    if($langs)
    {
        foreach($langs as $lang)
        {
            acf_add_options_sub_page(
                [
                    'page_title' 	=> sprintf(__('Theme Options %s', 'hs'), strtoupper($lang['code'])),
                    'menu_title'	=> sprintf(__('Theme Options %s', 'hs'), strtoupper($lang['code'])),
                    'parent_slug'   => 'themes.php',
                    'menu_slug' 	=> 'hs-theme-options-' . $lang['code'],
                    'capability'	=> 'manage_options'
                ]
            );
        }
    }
}
