<?php

/*
 * Register block categories
 * ----------------------------------------
 */
//add_filter('block_categories', 'hs_blocks_register_category');

function hs_blocks_register_category($categories)
{
	return array_merge(
		$categories,
        [
            [
				'slug' => 'hs-blocks',
				'title' => wp_get_theme()->get('Theme Name')
            ]
        ]
	);
}

/*
 * Register blocks
 * ----------------------------------------
 */
//add_action('init', 'hs_blocks_register');

function hs_blocks_register()
{
    register_block_type('hs/user-login-form', [
        'api_version' => 2,
        'render_callback' => 'hs_blocks_callback',
        'attributes' => [
            'view' => ['default' => 'user-login-form', 'type' => 'string']
        ]
    ]);
    
    register_block_type('hs/user-register-form', [
        'api_version' => 2,
        'render_callback' => 'hs_blocks_callback',
        'attributes' => [
            'view' => ['default' => 'user-register-form', 'type' => 'string']
        ]
    ]);
}

function hs_blocks_callback($attributes, $content)
{
    if(isset($attributes['view']))
    {
        $view = $attributes['view'];
        $args = $attributes;
        unset($args['view']);
        
        return wpseed_get_view($view, $args);
    }
}

/*
 * Register blocks scripts
 * ----------------------------------------
 */
//add_action('init', 'hs_blocks_register_scripts');

function hs_blocks_register_scripts()
{
    $asset_file = HS_DIR . '/build/index.asset.php';
    
    if(!file_exists($asset_file)) return;
    
    $asset = require $asset_file;
    
    wp_register_script(
        'hs-index',
        HS_INDEX . '/build/index.js',
        $asset['dependencies'],
        $asset['version']
    );

    wp_register_style(
        'hs-blocks-edit',
        HS_INDEX . '/assets/css/blocks-edit.css',
        [],
        HS_VERSION
    );

    wp_register_style(
        'hs-blocks-save',
        HS_INDEX . '/assets/css/blocks-save.css',
        [],
        HS_VERSION
    );
}

/*
 * Enqueue blocks scripts on the editor
 * ----------------------------------------
 */
//add_action('enqueue_block_editor_assets', 'hs_blocks_enqueue_scripts_edit');

function hs_blocks_enqueue_scripts_edit()
{
    wp_enqueue_script('hs-index');
    wp_enqueue_style('hs-blocks-edit');
    wp_enqueue_style('hs-blocks-save');
}

/*
 * Enqueue blocks scripts on the front
 * ----------------------------------------
 */
//add_action('wp_enqueue_scripts', 'hs_blocks_enqueue_scripts_save');

function hs_blocks_enqueue_scripts_save()
{
    wp_enqueue_script('hs-index');
    wp_enqueue_style('hs-blocks-save');
}

/*
 * Add global config JS var
 * ----------------------------------------
 */
//add_action('init', 'hs_blocks_add_configs');

function hs_blocks_add_configs()
{
    wp_localize_script(
        'hs-index', 
        'hsIndexConfigs', [
            'common' => [
                'ajax_url' => admin_url('admin-ajax.php')
            ]
        ]
    );
}
