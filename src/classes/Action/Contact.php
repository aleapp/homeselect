<?php 

namespace HS\Action;

use HSP\Type\Agent as Type_Agent;
use HSP\Type\Property as Type_Property;

class Contact 
{
    public function __construct()
    {
        add_action('wpcf7_before_send_mail', __CLASS__ . '::addCustomFormTags');
        add_filter('wpcf7_mail_components', __CLASS__ . '::parseCustomFormTags');
        add_filter('shortcode_atts_wpcf7', __CLASS__ . '::addCustomShortocdeAtts', 10, 3);
        add_action('wpcf7_before_send_mail', __CLASS__ . '::addAgentRecipient');
    }
    
    static function addCustomFormTags()
    {
        add_shortcode('property-ref', __CLASS__ . '::addCustomFormTagPropertyRef');
        add_shortcode('property-offer', __CLASS__ . '::addCustomFormTagPropertyOffer');
    }
    
    static function addCustomFormTagPropertyRef($atts)
    {
        $out = '';
        if(class_exists('\WPCF7_Submission'))
        {
            $form_data = \WPCF7_Submission::get_instance()->get_posted_data();
            $property_id = isset($form_data['property-id']) ? (int)$form_data['property-id'] : 0;
            $type_property = new Type_Property($property_id);
            $out = $type_property->getReference();
        }
        return $out;
    }
    
    static function addCustomFormTagPropertyOffer($atts)
    {
        $out = '';
        if(class_exists('\WPCF7_Submission'))
        {
            $form_data = \WPCF7_Submission::get_instance()->get_posted_data();
            $property_id = isset($form_data['property-id']) ? (int)$form_data['property-id'] : 0;
            $type_property = new Type_Property($property_id);
            $out = $type_property->getOffer(true);
        }
        return $out;
    }
    
    static function parseCustomFormTags($components)
    {
        if(isset($components['body']))
        {
            $components['body'] = do_shortcode($components['body']);
        }
        
        return $components;
    }

    static function addCustomShortocdeAtts($out, $pairs, $atts)
    {
        $fields = [
            'agent-id',
            'property-id'
        ];

        foreach($fields as $field)
        {
            if(isset($atts[$field]))
            {
                $out[$field] = $atts[$field];
            }
        }

        return $out;
    }

    static function addAgentRecipient($form)
    {
        // $form_curr = \WPCF7_ContactForm::get_current();
        $form_data = \WPCF7_Submission::get_instance()->get_posted_data();

        $agent_id = isset($form_data['agent-id']) ? (int)$form_data['agent-id'] : 0;
        $property_id = isset($form_data['property-id']) ? (int)$form_data['property-id'] : 0;

        if($agent_id)
        {
            $type_agent = new Type_Agent($agent_id);
            $type_property = new Type_Property($property_id);
            $agent_email = $type_property->isOfferSale() ? $type_agent->get_sales_email() : $type_agent->get_rents_email();

            if($agent_email)
            {
                $properties = $form->get_properties();

                if(isset($properties['mail']))
                {
                    $properties['mail']['recipient'] = $agent_email;
//                    $properties['mail']['recipient'] = 'aleappco@gmail.com';

                    $form->set_properties($properties);
                }
            }
        }
    }
}