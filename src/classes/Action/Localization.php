<?php

namespace HS\Action;

use WPSEED\Req;
use HS\Session as Session;
use HS\Localization as Utils_Localization;

class Localization
{
    public function __construct()
    {
        add_action('init', __CLASS__ . '::setCurrentCity');
        add_action('template_redirect', __CLASS__ . '::redirectHomeByCity');
    }

    static function setCurrentCity()
    {
        $req = new Req();

        $req_city = $req->get(Utils_Localization::SLUG);
        
        if($req_city && Utils_Localization::isValidCity($req_city))
        {
            Session::setCookie(Utils_Localization::getCookieName(), $req_city);
            wp_redirect(Utils_Localization::getHomeUrlByCity($req_city));
            exit;
        }
    }

    static function redirectHomeByCity()
    {
        global $post;

        $home_id = Utils_Localization::getHomeIdByCity();

        if(is_front_page() && $home_id && $post->ID !== $home_id)
        {
            wp_redirect(get_permalink($home_id));
            exit;
        }
    }
}
