<?php

namespace HS\Action;

use WPSEED\Req;
use HS\Utils;
use HS\Utils_Property;
use HSP\Type\Property as Type_Property;
use HS\Session;

class Property
{
    public function __construct()
    {
        add_action('wp_ajax_filter_properties', __CLASS__ . '::filterProperties');
        add_action('wp_ajax_nopriv_filter_properties', __CLASS__ . '::filterProperties');

        add_action('template_redirect', __CLASS__ . '::setPropertyListBackLink');
    }

    static function filterProperties()
    {
        $req = new Req();

        $resp = [
            'property_list' => '',
            'filters_dyn_opts' => []
            // 'debug' => []
        ];

        $q_args = [
            'property_offer' => $req->get('property_offer'),
            'city' => $req->get('city'),
            'parish' => $req->get('parish'),
            'sale_price' => $req->get('sale_price') ? $req->get('sale_price') : Utils::getMetaValsRange('sale_price', true),
            'rent_price' => $req->get('rent_price') ? $req->get('rent_price') : Utils::getMetaValsRange('rent_price', true),
            'bedrooms' => $req->get('bedrooms', 'integer', 0),
            'bathrooms' => $req->get('bathrooms', 'integer', 0),
            'paged' => $req->get('paged', 'integer', 1),
            'posts_per_page' => Utils_Property::PROPERTY_LIST_PER_PAGE,
            'orderby' => $req->get('orderby', 'text', 'price_high_low')
        ];

        // Dynamic options
        $props_config = Type_Property::_get_props_config();
        foreach($props_config as $key => $prop_config)
        {
            $type = isset($prop_config['type']) ? $prop_config['type'] : 'meta';

            $parent_key = isset($prop_config['parent']) ? $prop_config['parent'] : null;
            $parent_prop_config = (isset($parent_key) && isset($props_config[$parent_key])) ? $props_config[$parent_key] : null;

            if(isset($parent_prop_config))
            {
                $parent_type = isset($parent_prop_config['type']) ? $parent_prop_config['type'] : 'meta';
                $parent_value = (int)$req->get($parent_key);

                if(empty($parent_value))
                {
                    continue;
                }

                switch($type)
                {
                    case 'taxonomy':

                        //Unset child value
                        if(isset($q_args[$key]))
                        {
                            $parent_term_id = (int)get_term_meta($q_args[$key], 'parent_term_id', true);
                            if($parent_term_id !== $parent_value)
                            {
                                unset($q_args[$key]);
                            }
                        }
                        
                        $resp['filters_dyn_opts'][$key] = wpseed_get_view('nice-dropdown', [
                            // 'label' => isset($prop_config['filter_label']) ? (isset($prop_config['filter_label_alt']) ? $prop_config['filter_label_alt'] : $prop_config['filter_label']) : __('Select', 'hs'),
                            'label' => isset($prop_config['filter_label']) ? $prop_config['filter_label'] : __('Select', 'hs'),
                            'empty_name' => isset($prop_config['filter_empty_opt']) ? $prop_config['filter_empty_opt'] : __('All', 'hs'),
                            'selected' => isset($q_args[$key]) ? $q_args[$key] : '',
                            'input_name' => $key,
                            'options' => Utils::getTaxonomyTermOptions($key, 'term_id', [
                                'meta_parent_term_id' => $parent_value,
                                'empty_opt' => isset($prop_config['filter_empty_opt']) ? $prop_config['filter_empty_opt'] : ''
                            ]),
                            'input_class' => 'change-submit'
                        ]);
                        break;

                    case 'meta':

                        if(isset($prop_config['parent_meta_key']) && $parent_type === 'taxonomy')
                        {
                            $parent_meta_value = get_term_meta($parent_value, $prop_config['parent_meta_key'], true);

                            if($parent_meta_value)
                            {
                                $resp['filters_dyn_opts']['price_range'] = wpseed_get_view('property-filter-price-slider', [
                                    'input_name' => $parent_meta_value
                                ]);
                            }
    
                            //Unset child value
                            if(isset($q_args[$key]) && $parent_meta_value !== $key)
                            {
                                unset($q_args[$key]);
                            }
                        }
                    }
            }
        }

        $properties = Utils_Property::getProperties($q_args);

        $resp['property_list'] = wpseed_get_view('property-list', [
            'render_items_only' => true,
            'items' => $properties['items'],
            'items_total' => $properties['items_total'],
            // 'list_pager_args' => [
            //     'paged' => $q_args['paged'],
            //     'items_total' => $properties['items_total']
            // ],
            'show_list_pager' => true
        ]);

        $resp['property_list_pager'] = wpseed_get_view('list-pager', [
            'paged' => $q_args['paged'],
            'items_total' =>  $properties['items_total'],
            'items_per_page' => Utils_Property::PROPERTY_LIST_PER_PAGE
        ]);

        $resp['found_summary'] = \HS\View\Property_Filters_Advanced::getFoundSummaryText($properties['items_total']);

        wp_send_json($resp);
    }

    static function setPropertyListBackLink()
    {
        if(is_singular('property') && isset($_SERVER['HTTP_REFERER']))
        {
            $list_page = Utils_Property::getListPageUrl();

            if(strpos($_SERVER['HTTP_REFERER'], $list_page) === 0)
            {
                Session::setCookie('property_back_link', $_SERVER['HTTP_REFERER']);
            }
        }
    }
}
