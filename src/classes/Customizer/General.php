<?php

namespace HS\Customizer;

use HS\Utils;

class General 
{
    const SECTION_ID = 'general_';

    protected $lang;
    protected $section_name;

    public function __construct()
    {
        add_action('init', [$this, 'init']);
        // add_action('customize_register', [$this, 'addSectionGeneral']);
    }

    public function init()
    {
        $this->lang = Utils::getCurrentLanguage();
        $this->section_name = self::SECTION_ID . 'options';

        add_action('customize_register', [$this, 'addSectionGeneral']);
    }
    
    public function addSectionGeneral($wp_customize)
    {
        $wp_customize->add_section($this->section_name, 
            [
                'title'       => __('General', 'hs'),
                'capability'  => 'edit_theme_options'
            ]
        );

        // Contact page
        $wp_customize->add_setting(self::SECTION_ID . 'contact_page_' . $this->lang,
            [
                'type'       => 'theme_mod',
                'capability' => 'edit_theme_options',
                'transport'  => 'postMessage'
            ]
        );
        $wp_customize->add_control(new \WP_Customize_Control(
            $wp_customize,
            self::SECTION_ID . 'list_page_' . $this->lang,
            [
                'label'      => __('Contact page', 'hs'),
                'settings'   => self::SECTION_ID . 'contact_page_' . $this->lang,
                'section'    => $this->section_name,
                'type' => 'select',
                'choices' => Utils::getPostTypeSelectOptions('page')
            ]
        ));
    }
}
