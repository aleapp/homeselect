<?php

namespace HS\Customizer;

use HS\Utils;

class Property 
{
    const SECTION_ID = 'property_';

    protected $lang;
    protected $section_name;

    public function __construct()
    {
        add_action('init', [$this, 'init']);
        // add_action('customize_register', [$this, 'addSectionProperty']);
    }

    public function init()
    {
        $this->lang = Utils::getCurrentLanguage();
        $this->section_name = self::SECTION_ID . 'options';

        add_action('customize_register', [$this, 'addSectionProperty']);
    }
    
    public function addSectionProperty($wp_customize)
    {
        $wp_customize->add_section($this->section_name, 
            [
                'title'       => __('Property', 'hs'),
                'capability'  => 'edit_theme_options'
            ]
        );

        // List page
        $wp_customize->add_setting(self::SECTION_ID . 'list_page_' . $this->lang,
            [
                'type'       => 'theme_mod',
                'capability' => 'edit_theme_options',
                'transport'  => 'postMessage'
            ]
        );
        $wp_customize->add_control(new \WP_Customize_Control(
            $wp_customize,
            self::SECTION_ID . 'list_page_' . $this->lang,
            [
                'label'      => __('List page', 'hs'),
                'settings'   => self::SECTION_ID . 'list_page_' . $this->lang,
                'section'    => $this->section_name,
                'type' => 'select',
                'choices' => Utils::getPostTypeSelectOptions('page')
            ]
        ));

        // Contact form Viewing
        $wp_customize->add_setting(self::SECTION_ID . 'contact_form_viewing_' . $this->lang,
            [
                'type'       => 'theme_mod',
                'capability' => 'edit_theme_options',
                'transport'  => 'postMessage'
            ]
        );
        $wp_customize->add_control(new \WP_Customize_Control(
            $wp_customize,
            self::SECTION_ID . 'contact_form_viewing_' . $this->lang,
            [
                'label'      => __('Form: Arrange viewing', 'hs'),
                'settings'   => self::SECTION_ID . 'contact_form_viewing_' . $this->lang,
                'section'    => $this->section_name,
                'type' => 'select',
                'choices' => Utils::getPostTypeSelectOptions('wpcf7_contact_form')
            ]
        ));

        // Contact form Agent
        $wp_customize->add_setting(self::SECTION_ID . 'contact_form_agent_' . $this->lang,
            [
                'type'       => 'theme_mod',
                'capability' => 'edit_theme_options',
                'transport'  => 'postMessage'
            ]
        );
        $wp_customize->add_control(new \WP_Customize_Control(
            $wp_customize,
            self::SECTION_ID . 'contact_form_agent_' . $this->lang,
            [
                'label'      => __('Form: Contact agent', 'hs'),
                'settings'   => self::SECTION_ID . 'contact_form_agent_' . $this->lang,
                'section'    => $this->section_name,
                'type' => 'select',
                'choices' => Utils::getPostTypeSelectOptions('wpcf7_contact_form')
            ]
        ));

        // Google maps API Key
        $wp_customize->add_setting(self::SECTION_ID . 'googlemaps_api_key_' . $this->lang,
            [
                'type'       => 'theme_mod',
                'capability' => 'edit_theme_options',
                'transport'  => 'postMessage'
            ]
        );
        $wp_customize->add_control(new \WP_Customize_Control(
            $wp_customize,
            self::SECTION_ID . 'googlemaps_api_key_' . $this->lang,
            [
                'label'      => __('Google maps Api key', 'hs'),
                'settings'   => self::SECTION_ID . 'googlemaps_api_key_' . $this->lang,
                'section'    => $this->section_name,
                'type' => 'text'
            ]
        ));
    }
}
