<?php

namespace HS\Filter;

use HS\Utils;
use HS\View\View as View;

class Nav
{
    public function __construct()
    {
        add_filter('walker_nav_menu_start_el', __CLASS__ . '::addNavItemImage', 10, 4);
        add_filter('nav_menu_css_class', __CLASS__ . '::addNavItemClasses', 10, 3);
    }
    
    static function addNavItemImage($item_output, $item, $depth, $args)
    {
        $image_id = get_field('hs__menu__image', $item);
        // $image_html = wp_get_attachment_image($image_id, 'full', false, ['class' => 'img-fluid menu-item-img']);
        $image_html = View::getBgImageHtml($image_id, 'rect-180-100', 'bg-img-cover', ['class' => 'nav-image']);
        if($image_html)
        {
            $item_output = str_replace('<span class="item-name">', $image_html . '<span class="item-name">', $item_output);
        }
        
        return $item_output;
    }

    static function addNavItemClasses($classes, $item, $args)
    {
        $image_id = get_field('hs__menu__image', $item);

        if($image_id)
        {
            $classes[] = 'has-image';
        }
        
        return $classes;
    }
}
