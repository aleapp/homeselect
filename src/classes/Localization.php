<?php

namespace HS;

use HSP\Utils_Property;

class Localization
{
    const SLUG = 'loc';

    static function getCookieName()
    {
        $lang = Utils::getCurrentLanguage(true);
        return self::SLUG . '_' . $lang;
    }

    static function getCityOptions($keys_only=false)
    {
        $city_options = [];

        $terms = get_terms([
            'taxonomy' => 'city',
            'hide_empty' => false,
            // 'orderby' => 'term_order',
            // 'order' => 'ASC',
            'meta_key' => 'term_order',
            'meta_type' => 'NUMERIC',
            'orderby' => 'meta_value',
            'order' => 'ASC'
        ]);
        
        if(!is_wp_error($terms))
        {
            foreach($terms as $term)
            {
                $home_page = (int)get_term_meta($term->term_id, 'home_page', true);
                if($home_page)
                {
                    $city_options[$term->slug] = [
                        'id' => $term->term_id,
                        'slug' => $term->slug,
                        'name' => $term->name,
                        'home_id' => $home_page
                    ];
                }
            }
        }

        return $keys_only ? array_keys($city_options) : $city_options;
    }

    static function getCityConfig($city=null)
    {
        if(!isset($city))
        {
            $city = self::getSessionCity();
        }
        
        $city_options = self::getCityOptions();

        foreach($city_options as $city_option)
        {
            if($city_option['slug'] === $city || $city_option['id'] === $city)
            {
                return $city_option;
            }
        }

        return false;
    }

    static function isValidCity($city)
    {
        $city_config = self::getCityConfig($city);
        return !empty($city_config);
    }

    static function getSessionCity($as_id=false)
    {
        $city = Session::getCookie(self::getCookieName());

        if(isset($_GET[self::SLUG]) && self::isValidCity($_GET[self::SLUG]))
        {
            $city = $_GET[self::SLUG];
        }

        if(empty($city))
        {
            // Get default city
            $city_options = self::getCityOptions(true);
            
            if(isset($city_options[0]))
            {
                $city = $city_options[0];
            }
        }

        if($as_id)
        {
            $city_term = get_term_by('slug', $city, 'city');
            return is_a($city_term, 'WP_Term') ? $city_term->term_id : 0;
        }

        return $city;
    }

    static function getHomeIdByCity($city=null)
    {
        $city_config = self::getCityConfig($city);
        return $city_config ? $city_config['home_id'] : 0;
    }

    static function getHomeUrlByCity($city=null)
    {
        $city_home_id = self::getHomeIdByCity($city);
        return $city_home_id ? get_permalink($city_home_id) : get_home_url();
    }
}
