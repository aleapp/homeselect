<?php

namespace HS;

class Utils extends \HSP\Utils 
{
    static function getThemeOption($key, $default=null)
    {
        $lang = self::getCurrentLanguage();
        // $option_name = 'options_' . $key . '_' . $lang;
        // return get_option($option_name, $default);

        $option_name = $key . '_' . $lang;
        
        $option = get_theme_mod($option_name);

        return (empty($option) && isset($default)) ? $default : $option;
    }

    static function genId($pref='', $sfx='')
    {
        $chars = str_shuffle('abcdefghijklmnopqrstuvwxyz1234567890');
        return $pref . substr(str_shuffle($chars), 0, 10) . $sfx;
    }
    
    static function isAdmin()
    {
        return (is_admin() && (!wp_doing_ajax() || isset($_POST['block'])));
    }
    
    static function parseCsv($file, $sep=",", $enc='"')
    {
        $data = [];
        
        $handle = file_exists($file) ? fopen($file, "r") : false;
        
        if($handle !== false)
        {
            $header = [];
            $rows = [];
            $i = 0;
            while(($row = fgetcsv($handle, 0, $sep, $enc)) !== false)
            {
                if(!$i)
                {
                    $header = $row;
                }
                else{
                    $data[] = array_combine($header, $row);
                }
                $i++;
            }
            
            fclose($handle);
        }
        
        return $data;
    }
    
    static function getAttachmentSrc($id, $size='full')
    {
        $image_src = $id ? wp_get_attachment_image_src((int)$id, $size) : [];
        return isset($image_src[0]) ? $image_src[0] : '';
    }
    
    static function getAttachmentSrcArr($ids, $size='full')
    {
        $_ids = is_array($ids) ? $ids : [$ids];
        $src_arr = [];
        foreach($ids as $id)
        {
            $image_src = self::getAttachmentSrc($id, $size);
            if($image_src)
            {
                $src_arr[] = $image_src;
            }
        }
        return $src_arr;
    }

    static function formatPhoneTel($num_str)
    {
        $tel = '';
        if($num_str)
        {
            $tel = str_replace([' ', '-'], '', $num_str);
        }
        return $tel;
    }

    static function getAssignedMenuByLocation($location)
    {
        $menu_locations = get_nav_menu_locations();
        return isset($menu_locations[$location]) ? $menu_locations[$location] : false;
    }

    static function getTaxonomyTermOptions($taxonomy, $field='term_id', $args=[], $alt_name=false)
    {
        $_args = wp_parse_args($args, [

            'meta_key' => '',
            'meta_value' => '',
            'meta_parent_term_id' => 0,
            'orderby' => 'name',
            'order' => 'ASC',
            // 'empty_opt' => ''
        ]);
        if($_args['meta_parent_term_id'])
        {
            $_args['meta_key'] = 'parent_term_id';
            $_args['meta_value'] = $_args['meta_parent_term_id'];
        }

        $q_args = [
            'taxonomy' => $taxonomy,
            'hide_empty' => true,
            'meta_key' => $_args['meta_key'],
            'meta_value' => $_args['meta_value']
        ];

        $options = [];

        // if($_args['empty_opt'])
        // {
        //     $options[] = [
        //         'value' => '',
        //         'name' => $_args['empty_opt']
        //     ];
        // }

        $terms = get_terms($q_args);

        if(!is_wp_error($terms))
        {
            foreach($terms as $term)
            {
                $_alt_name = $alt_name ? get_term_meta($term->term_id, 'alt_name', true) : false;

                $options[] = [
                    'value' => $term->$field,
                    'name' => $_alt_name ? $_alt_name : $term->name
                ];
            }
        }
        
        return $options;
    }

    /*
    @param string $cast integer|float
    */
    static function getMetaValsRange($meta_key, $as_string=false, $cast='integer')
    {
        global $wpdb;

        $range = [
            'min' => $wpdb->get_var($wpdb->prepare("SELECT MIN(CAST(meta_value AS DECIMAL)) FROM $wpdb->postmeta WHERE meta_key=%s", $meta_key)),
            'max' => $wpdb->get_var($wpdb->prepare("SELECT MAX(CAST(meta_value AS DECIMAL)) FROM $wpdb->postmeta WHERE meta_key=%s", $meta_key))
        ];

        switch($cast)
        {
            case 'integer':
                $range['min'] = intval($range['min']);
                $range['max'] = intval($range['max']);
                break;
            case 'float':
                $range['min'] = floatval($range['min']);
                $range['max'] = floatval($range['max']);
                break;
        }

        return $as_string ? $range['min'] . '-' . $range['max'] : $range;
    }

    /*
    @param string $cast integer|float
    */
    static function getMetaValsUnique($meta_key, $cast='integer', $as_options=false)
    {
        global $wpdb;

        $vals = $wpdb->get_col($wpdb->prepare("SELECT DISTINCT meta_value FROM $wpdb->postmeta WHERE meta_key=%s", $meta_key));

        if($vals)
        {
            foreach($vals as &$val)
            {
                switch($cast)
                {
                    case 'integer':
                        $val = intval($val);
                        break;
                    case 'float':
                        $val = floatval($val);
                        break;
                }
            }

            if(in_array($cast, ['integer', 'float']))
            {
                sort($vals, SORT_NUMERIC);
            }
            else{
                sort($vals, SORT_STRING);
            }

            if($as_options)
            {
                $options = [];
                foreach($vals as $val)
                {
                    $options[] = [
                        'value' => $val,
                        'name' => $val
                    ];
                }
                $vals = $options;
            }
        }

        return $vals;
    }
}
