<?php

namespace HS;

class Utils_Property extends \HSP\Utils_Property 
{
    const PROPERTY_LIST_PER_PAGE = 10;
    const PROPERTY_LIST_MAP_ZOOM = 16;

    static function getListPageUrl()
    {
        $list_page_id = (int)Utils::getThemeOption('property_list_page');
        return $list_page_id ? get_permalink($list_page_id) : '';
    }

    static function getSimpleFilterLinks()
    {
        $links = [];
        // $links_options = Utils::getThemeOption('simple_filter_links', []);
        $links_options = get_field('simple_filter_links', 'options');
        if(is_array($links_options))
        {
            foreach($links_options as $link)
            {
                $links[] = wp_parse_args($link, [
                    'link_url' => '',
                    'link_page' => '',
                    'link_label' => ''
                ]);
            }
        }
        return $links;
    }

    static function getCityContactEmail($city_id=null)
    {
        $_city_id = isset($city_id) ? $city_id : Localization::getSessionCity(true);
        return $_city_id ? get_term_meta($_city_id, 'contact_email', true) : '';
    }

    static function getCityContactPhone($city_id=null)
    {
        $_city_id = isset($city_id) ? $city_id : Localization::getSessionCity(true);
        return $_city_id ? get_term_meta($_city_id, 'contact_phone', true) : '';
    }

    static function getPropertyListBackLink()
    {
        return Session::getCookie('property_back_link', self::getListPageUrl());
    }
}
