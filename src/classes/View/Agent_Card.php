<?php

namespace HS\View;

use HSP\Type\Agent;
use HSP\Type\Property;

class Agent_Card extends View
{
    public function __construct($args)
    {
        parent::__construct($args, [
            
            'agent' => new Agent(),
            'property' => new Property(),
            'contact_form_id' => 0
        ]);

        $this->setArgs();

        add_action('wp_footer', [$this, 'addContactFormModal']);
    }

    protected function setArgs()
    {
        $agent = $this->args['agent'];
        $property = $this->args['property'];

        $this->args['image_html'] = $agent->get_profile_image_html();
        $this->args['contact_phone'] = $agent->get_phone();
        $this->args['contact_phone_href'] = $agent->get_phone(true);
        $this->args['contact_email'] = $property->isOfferSale() ? $agent->get_sales_email() : $agent->get_rents_email();
        $this->args['full_name'] = $agent->get_full_name();
    }

    public function addContactFormModal()
    {
        if($this->has_contact_form_id())
        {
            wpseed_print_view('property-contact-agent', [

                'id' => $this->getId(),
                'agent_id' => $this->args['agent']->get_id(),
                'property_id' => $this->args['property']->get_id(),
                'contact_form_id' => $this->get_contact_form_id()
            ]);
        }
    }
}
