<?php

namespace HS\View;

class Banner_Slider extends View
{
    public function __construct($args)
    {
        parent::__construct($args, [
            
            'slide_items' => $this->getField('slides', []),

            'show_pagination' => $this->getField('show_pagination', false),
            'show_nav' => $this->getField('show_nav', false)
        ]);
        
        $this->setSlideItems();
    }
    
    protected function setSlideItems()
    {
        $slide_items = [];
        foreach($this->get_slide_items() as $slide)
        {
            $slide_items[] = [
                'banner_image' => isset($slide['banner_image']) ? $this->getAttachmentImageSrc($slide['banner_image'], 'full') : ''
            ];
        }
        $this->args['slide_items'] = $slide_items;
    }
}
