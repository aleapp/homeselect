<?php

namespace HS\View;

class Client_Logos extends View
{
    public function __construct($args)
    {
        parent::__construct($args, [
            
            'title' => $this->getField('title', ''),
            'logos' => $this->getField('logos', []),
            'row_max' => 6
        ]);
        
        $this->setLogos();
        $this->__setHtmlClass();
    }

    protected function setLogos()
    {
        $logos = [];
        foreach($this->get_logos() as $logo)
        {
            $logos[] = isset($logo['logo_image']) ? $this->getAttachmentImageHtml((int)$logo['logo_image'], 'full', 'rect-150-100', 'contain') : '';
        }
        $this->args['logos'] = array_chunk($logos, $this->get_row_max());
    }
    
    protected function __setHtmlClass()
    {
        if($this->isBgColorDark())
        {
            $this->_addHtmlClass('bg-dark');
        }
    }
    
    public function isBgColorDark()
    {
        return ($this->has_bg_color() && in_array($this->get_bg_color(), ['dark-grey', 'dark-blue']));
    }
}
