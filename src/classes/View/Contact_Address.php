<?php

namespace HS\View;

use HS\Utils as Utils;

class Contact_Address extends View
{
    public function __construct($args)
    {
        parent::__construct($args, [
            
            'title' => $this->getField('title', ''),

            'address' => $this->getField('address', ''),
            'phones' => $this->getField('phones', []),
            'emails' => $this->getField('emails', []),
            'working_hours' => $this->getField('working_hours', []),

            'variant' => $this->getField('variant', 'default')
        ]);

        $this->setArgs();
        $this->__setHtmlClass();
    }

    protected function setArgs()
    {
        foreach($this->get_phones() as $i => $phone)
        {
            $this->args['phones'][$i] = wp_parse_args($phone, [
                'label' => '',
                'number' => '',
                'number_tel' => ''
            ]);
            if($this->args['phones'][$i]['number'])
            {
                $this->args['phones'][$i]['number_tel'] = Utils::formatPhoneTel($this->args['phones'][$i]['number']);
            }
        }

        foreach($this->get_emails() as $i => $email)
        {
            $this->args['emails'][$i] = wp_parse_args($email, [
                'label' => '',
                'email' => ''
            ]);
        }

        foreach($this->get_working_hours() as $i => $hours)
        {
            $this->args['working_hours'][$i] = wp_parse_args($hours, [
                'day' => '',
                'hours' => ''
            ]);
        }

        //$this->setArgsToProps(true);
    }

    protected function __setHtmlClass()
    {
        $this->_addHtmlClass('variant-' . $this->get_variant());
    }
}
