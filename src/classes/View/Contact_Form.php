<?php

namespace HS\View;

class Contact_Form extends View
{
    public function __construct($args)
    {
        parent::__construct($args, [
            
            'title' => $this->getField('title', ''),
            'description' => $this->getField('description', ''),
            'cf7_id' => $this->getField('cf7_id', ''),
            'address_info' => $this->getField('address_info', ''),
            'address_info_shortcode' => $this->getField('address_info_shortcode', ''),

            'bg_color' => $this->getField('bg_color', 'dark-grey')
        ]);

        $this->__setHtmlClass();
    }

    protected function __setHtmlClass()
    {
        if($this->has_address_info())
        {
            $this->_addHtmlClass('has-address-info');
        }
    }
}
