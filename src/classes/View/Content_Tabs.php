<?php

namespace HS\View;

class Content_Tabs extends View
{
    public function __construct($args)
    {
        parent::__construct($args, [
            
            'title' => $this->getField('title', ''),
            'headline_text' => $this->getField('headline_text', ''),
            'tab_items' => $this->getField('tab_items', [])
        ]);

        $this->setItems();
        //$this->setArgsToProps(true);
    }

    protected function setItems()
    {
        $tab_items = [];

        foreach($this->get_tab_items() as $item)
        {
            $tab_items[] = wp_parse_args($item, [
                'tab_intro' => '',
                'tab_shortcode' => '',
                'tab_wp_block' => '',
                'tab_content' => '',
                'tab_more_url' => '',
                'tab_more_page' => '',
                'tab_more_label' => __('See more', 'hs')
            ]);
        }

        $this->args['tab_items'] = $tab_items;
    }
}
