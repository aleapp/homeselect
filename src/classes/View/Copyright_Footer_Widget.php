<?php

namespace HS\View;

class Copyright_Footer_Widget extends View
{
    public function __construct($args)
    {
        parent::__construct($args, [

            'copyright_text' => $this->getField('copyright_text', '&copy; ' . date('Y') . ' ' . get_bloginfo('site_name')),

            'top_level' => true
        ]);

        $this->parseCopyrightText();
    }

    protected function parseCopyrightText()
    {
        if(strpos($this->args['copyright_text'], '%year%') !== false)
        {
            $this->args['copyright_text'] = str_replace('%year%', date('Y'), $this->args['copyright_text']);
            //$this->setArgsToProps(true);
        }
    }
}
