<?php

namespace HS\View;

class Cta_Button extends View
{
    public function __construct($args)
    {
        parent::__construct($args, [
            
            'label' => $this->getField('label', 'Button'),
            'url' => $this->getField('url', ''),
            'url_target' => $this->getField('url_target', '_self'),
            'size' => $this->getField('size', 'small'),
            'color' => $this->getField('color', 'dark-blue'),
            'full_width' => $this->getField('full_width', false)
        ]);
        
        $this->__setHtmlClass();
    }
    
    protected function __setHtmlClass()
    {
        $this->_addHtmlClass('btn-t');

        if($this->has_size())
        {
            $this->_addHtmlClass('btn-' . $this->get_size());
        }
        if($this->has_color())
        {
            $this->_addHtmlClass('btn-' . $this->get_color());
        }
        if($this->has_full_width())
        {
            $this->_addHtmlClass('btn-full-w');
        }
    }
}
