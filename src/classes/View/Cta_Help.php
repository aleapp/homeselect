<?php

namespace HS\View;

class Cta_Help extends View
{
    public function __construct($args)
    {
        parent::__construct($args, [
            
            'title' => $this->getField('title', ''),
            'description' => $this->getField('description', ''),

            'more_page' => $this->getField('more_page', 0),
            'more_label' => $this->getField('more_label', __('Contact Us')),

            'bg_image' => $this->getField('bg_image', 0),
            'bg_image_url' => ''
        ]);
        
        $this->setArgs();
        $this->__setHtmlClass();
    }

    protected function setArgs()
    {
        $this->args['more_url'] = $this->args['more_page'] ? get_permalink($this->args['more_page']) : '';
        if($this->args['bg_image'])
        {
            $this->args['bg_image_url'] = $this->getAttachmentImageSrc($this->args['bg_image'], 'full');
        }
    }

    protected function __setHtmlClass()
    {
        $this->_addHtmlClass('bg-dark-blue');
        
        if($this->args['bg_image_url'])
        {
            $this->_addHtmlClass('has-bg-image');
        }
    }
}
