<?php

namespace HS\View;

class Cta_Meet extends View
{
    public function __construct($args)
    {
        parent::__construct($args, [
            
            'main_image' => (int)$this->getField('main_image', 0),
            'second_image' => (int)$this->getField('second_image', 0),
            
            'pre_title' => $this->getField('pre_title', ''),
            'title' => $this->getField('title', ''),
            
            'description' => $this->getField('description', ''),
            'more_url' => $this->getField('more_url', ''),
            'more_page' => (int)$this->getField('more_page', 0),
            'more_label' => $this->getField('more_label', __('See more', 'hs')),
            
            'bg_color' => $this->getField('bg_color', 'grey'),
            
            'top_level' => true
        ]);
        
        $this->__setHtmlClass();
        $this->setImages();
    }
    
    protected function __setHtmlClass()
    {
        if($this->has_bg_color())
        {
            $this->_addHtmlClass('bg-' . $this->get_bg_color());
        }
    }
    
    protected function setImages()
    {
        $this->args['main_image_url'] = $this->getAttachmentImageSrc($this->args['main_image'], 'full');
        $this->args['second_image_html'] = $this->getAttachmentImage($this->args['second_image'], 'full');
    }
}
