<?php

namespace HS\View;

class Cta_Quote extends View
{
    public function __construct($args)
    {
        parent::__construct($args, [
            
            'title' => $this->getField('title', ''),
            'description' => $this->getField('description', ''),
            'more_url' => $this->getField('more_url', ''),
            'more_label' => $this->getField('more_label', '')
        ]);
        
        $this->__setHtmlClass();
    }
    
    protected function __setHtmlClass()
    {
        if($this->isBgColorDark())
        {
            $this->_addHtmlClass('is-bg-dark');
        }
    }
    
    public function isBgColorDark()
    {
        return ($this->has_bg_color() && in_array($this->get_bg_color(), ['dark-grey', 'dark-blue']));
    }
}
