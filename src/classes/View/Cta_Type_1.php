<?php

namespace HS\View;

class Cta_Type_1 extends View
{
    public function __construct($args)
    {
        parent::__construct($args, [
            
            'pre_title' => $this->getField('pre_title', ''),
            'title' => $this->getField('title', ''),
            'description' => $this->getField('description', ''),
            'more_url' => $this->getField('more_url', ''),
            'more_page' => (int)$this->getField('more_page', 0),
            'more_label' => $this->getField('more_label', __('See more', 'hs'))
        ]);
        
        $this->__setHtmlClass();
    }
    
    protected function __setHtmlClass()
    {
        if($this->isBgColorDark())
        {
            $this->_addHtmlClass('is-bg-dark');
        }
    }
    
    public function isBgColorDark()
    {
        return ($this->has_bg_color() && in_array($this->get_bg_color(), ['dark-grey', 'dark-blue']));
    }
}
