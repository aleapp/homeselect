<?php

namespace HS\View;

use HS\Utils;

class Cta_Type_2 extends View
{
    public function __construct($args)
    {
        parent::__construct($args, [
            
            'banner_image_id' => $this->getField('banner_image', 0),
            'before_title' => $this->getField('before_title', ''),
            'title' => $this->getField('title', ''),
            'description' => $this->getField('description', ''),
            'more_url' => $this->getField('more_url', ''),
            'more_label' => $this->getField('more_label', '')
        ]);
        
        $this->setBannerImage();
    }
    
    protected function setBannerImage()
    {
        $this->args['banner_image'] = $this->getAttachmentImageSrc($this->args['banner_image_id'], 'full');
    }
}
