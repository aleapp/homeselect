<?php

namespace HS\View;

class Cta_Type_3 extends View
{
    public function __construct($args)
    {
        parent::__construct($args, [
            
            'title_main' => $this->getField('title_main', ''),
            'description_main' => $this->getField('description_main', ''),

            'title_second' => $this->getField('title_second', ''),
            'description_second' => $this->getField('description_second', ''),

            'more_url' => $this->getField('more_url', ''),
            'more_url_target' => $this->getField('more_url_target', ''),
            'more_label' => $this->getField('more_label', '')
        ]);
    }
}
