<?php

namespace HS\View;

class Footer extends View
{
    public function __construct($args)
    {
        parent::__construct($args, [

            'top_level' => true
        ]);
    }
}
