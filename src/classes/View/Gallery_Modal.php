<?php

namespace HS\View;

class Gallery_Modal extends View
{
    public function __construct($args)
    {
        parent::__construct($args, [

            'images' => [],
            'images_num' => 0,

            'gallery_title' => ''
        ]);

        $this->args['images_num'] = count($this->args['images']);
    }
}
