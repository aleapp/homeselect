<?php

namespace HS\View;

use HS\Localization;
use HS\Utils;
USE HS\Utils_Property;

class Header extends View
{
    public function __construct($args)
    {
        parent::__construct($args, [

            'city_options' => Localization::getCityOptions(),
            'home_url' => Localization::getHomeUrlByCity()
        ]);
    }

    public function getDefaultCityName()
    {
        $city_config = Localization::getCityConfig();
        return $city_config ? $city_config['name'] : __('Location', 'hs');
    }
}
