<?php

namespace HS\View;

class Header_Banner extends View
{
    public function __construct($args, $default_args=[])
    {
        parent::__construct($args, wp_parse_args($default_args, [
            
            'image_id' => $this->getField('image', 0),
            'title' => $this->getField('title', ''),
            'use_page_title' => $this->getField('use_page_title', false),
            'shadow' => $this->getField('shadow', false),
            
            'top_level' => true
        ]));

        $this->setPageTitle();
        $this->setImageUrl();
        $this->__setHtmlClass();
    }
    
    protected function __setHtmlClass()
    {
        if($this->has_image_url())
        {
            $this->_addHtmlClass('has-bg-image');
        }
    }
    
    protected function setPageTitle()
    {
        $admin_post_id = $this->getAdminPostId();
        $this->args['page_title'] = $this->args['use_page_title'] ? ((is_admin() && $admin_post_id) ? get_the_title($admin_post_id) : get_the_title()) : '';
    }
    
    protected function setImageUrl()
    {
        $this->args['image_url'] = $this->getAttachmentImageSrc($this->args['image_id'], 'full');
    }
}
