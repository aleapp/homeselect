<?php

namespace HS\View;

class Header_Banner_Search extends Header_Banner
{
    public function __construct($args)
    {
        parent::__construct($args, [
            
            'filter_external_links' => $this->getField('filter_external_links', [])
        ]);
    }
}
