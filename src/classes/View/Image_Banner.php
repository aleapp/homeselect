<?php

namespace HS\View;

class Image_Banner extends View
{
    public function __construct($args)
    {
        parent::__construct($args, [
            
            'image' => $this->getField('image', 0),
            'image_ratio' => $this->getField('image_ratio', '300-100'),
            'shadow' => $this->getField('shadow', false)
        ]);

        // $this->args['image_html'] = $this->args['image'] ? $this->getBgImageHtml($this->args['image'], 'rect-' . $this->args['image_ratio']) : '';
        $this->args['image_html'] = $this->args['image'] ? $this->getAttachmentImageHtml($this->args['image'], 'rect-' . $this->args['image_ratio']) : '';
    }
}
