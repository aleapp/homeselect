<?php

namespace HS\View;

use HS\Utils;

class Lang_Selector extends View
{
    public function __construct($args)
    {
        parent::__construct($args, [

            // 'current_lang' => 'en',
            'current_lang_url' => '',
            'lang_options' => [],
        ]);
        
        $this->setLangOptions();
        //$this->setArgsToProps(true);
    }
    
    protected function setLangOptions()
    {
        $langs = Utils::getLanguages();

        foreach($langs as $lang)
        {
            if($lang['current'])
            {
                // $this->args['current_lang'] = $lang['code'];
                $this->args['current_lang_url'] = $lang['url'];
            }
        }

        $this->args['lang_options'] = $langs;
    }
}
