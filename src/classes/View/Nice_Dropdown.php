<?php

namespace HS\View;

class Nice_Dropdown extends View
{
    public function __construct($args)
    {
        parent::__construct($args, [
            'input_name' => '',
            'input_type' => 'radio',
            'label' => '',
            'empty_name' => '',
            'selected' => '',
            'options' => [],
            'input_class' => ''
            // 'label_icon' => false
        ]);
        if(!$this->args['label'])
        {
            $this->args['label'] = $this->args['empty_name'];
        }

        $this->__setHtmlClass();

        $this->setOptions();

        //$this->setArgsToProps(true);
    }

    protected function __setHtmlClass()
    {
        $this->_addHtmlClass($this->get_input_name());
        // if($this->has_label_icon())
        // {
        //     $this->_addHtmlClass('has-label-icon');
        // }
    }

    protected function setOptions()
    {
        $_options = [];
        foreach($this->args['options'] as $option)
        {
            $_option = wp_parse_args($option, [
                'name' => '',
                'value' => '',
                'url' => '',
                'target' => '_self',
                'class' => ''
            ]);

            if($_option['name'] && ($_option['url'] || ($_option['value'] || $this->has_input_name())))
            {
                $_options[] = $_option;
            }
        }
        $this->args['options'] = $_options;
    }
}
