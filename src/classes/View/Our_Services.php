<?php

namespace HS\View;

class Our_Services extends View
{
    public function __construct($args)
    {
        parent::__construct($args, [
            
            'title' => $this->getField('title', ''),
            'list_items' => $this->getField('list', []),
            'cols_num' => $this->getField('cols_num', 3)
        ]);
        
        $this->setListItems();
    }
    
    protected function setListItems()
    {
        $list_items = [];
        foreach($this->get_list_items() as $item)
        {
            $list_items[] = [
                'title' => isset($item['service_title']) ? $item['service_title'] : '',
                'image_src' => isset($item['service_image']) ? $this->getAttachmentImageSrc($item['service_image'], 'full') : ''
            ];
        }
        $this->args['list_items'] = $list_items;
    }
}
