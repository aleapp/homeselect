<?php

namespace HS\View;

class Person_Card extends View
{
    public function __construct($args)
    {
        parent::__construct($args, [
            
            'image' => (int)$this->getField('image', 0),
            'name' => $this->getField('name', ''),
            'position' => $this->getField('position', '')
        ]);

        $this->args['image_html'] = $this->getAttachmentImageHtml($this->args['image'], 'full', 'rect-90-100');
    }
}
