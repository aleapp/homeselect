<?php

namespace HS\View;

use HS\Utils;

class Person_Card_Group extends View
{
    public function __construct($args)
    {
        parent::__construct($args, [
            
            'title' => $this->getField('title', ''),
            'items' => $this->getField('items', []),
            'cols_num' => (int)$this->getField('cols_num', 4)
            // 'more_url' => $this->getField('more_url', ''),
            // 'more_label' => $this->getField('more_label', '')
        ]);

        $this->setItems();
        // $this->__setHtmlClass();
    }
    
    protected function setItems()
    {
        $this->args['items'] = $this->args['items'] ? array_chunk($this->args['items'], $this->get_cols_num()) : [];
    }

    // protected function __setHtmlClass()
    // {
    //     if($this->has_add_seps())
    //     {
    //         $this->_addHtmlClass('has-seps');
    //     }
    // }
}
