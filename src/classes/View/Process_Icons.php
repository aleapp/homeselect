<?php

namespace HS\View;

use HS\Utils;

class Process_Icons extends View
{
    public function __construct($args)
    {
        parent::__construct($args, [
            
            'title' => $this->getField('title', []),
            'items' => $this->getField('items', []),
            'items_num' => 0,
            'add_separators' => (bool)$this->getField('add_separators', false)
        ]);
        
        $this->setItems();
        $this->__setHtmlClass();
    }
    
    protected function setItems()
    {
       $items = [];
       foreach($this->get_items() as $i => $_item)
       {
            $item = wp_parse_args($_item, [
                'icon_type' => '',
                'icon_title' => ''
            ]);
            if($item['icon_type'])
            {
                $items[] = $_item;
            }
       }

       $this->args['items'] = $items;
       $this->args['items_num'] = count($this->args['items']);
    }

    protected function __setHtmlClass()
    {
        if($this->has_add_separators())
        {
            $this->_addHtmlClass('has-seps');
        }
    }
}
