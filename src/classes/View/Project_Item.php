<?php

namespace HS\View;

use HSP\Type\Project;

class Project_Item extends View
{
    public function __construct($args)
    {
        parent::__construct($args, [

            'project' => new Project(),
            'details' => [],
            'details_num' => 0
        ]);
        
        $this->setArgs();
        
        //$this->setArgsToProps(true);
    }
    
    protected function setArgs()
    {
        $project = $this->args['project'];

        $this->args['featured_image'] = $project->getFeaturedImageId();
        $this->args['gallery_images'] = $project->getGalleryImageIds();
        $this->args['gallery_images_num'] = count($this->args['gallery_images']);

        $this->args['title'] = $project->getTitle('title');
        $this->args['description'] = $project->getDescription(true);

        
        $this->args['details'][] = [
            'key' => 'area',
            'label' => __('area', 'hs'),
            'value' => $project->getArea(),
            'value_formatted' => $project->getArea(true)
        ];
        $this->args['details'][] = [
            'key' => 'cost',
            'label' => __('cost', 'hs'),
            'value' => $project->getCost(),
            'value_formatted' => $project->getCost(true)
        ];
        $this->args['details'][] = [
            'key' => 'category',
            'label' => __('category', 'hs'),
            'value' => $project->getCategory(true),
            'value_formatted' => $project->getCategory(true)
        ];
        $this->args['details'][] = [
            'key' => 'year',
            'label' => __('year', 'hs'),
            'value' => $project->getYear(),
            'value_formatted' => $project->getYear()
        ];

        $this->args['details_num'] = count($this->args['details']);
        $this->args['details'] = array_chunk($this->args['details'], 2);
    }
}
