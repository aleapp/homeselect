<?php

namespace HS\View;

use HS\Utils_Project;

class Project_List extends View
{
    public function __construct($args)
    {
        parent::__construct($args, [
            
            'items' => null,
            'items_total' => 0,

            'q_args' => [],

            'cols_num' => 2
        ]);

        $this->args['q_args'] = wp_parse_args($this->args['q_args'], [
            'project_cat' => (int)$this->getField('project_cat', 0),
            'project_type' => (int)$this->getField('project_type', 0)
        ]);

        $this->setItems();

        $this->__setHtmlClass();

        //$this->setArgsToProps(true);
    }

    protected function __setHtmlClass()
    {
        if($this->has_show_map())
        {
            $this->_addHtmlClass('has-map');
        }
    }
    
    protected function setItems()
    {
        if(!isset($this->args['items']))
        {
            $projects = Utils_Project::getProjects($this->args['q_args'], true);
            
            $this->args['items'] = $projects['items'];
            $this->args['items_total'] = $projects['items_total'];
        }
        $this->args['items'] = array_chunk($this->args['items'], $this->args['cols_num']);
    }
}
