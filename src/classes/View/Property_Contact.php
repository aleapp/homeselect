<?php

namespace HS\View;

use HSP\Type\Property;
use HSP\Type\Agent;
use HS\Utils;

class Property_Contact extends View
{
    public function __construct($args)
    {
        parent::__construct($args, [
            
            'property' => new Property(),
            'agent' => new Agent(),
            'show_agent_meet_btn' => true,
            'contact_form_id_viewing' => (int)Utils::getThemeOption('property_contact_form_viewing'),
            'contact_form_id_agent' => (int)Utils::getThemeOption('property_contact_form_agent')
        ]);
    }
}
