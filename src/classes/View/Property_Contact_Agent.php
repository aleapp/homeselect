<?php

namespace HS\View;

use HS\Utils_Property;

class Property_Contact_Agent extends View
{
    public function __construct($args)
    {
        parent::__construct($args, [
            
            'agent_id' => 0,
            'property_id' => 0,
            'contact_form_id' => 0,
            'title' => __('Contact Us', 'hs'),
            'description' => '',
            'contact_email' => Utils_Property::getCityContactEmail(),
            'contact_phone' => Utils_Property::getCityContactPhone()
        ]);

        $this->args['description'] = sprintf(__('Fill in your details below and a member of our team will be in touch with you shortly. Alternatively you can email us on <a href="mailto:%1$s"><strong>%1$s</strong></a> or call us on <a href="%2$s"><strong>%2$s</strong></a>.', 'hs'), $this->get_contact_email(), $this->get_contact_phone());
    }
}
