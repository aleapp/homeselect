<?php

namespace HS\View;

class Property_Contact_Viewing extends View
{
    public function __construct($args)
    {
        parent::__construct($args, [
            
            'agent_id' => 0,
            'property_id' => 0,
            'contact_form_id' => 0,
            'title' => __('Arrange a Viewing', 'hs'),
            'description' => __('We will review and respond to your request within 24 hours.', 'hs')
        ]);
    }
}
