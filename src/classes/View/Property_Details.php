<?php

namespace HS\View;

use HSP\Type\Property;

class Property_Details extends View
{
    public function __construct($args)
    {
        parent::__construct($args, [
            
            'property' => new Property(),
            'cont_id_specs' => '',
            'cont_id_amenities' => '',
            'cont_id_gallery' => '',
            'top_level' => true
        ]);
    }
}
