<?php

namespace HS\View;

use HS\Utils;
use HS\Utils_Property;
use HSP\Type\Property;

use WPSEED\Req;

class Property_Filter_Price_Slider extends View
{
    public function __construct($args)
    {
        $this->req = new Req();

        parent::__construct($args, [

            'input_name' => 'sale_price',
            'price_range' => ''
        ]);

        $this->args['price_range'] = Utils::getMetaValsRange($this->args['input_name']);

        //$this->setArgsToProps(true);
    }

    public function getDefaultValue($key, $default=null)
    {
        $props_config = Property::_get_props_config();
        $cast = isset($props_config[$key]) ? $props_config[$key] : 'text';
        $req_val = $this->req->get($key, $cast, '');
        return (isset($default) && empty($req_val)) ? $default : $req_val;
    }

    public function getDefaultValueRange($key, $e=0)
    {
        $value = $this->getDefaultValue($key);
        $value_range = (strpos($value, '-') !== false) ? explode('-', $value) : [];
        return isset($value_range[$e]) ? $value_range[$e] : '';
    }

    public function getPriceRangeMin()
    {
        $range = $this->get_price_range();
        return $range['min'];
    }

    public function getPriceRangeMax()
    {
        $range = $this->get_price_range();
        return $range['max'];
    }

    public function getCurrencySymbol()
    {
        return Utils_Property::getCurrencySymbol();
    }
}
