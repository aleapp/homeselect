<?php

namespace HS\View;

use HS\Utils;

class Property_Filters_Advanced extends Property_Filters_Simple
{
    public function __construct($args)
    {
        parent::__construct($args, [
            
            'property_offer_options' => Utils::getTaxonomyTermOptions('property_offer')
        ]);
    }

    public function getOrderingOptions()
    {
        return [
            [
                'value' => 'price_low_high',
                'name' => __('Price: Low to High', 'hs')
            ],
            [
                'value' => 'price_high_low',
                'name' => __('Price: High to Low', 'hs')
            ],
            [
                'value' => 'bedrooms_less_more',
                'name' => __('Bedrooms: Feweset to Most', 'hs')
            ],
            [
                'value' => 'bedrooms_more_less',
                'name' => __('Bedrooms: Most to Fewest', 'hs')
            ]
        ];
    }

    static function getFoundSummaryText($items_total=0)
    {
        if($items_total > 1){
            return sprintf(__('We have found %d apartments.', 'hs'), $items_total);
        }elseif($items_total === 1){
            return sprintf(__('We have found %d apartment.', 'hs'), $items_total);
        }else{
            return __('We have found no apartments.', 'hs');
        }
    }
}
