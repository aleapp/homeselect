<?php

namespace HS\View;

use HS\Utils;
use HSP\Type\Property;
use HS\Utils_Property;
use HS\Localization;

use WPSEED\Req;

class Property_Filters_Simple extends View
{
    var $req;

    public function __construct($args, $default_args=[])
    {
        $this->req = new Req();

        parent::__construct($args, wp_parse_args($default_args, [

            'external_links' => [],
            'property_offer_options' => [],

            'city_options' => Utils::getTaxonomyTermOptions('city'),

            'city_default' => $this->getDefaultCityName(),

            'parish_options' => Utils::getTaxonomyTermOptions('parish', 'term_id', [
                'meta_parent_term_id' => $this->req->get(Property::getPropConfigData('parent', 'parish', 'city'), 'integer', $this->getDefaultCity()),
                'empty_opt' => Property::getPropConfigData('empty_opt', 'parish', '')
            ]),

            'bedrooms_options' => Utils::getMetaValsUnique('bedrooms', 'integer', true),
            'bathrooms_options' => Utils::getMetaValsUnique('bathrooms', 'integer', true),

            'paged' => $this->getDefaultValue('paged', 1),

            'items_total' => 0,

            'list_cont' => '.view.property-list .list-items'
        ]));

        $this->setPropertyOfferOptions();
    }

    protected function setPropertyOfferOptions()
    {
        $options = $this->has_property_offer_options() ? $this->get_property_offer_options() : Utils::getTaxonomyTermOptions('property_offer', 'term_id', [], true);

        if($this->has_external_links())
        {
            foreach($this->get_external_links() as $link)
            {
                $link = wp_parse_args($link, [
                    'link_url' => '',
                    'link_page' => 0,
                    'link_label' => ''
                ]);

                $link_url = $link['link_page'] ? get_permalink((int)$link['link_page']) : $link['link_url'];

                if($link_url)
                {
                    $options[] = [
                        'name' => $link['link_label'],
                        'url' => $link_url,
                        'target' => 'self'
                    ];
                }
            }
        }

        $this->args['property_offer_options'] = $options;
    }

    public function getListPageUrl()
    {
        return Utils_Property::getListPageUrl();
    }

    public function getDefaultValue($key, $default=null)
    {
        $props_config = Property::_get_props_config();
        $cast = isset($props_config[$key]) ? $props_config[$key] : 'text';
        $req_val = $this->req->get($key, $cast, '');
        return (isset($default) && empty($req_val)) ? $default : $req_val;
    }

    public function getDefaultCity()
    {
        $city_id = (int)$this->getDefaultValue('city');

        if($city_id)
        {
            return $city_id;
        }

        $city_config = Localization::getSessionCity();
        $city_term = $city_config ? get_term_by("slug", $city_config, 'city') : null;

        $city_id = is_a($city_term, 'WP_Term') ? $city_term->term_id : 0;

        return $city_id;
    }
}
