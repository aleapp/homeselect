<?php

namespace HS\View;

use HSP\Type\Property;

class Property_Gallery extends View
{
    public function __construct($args)
    {
        parent::__construct($args, [
            
            'property' => new Property()
        ]);
    }
}
