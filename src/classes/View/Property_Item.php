<?php

namespace HS\View;

use HSP\Type\Property;

class Property_Item extends View
{
    public function __construct($args)
    {
        parent::__construct($args, [

            'property' => new Property(),

            'gallery_images' => [],
            'gallery_images_num' => 0,
            'featured_image' => '',
            'offer' => '',
            'location' => '',
            'reference' => '',
            'specs' => [],
            'price' => 0
        ]);
        
        $this->setArgs();
        //$this->setArgsToProps(true);
        $this->_addHtmlClass([
            'p-' . $this->args['property']->getId(),
            'i-' .  $this->args['property']->getImportId()
        ]);
    }
    
    protected function setArgs()
    {
        $property = $this->args['property'];

        $this->args['gallery_images'] = $property->getGalleryImagesSrc('Thumbnail');
        $this->args['gallery_images_num'] = count($this->args['gallery_images']);
        $this->args['featured_image'] = $property->getFeaturedImageSrc('Thumbnail');
        $this->args['offer'] = $property->getOffer(true);
        $this->args['location'] = $property->getLocation();
        $this->args['reference'] = $property->getReference();
        $this->args['bedrooms'] = $property->getBedrooms();
        $this->args['bathrooms'] = $property->getBathrooms();
        $this->args['area'] = $property->getArea(true);
        $this->args['price_raw'] = $property->getPrice();
        $this->args['price'] = $property->getPrice(true);
        $this->args['gps_lat'] = $property->getGpsLat();
        $this->args['gps_lon'] = $property->getGpsLon();
    }
}
