<?php

namespace HS\View;

use HS\Utils;

use HSP\Type\Property;

class Property_Item_Map_info extends View
{
    public function __construct($args)
    {
        parent::__construct($args, [

            'property' => new Property()
        ]);
        
        $this->setArgs();
        //$this->setArgsToProps(true);
    }
    
    protected function setArgs()
    {
        $property = $this->args['property'];

        $this->args['featured_image'] = $property->getFeaturedImageSrc('Thumbnail');
        $this->args['location'] = $property->getLocation();
        $this->args['reference'] = $property->getReference();
        $this->args['price_raw'] = $property->getPrice();
        $this->args['price'] = $property->getPrice(true);
    }
}
