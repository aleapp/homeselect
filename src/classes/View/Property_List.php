<?php

namespace HS\View;

use WPSEED\Req;
use HS\Utils_Property;

class Property_List extends View
{
    var $req;

    public function __construct($args)
    {
        $this->req = new Req();

        parent::__construct($args, [
            
            'items' => null,
            'items_total' => 0,
            'set_items' => true,

            'no_found_text' => __('No properties found', 'hs'),

            'q_args' => [],

            'render_items_only' => false,

            'show_list_pager' => false,
            'list_pager_args' => [],

            'more_url' => $this->getField('more_url', ''),
            'more_label' => $this->getField('more_label', ''),

            'map_cont' => '',
            'map_zoom' => Utils_Property::PROPERTY_LIST_MAP_ZOOM,

            'show_filters' => false,
            'show_map' => false,

            'cols_num' => 2
        ]);

        $this->args['q_args'] = wp_parse_args($this->args['q_args'], [

            'property_offer' => $this->req->get('property_offer', 'text', ''),
            'city' => $this->req->get('city', 'text', ''),
            'parish' => $this->req->get('parish', 'text', ''),
            'sale_price' => $this->req->get('sale_price', 'text', ''),
            'rent_price' => $this->req->get('rent_price', 'text', ''),
            'bedrooms' => $this->req->get('bedrooms', 'integer', 0),
            'bathrooms' => $this->req->get('bathrooms', 'integer', 0),
            'paged' => $this->req->get('paged', 'integer', 1),
            'posts_per_page' => $this->getField('posts_number', Utils_Property::PROPERTY_LIST_PER_PAGE)
        ]);

        $this->__setHtmlClass();
        
        $this->setItems();

        //$this->setArgsToProps(true);
    }

    protected function __setHtmlClass()
    {
        if($this->has_show_map())
        {
            $this->_addHtmlClass('has-map');
        }
    }
    
    protected function setItems()
    {
        if($this->args['set_items'] && !isset($this->args['items']))
        {
            $properties = Utils_Property::getProperties($this->args['q_args']);
            
            $this->args['items'] = $properties['items'];
            $this->args['items_total'] = $properties['items_total'];
        }
        
        $this->args['items'] = $this->args['items'] ? array_chunk($this->args['items'], $this->args['cols_num']) : [];
    }
}
