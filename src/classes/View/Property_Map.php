<?php

namespace HS\View;

use HSP\Type\Property;

class Property_Map extends View
{
    public function __construct($args)
    {
        parent::__construct($args, [
            'property' => new Property(),
            'zoom' => 15,
            'show_area_info' => true,
            'top_level' => true
        ]);

        $this->setAreaInfo();
    }

    protected function setAreaInfo()
    {
        if(!$this->args['show_area_info'])
        {
            return;
        }

        $term_id = $this->args['property']->getParish();
        $term = $term_id ? get_term($term_id, 'parish') : null;

        if(!is_a($term, 'WP_Term'))
        {
            return;
        }

        $this->args['area_display_title'] = get_term_meta($term->term_id, 'display_title', true);
        $this->args['area_display_subtitle'] = get_term_meta($term->term_id, 'display_subtitle', true);
        $this->args['area_description'] = $term->description;
    }
}
