<?php

namespace HS\View;

use HSP\Type\Property;

class Property_Nav extends View
{
    public function __construct($args)
    {
        parent::__construct($args, [
            
            'back_link' => '',
            'back_label' => __('Go back', 'hs'),
            'items' => []
        ]);
    }
}
