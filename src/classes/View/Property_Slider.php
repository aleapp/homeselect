<?php

namespace HS\View;

use HS\Localization;

use HS\Utils_Property;

// class Property_Slider extends Property_List
class Property_Slider extends View
{
    public function __construct($args)
    {
        parent::__construct($args, [
            
            'show_pagination' => $this->getField('show_pagination', false),
            'show_nav' => $this->getField('show_nav', false),

            'q_args' => [],

            'items' => null,
            'items_total' => 0,

            'cols_num' => 1
        ]);

        $this->args['q_args'] = wp_parse_args($this->args['q_args'], [
            'property_offer' => (int)$this->getField('property_offer', 0),
            'city' => (int)$this->getField('property_city', Localization::getSessionCity(true)),
            'posts_per_page' => $this->getField('properties_number', Utils_Property::PROPERTY_LIST_PER_PAGE)
        ]);

        $this->setItems();
        //$this->setArgsToProps(true);
    }

    protected function setItems()
    {
        if(!isset($this->args['items']))
        {
            $properties = Utils_Property::getProperties($this->args['q_args']);

            $this->args['items'] = $properties['items'];
            $this->args['items_total'] = $properties['items_total'];
        }
    }
}
