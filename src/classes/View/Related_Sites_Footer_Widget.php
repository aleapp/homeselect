<?php

namespace HS\View;

class Related_Sites_Footer_Widget extends View
{
    public function __construct($args)
    {
        parent::__construct($args, [

            'related_sites' => $this->getField('related_sites', []),
            
            'more_page' => (int)$this->getField('more_page', 0),
            'more_url' => $this->getField('more_url', ''),
            'more_label' => $this->getField('more_label', __('See more', 'hs')),

            'related_sites_num' => 0,
            'related_sites_max_num' => 3,

            'top_level' => true
        ]);
        
        $this->setRelatedSites();
    }
    
    protected function setRelatedSites()
    {
        $related_sites = [];

        foreach($this->get_related_sites() as $i => $related_site)
        {
            if(($i+1) > $this->args['related_sites_max_num'])
            {
                continue;
            }

            $_related_site = wp_parse_args($related_site, [
                'site_name' => '',
                'site_url' => '',
                'site_url_target' => '_self',
            ]);
            if($_related_site['site_name'] && $_related_site['site_url'])
            {
                $related_sites[] = $_related_site;
            }
        }
        $this->args['related_sites'] = $related_sites;
        $this->args['related_sites_num'] = count($this->args['related_sites']);

        //$this->setArgsToProps(true);
    }
}
