<?php

namespace HS\View;

class Service_Type_1 extends View
{
    public function __construct($args)
    {
        parent::__construct($args, [
            
            'image' => (int)$this->getField('image', 0),
            'title' => $this->getField('title', ''),
            'description' => $this->getField('description', ''),
            'description_color' => $this->getField('description_color', 'default'),
            'align' => $this->getField('align', 'center'),
            'variant' => $this->getField('variant', 'variant-1'),
            'more_url' => $this->getField('more_url', '')
            // 'more_label' => $this->getField('more_label', __('View more', 'hs'))
        ]);
        
        $this->args['image_html'] = $this->getAttachmentImageHtml($this->get_image());
        
        $this->__setHtmlClass();
    }
    
    protected function __setHtmlClass()
    {
        if($this->has_title())
        {
            $this->_addHtmlClass('has-title');
        }

        $this->_addHtmlClass('ta-' . $this->get_align());

        $this->_addHtmlClass($this->get_variant());
    }
}
