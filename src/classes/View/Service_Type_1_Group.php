<?php

namespace HS\View;

use HS\Utils;

class Service_Type_1_Group extends View
{
    public function __construct($args)
    {
        parent::__construct($args, [
            
            'title' => $this->getField('title', ''),
            'description' => $this->getField('description', ''),
            'items' => $this->getField('items', []),
            'items_description_color' => $this->getField('items_description_color', 'default'),
            'cols_num' => (int)$this->getField('cols_num', 3),
            'variant' => $this->getField('variant', 'variant-1'),
            'align_items_content' => $this->getField('align_items_content', 'center'),
            'more_url' => $this->getField('more_url', ''),
            'more_label' => $this->getField('more_label', ''),
            'more_bg_color' => $this->getField('more_bg_color', 'dark-blue'),
            'top_line_sep' => (bool)$this->getField('top_line_sep', false)
        ]);
        
        $this->setItems();
        $this->__setHtmlClass();
    }
    
    protected function setItems()
    {
        $this->args['items'] = $this->args['items'] ? array_chunk($this->args['items'], $this->get_cols_num()) : [];
    }

    protected function __setHtmlClass()
    {
        $this->_addHtmlClass($this->get_variant());
        if($this->has_top_line_sep())
        {
            $this->_addHtmlClass('has-top-line-sep');
        }
    }
}
