<?php

namespace HS\View;

class Service_Type_2 extends View
{
    public function __construct($args)
    {
        parent::__construct($args, [
            
            'image_id' => (int)$this->getField('image', 0),
            'image_pos' => $this->getField('image_pos', 'right'),
            'image_overlay_mob' => $this->getField('image_overlay_mob', 'grey'),
            'title' => $this->getField('title', ''),
            'description' => $this->getField('description', ''),
            'more_url' => $this->getField('more_url', ''),
            'more_page' => (int)$this->getField('more_page', 0),
            'more_label' => $this->getField('more_label', __('See more', 'hs')),
            'top_level' => true
        ]);

        $this->args['image_src'] = $this->getAttachmentImageSrc($this->args['image_id'], 'full');
        
        $this->__setHtmlClass();
    }
    
    protected function __setHtmlClass()
    {
        $this->_addHtmlClass('image-pos-' . $this->get_image_pos());
        $this->_addHtmlClass('image-overlay-mob-' . $this->get_image_overlay_mob());
    }
}
