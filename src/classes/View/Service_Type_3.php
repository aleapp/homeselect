<?php

namespace HS\View;

class Service_Type_3 extends View
{
    var $image_html;
    
    public function __construct($args)
    {
        parent::__construct($args, [
            
            'image' => (int)$this->getField('image', 0),
            'title' => $this->getField('title', ''),
            'link_page' => (int)$this->getField('link_page', 0),
            'link' => $this->getField('link', ''),
        ]);

        if($this->args['link_page'])
        {
            $this->args['link'] = get_permalink($this->args['link_page']);
        }

        $this->args['image_html'] = $this->getAttachmentImageHtml($this->args['image'], 'full', 'rect-120-100');
    }


}
