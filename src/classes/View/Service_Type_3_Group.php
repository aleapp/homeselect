<?php

namespace HS\View;

use HS\Utils;

class Service_Type_3_Group extends View
{
    public function __construct($args)
    {
        parent::__construct($args, [
            
            'title' => $this->getField('title', ''),
            'items' => $this->getField('items', []),
            'cols_num' => (int)$this->getField('cols_num', 3)
        ]);
        
        $this->args['items'] = $this->args['items'] ? array_chunk($this->args['items'], $this->args['cols_num']) : [];
    }

    protected function setItems()
    {
        $items = [];
        foreach($this->get_items() as $item)
        {
            $items[] = wp_parse_args($item, [
                'image' => 0,
                'title' => '',
                'link_page' => 0,
                'link' => ''
            ]);
        }
        $this->args['items'] = $items ? array_chunk($items, $this->args['cols_num']) : [];
    }
}
