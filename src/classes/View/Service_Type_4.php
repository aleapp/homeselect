<?php

namespace HS\View;

class Service_Type_4 extends View
{
    public function __construct($args)
    {
        parent::__construct($args, [
            
            'counter_num' => (int)$this->getField('counter_num', 0),
            'title' => $this->getField('title', ''),
            'description' => $this->getField('description', ''),
            'is_last' => false
        ]);

        $this->__setHtmlClass();
    }

    protected function __setHtmlClass()
    {
        if($this->has_is_last())
        {
            $this->_addHtmlClass('is-last');
        }
    }
}
