<?php

namespace HS\View;

use HS\Utils;

class Service_Type_4_Group extends View
{
    public function __construct($args)
    {
        parent::__construct($args, [
            
            'title' => $this->getField('title', ''),
            'items' => $this->getField('items', []),
            'cols_num' => (int)$this->getField('cols_num', 3),
            'add_counters' => (bool)$this->getField('add_counters', false),
            'add_seps' => (bool)$this->getField('add_seps', false),
            'more_url' => $this->getField('more_url', ''),
            'more_label' => $this->getField('more_label', '')
        ]);
        
        $this->args['items'] = $this->args['items'] ? array_chunk($this->args['items'],  $this->args['cols_num']) : [];

        $this->__setHtmlClass();
    }
    
    protected function __setHtmlClass()
    {
        if($this->has_add_seps())
        {
            $this->_addHtmlClass('has-seps');
        }
    }
}
