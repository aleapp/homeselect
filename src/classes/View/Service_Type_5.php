<?php

namespace HS\View;

class Service_Type_5 extends View
{
    public function __construct($args)
    {
        parent::__construct($args, [
            
            'title' => $this->getField('title', ''),
            'description' => $this->getField('description', ''),
            'image' => (int)$this->getField('image', ''),
            'tagline' => $this->getField('tagline', ''),
            'more_url' => $this->getField('more_url', ''),
            'more_url_target' => $this->getField('more_url_target', ''),
            'more_label' => $this->getField('more_label', ''),
            'extra_pads' => $this->getField('extra_pads', false)
        ]);

        $this->args['image_html'] = $this->getAttachmentImageHtml($this->args['image'], 'full');

        $this->__setHtmlClass();
    }

    protected function __setHtmlClass()
    {
        if($this->has_extra_pads())
        {
            $this->_addHtmlClass('extra-pads');
        }
    }
}
