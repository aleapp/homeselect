<?php

namespace HS\View;

class Service_Type_6 extends View
{
    public function __construct($args)
    {
        parent::__construct($args, [
            
            'image' => (int)$this->getField('image', 0),
            'title' => $this->getField('title', ''),
            'description' => $this->getField('description', '')
        ]);
        
        $this->args['image_html'] = $this->args['image'] ? $this->getAttachmentImageHtml($this->args['image'], 'full', 'rect-150-100') : '';
    }
}
