<?php

namespace HS\View;

use HS\Utils;

class Service_Type_6_Group extends View
{
    public function __construct($args)
    {
        parent::__construct($args, [
            
            'title' => $this->getField('title', ''),
            'description' => $this->getField('description', ''),
            'items' => $this->getField('items', []),
            'cols_num' => (int)$this->getField('cols_num', 3)
        ]);
        
        $this->setItems();
    }
    
    protected function setItems()
    {
        $this->args['items'] = $this->args['items'] ? array_chunk($this->args['items'], $this->get_cols_num()) : [];
    }
}
