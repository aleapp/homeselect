<?php

namespace HS\View;

class Social_Links_Footer_Widget extends View
{
    public function __construct($args)
    {
        parent::__construct($args, [

            'links' => $this->getField('links', []),
            'links_max_num' => 6
        ]);
        
        $this->setLinks();
    }
    
    protected function setLinks()
    {
        $links = [];

        foreach($this->get_links() as $i => $link)
        {
            if(($i+1) > $this->args['links_max_num'])
            {
                continue;
            }

            $_link = wp_parse_args($link, [
                'type' => '',
                'url' => ''
            ]);
            if($_link['type'] && $_link['url'])
            {
                $links[] = $_link;
            }
        }

        $this->args['links'] = $links;
        $this->args['links_num'] = count($this->args['links']);

        //$this->setArgsToProps(true);
    }
}
