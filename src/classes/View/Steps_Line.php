<?php

namespace HS\View;

class Steps_Line extends View
{
    public function __construct($args)
    {
        parent::__construct($args, [
            
            'pre_title' => $this->getField('pre_title', ''),
            'title' => $this->getField('title', ''),
            'items' => $this->getField('items', []),
            // 'show_numbers' => (bool)$this->getField('show_numbers', true),
            'bg_color' => $this->getField('bg_color', 'default')
        ]);
        
        $this->setArgs();
    }
    
    protected function setArgs()
    {
        $items = [];
        foreach($this->get_items() as $i => $item)
        {
            $items[$i] = wp_parse_args($item, [
                'label' => '',
                'description' => ''
            ]);
        }

        $this->args['items'] = $items;

        //$this->setArgsToProps(true);
    }
}
