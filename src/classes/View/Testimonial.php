<?php

namespace HS\View;

class Testimonial extends View
{
    public function __construct($args)
    {
        parent::__construct($args, [
            
            'customer_image_id' => (int)$this->getField('customer_image', 0),
            'customer_name' => $this->getField('customer_name', ''),
            'customer_title' => $this->getField('customer_title', ''),
            'customer_quote' => $this->getField('customer_quote', '')
        ]);

        $this->setImage();
    }
    
    protected function setImage()
    {
        $this->args['customer_image_html'] = $this->getAttachmentImageHtml($this->get_customer_image_id(), 'thumbnail', '');
        $this->args['customer_image_src'] = $this->getAttachmentImageSrc($this->get_customer_image_id(), 'thumbnail');
    }
}
