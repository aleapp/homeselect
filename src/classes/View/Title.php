<?php

namespace HS\View;

class Title extends View
{
    public function __construct($args)
    {
        parent::__construct($args, [
            
            'pre_title' => $this->getField('pre_title', ''),
            'title' => $this->getField('title', ''),
            'h_type' => $this->getField('h_type', 'h2'),
            'color' => $this->getField('color', ''),
            // 'bg_color' => $this->getField('bg_color', 'white'),
            'alignment' => $this->getField('alignment', 'left'),
            'variant' => $this->getField('variant', 'section-title')
        ]);
        
        $this->__setHtmlClass();
    }
    
    protected function __setHtmlClass()
    {
        if($this->has_color())
        {
            $this->_addHtmlClass('tc-' . $this->get_color());
        }
        if($this->has_alignment())
        {
            $this->_addHtmlClass('ta-' . $this->get_alignment());
        }
        if($this->has_variant())
        {
            $this->_addHtmlClass('variant-' . $this->get_variant());
        }
    }
}
