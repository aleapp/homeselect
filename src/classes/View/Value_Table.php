<?php

namespace HS\View;

class Value_Table extends View
{
    public function __construct($args)
    {
        parent::__construct($args, [
            
            'header_row' => $this->getField('header_row', []),
            'data_rows' => $this->getField('data_rows', [])
        ]);
        
        $this->args['num_cols'] = count($this->args['header_row']);
    }
}
