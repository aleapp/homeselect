<?php

namespace HS\View;

use HS\Utils;

class View extends \WPSEED\View 
{
    public function __construct($args, $default_args=[])
    {
        $default_args = wp_parse_args($default_args, [

            'id' => $this->getField('id', ''),
            'html_class' => $this->getField('html_class', ''),
            'hide' => $this->getField('hide', false),
            'hide_mobile' => (bool)$this->getField('hide_mobile', false),
            'hide_desktop' => (bool)$this->getField('hide_desktop', false),
            'top_level' => $this->getField('top_level', false),
            'margin_bottom' => $this->getField('margin_bottom', 'standard'),
            'bg_color' => $this->getField('bg_color', ''),
            'container_class' => $this->getField('container_class', 'container-lg'),
            'container_narrow' => (bool)$this->getField('container_narrow', false)
        ]);

        parent::__construct($args, $default_args);

        $this->_setHtmlClass();

        // $this->setPropsToArgs(true);
    }
    
    protected function getField($name, $default=null)
    {
        $_name = 'hs__' . $this->getName(true) . '__' . $name;
        
        $field = get_field($_name);
        
        return (empty($field) && isset($default)) ? $default : $field;
    }

    protected function _addHtmlClass($class)
    {
        if(is_string($this->args['html_class']))
        {
            $this->args['html_class'] = empty($this->args['html_class']) ? [] : explode(' ', $this->args['html_class']);
        }
        if(is_string($class))
        {
            $class = empty($class) ? [] : explode(' ', $class);
        }

        $this->args['html_class'] = array_merge($this->args['html_class'], $class);
    }
    
    protected function _setHtmlClass()
    {
        if($this->args['bg_color'] !== '')
        {
            $bg_color = (strpos($this->args['bg_color'], 'bg-') === 0) ? $this->args['bg_color'] : 'bg-' . $this->args['bg_color'];
            $this->_addHtmlClass($bg_color);
        }
        
        if($this->args['top_level'])
        {
            $this->_addHtmlClass('section');
        }
        
        if($this->args['margin_bottom'] !== 'standard')
        {
            $this->_addHtmlClass('mb-' . $this->args['margin_bottom']);
        }

        if($this->args['hide_mobile'])
        {
            $this->_addHtmlClass('hide-mobile');
        }
        if($this->args['hide_desktop'])
        {
            $this->_addHtmlClass('hide-desktop');
        }
    }
    
    public function _getHtmlClass($class=null)
    {
        if(isset($class))
        {
            $this->_addHtmlClass($class);
        }

        return $this->getHtmlClass($this->args['html_class']);
    }
    
    protected function getAdminPostId()
    {
        return (is_admin() && isset($_GET['post'])) ? (int)$_GET['post'] : ((is_admin() && isset($_POST['post_id'])) ? (int)$_POST['post_id'] : 0);
    }
    
    public function encodeFieldToJson($field_name)
    {
        echo json_encode(is_array($this->$field_name) ? $this->$field_name : []);
    }
    
    public function isAdmin()
    {
        return Utils::isAdmin();
    }
    
    public function getViewTag()
    {
        return $this->has_top_level() ? 'section' : 'div';
    }
    
    public function getContainerTagOpen($type='lg')
    {
        $tag_open = $this->has_top_level() ? '<div class="container-' . $type . '">' : '';

        if($tag_open && $this->has_container_narrow())
        {
            $tag_open .= '<div class="cont-narrow">';
        }

        return $tag_open;
    }
    
    public function getContainerTagClose()
    {
        $tag_close = $this->has_top_level() ? '</div><!-- .container -->' : '';

        if($tag_close && $this->has_container_narrow())
        {
            $tag_close = '</div><!-- .cont-narrow -->' . $tag_close;
        }

        return $tag_close;
    }

    static function implodeAtts($atts)
    {
        $_atts = [];

        if(!empty($atts) && is_array($atts))
        {
            foreach($atts as $att_name => $att)
            {
                $_att = is_array($att) ? implode(' ', $att) : $att;
                $_atts[] = $att_name . '="' . $_att . '"';
            }
        }

        return $_atts ? implode(' ', $_atts) : '';
    }

    static function getAttachmentImage($attachment_id, $size='full')
    {
        return $attachment_id ? wp_get_attachment_image($attachment_id, $size) : '';
    }

    static function getAttachmentImageHtml($attachment_id, $size='full', $rel_class='rect-150-100', $fit_type='cover')
    {
        $img_cont = '<div class="img-resp-cont ' . $rel_class . ' img-' . $fit_type . '">';
            $img_cont .= is_int($attachment_id) ? self::getAttachmentImage($attachment_id, $size) : '<img src="' . $attachment_id . '" />';
        $img_cont .= '</div>';

        return $img_cont;
    }

    static function getAttachmentImageSrc($attachment_id, $size='full')
    {
        $image_src = $attachment_id ? wp_get_attachment_image_src($attachment_id, $size) : [];
        return ($image_src && isset($image_src[0])) ? $image_src[0] : '';
    }

    static function getBgImageHtml($src, $rel_class, $fit_class='bg-img-cover', $atts=[])
    {
        $_src = is_int($src) ? self::getAttachmentImageSrc($src) : $src;

        if(isset($atts['class']))
        {
            $atts['class'] .= ' ' . $rel_class . ' ' . $fit_class;
        }
        else{
            $atts['class'] = 'bg-img ' . $rel_class . ' ' . $fit_class;
        }

        $_atts = self::implodeAtts($atts);

        return $_src ? '<div ' . $_atts . ' style="background-image: url(' . $_src . ')"></div>' : '';
    }

    public function getAdminEditButton()
    {
        if((!wp_doing_ajax() && is_admin()) || (wp_doing_ajax() && isset($_SERVER['HTTP_REFERER']) && strpos($_SERVER['HTTP_REFERER'], '/wp-admin') !== false))
        {
            echo '<div class="block-edit-handle">';
                echo '<span class="edit-handle">' . $this->getName() . '</span>';
            echo '</div>';
        }
    }
}
