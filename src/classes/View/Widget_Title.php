<?php

namespace HS\View;

class Widget_Title extends View
{
    public function __construct($args)
    {
        parent::__construct($args, [
            
            'title' => $this->getField('title', '')
        ]);
    }
}
