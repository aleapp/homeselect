<?php

/*
 * Autoload classes
 * -------------------------
 */
wpseed_load_dir_classes(dirname(__FILE__) . '/Action', '\HS\Action');
wpseed_load_dir_classes(dirname(__FILE__) . '/Filter', '\HS\Filter');
wpseed_load_dir_classes(dirname(__FILE__) . '/Customizer', '\HS\Customizer');
