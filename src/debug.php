<?php

/*
 * Simple debug function with pre output
 */
function hs_debug_pre($var)
{
    echo '<pre>';
        print_r($var);
    echo '</pre>';
}

/*
 * Simple debug function
 */
function hs_debug($var, $append=false, $ip=null)
{
    if(!(!isset($ip) || (isset($_SERVER['REMOTE_ADDR']) && $_SERVER['REMOTE_ADDR'] == $ip)))
    {
        return;
    }
    
    $file_path = ABSPATH . '/__debug.txt';

    if(is_array($var) || is_object($var))
    {
        if($append)
        {
            file_put_contents($file_path, print_r($var, true), FILE_APPEND);
        }
        else
        {
            file_put_contents($file_path, print_r($var, true));
        }
    }
    else
    {
        if($append)
        {
            file_put_contents($file_path, $var, FILE_APPEND);
        }
        else
        {
            file_put_contents($file_path, $var);
        }
    }
}

function hs_is_debug_ip($ip)
{
    return (isset($_SERVER['REMOTE_ADDR']) && $_SERVER['REMOTE_ADDR'] == $ip);
}
