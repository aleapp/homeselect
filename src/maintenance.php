<?php

/*
 * Redirect user to login on maintenance
 * ----------------------------------------
 */
add_action('init', 'hs_redirect_to_login_maintenance');

function hs_redirect_to_login_maintenance()
{
    if(!HS_MAINTENANCE)
    {
        return;
    }
    
    $is_login = (isset($_SERVER['REQUEST_URI']) && strpos($_SERVER['REQUEST_URI'], '/wp-login.php') !== false);
    
    if(!is_user_logged_in() && !$is_login)
    {
        $login_url = wp_login_url(get_site_url());
        wp_redirect($login_url);
        exit;
    }
}
