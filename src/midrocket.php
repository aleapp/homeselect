<?php 

add_action('wp_footer', function(){
	?>

<script text="text/javascript">
	jQuery(document).ready(function(){
		var ventas 				= ['Compra', 'Venta', 'Selling'];
		var alquiler 			= ['Alquiler', 'Renting'];
		var nuevasPropiedades 	= ['Nuevas propiedades', 'Interiorismo', 'Otros', 'New Properties', 'Interior Design', 'Other'];
        var allCountries        = [];
        var unusedCountries     = [];
        var usedCountries       = [];

        // All countries
        jQuery('select[name="contact-country-code"] option').each(function(){
            allCountries.push(jQuery(this).val());
        });

		// Peru
		var options = {};
        options.peru = {};
		options.peru.subject = ventas.concat(alquiler);
		options.peru.country = ['Perú', 'Peru'];
        options.peru.email   = ['peru.ventas@homeselect.com'];
        usedCountries        = usedCountries.concat(options.peru.country);

		// Mexico
        options.mexico = {};
		options.mexico.subject = ventas.concat(alquiler);
		options.mexico.country = ['Méjico', 'Mexico'];
        options.mexico.email   = ['mexico.ventas@homeselect.com'];
        usedCountries           = usedCountries.concat(options.mexico.country);

        // All unused countries
        jQuery('select[name="contact-country-code"] option').each(function(){
            if(jQuery.inArray(jQuery(this).val(), usedCountries) == -1){
                unusedCountries.push(jQuery(this).val());
            }
        });

		// Madrid Ventas
        options.madridv = {};
		options.madridv.subject = ventas;
		options.madridv.country = unusedCountries;
        options.madridv.email   = ['madrid.ventas@homeselect.com'];

		// Madrid Ventas
        options.madrida = {};
		options.madrida.subject = alquiler;
		options.madrida.country = unusedCountries;
        options.madrida.email   = ['madrid.alquiler@homeselect.com'];

		// Nuevas Propiedades
        options.nuevasp = {};
		options.nuevasp.subject = nuevasPropiedades;
		options.nuevasp.country = allCountries;
        options.nuevasp.email   = ['nuevaspropiedades@homeselect.com'];
		
        if(jQuery('input[name="email-recipient"]').length){
            jQuery(".contact-form-cont form").change(function() {
                var selectedSubject = jQuery('select[name="contact-subject"]').children('option:selected').val();
                var selectedCountry = jQuery('select[name="contact-country-code"]').children('option:selected').val();
                jQuery.each( options, function( country, arrayOpts ) {
                    if(jQuery.inArray(selectedSubject, arrayOpts.subject) !== -1 && jQuery.inArray(selectedCountry, arrayOpts.country) !== -1){
                        jQuery('input[name="email-recipient"]').val(arrayOpts.email);
                    }
                });
            }); 
        }

		
	});
</script>

<?php
});