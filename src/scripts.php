<?php

/*
 * Register scripts
 * ----------------------------------------
 */
add_action('init', 'hs_register_scripts');

function hs_register_scripts()
{
    wp_register_script('bootstrap', 'https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js', [], null);
    wp_register_script('swiper-7', HS_INDEX . '/assets/swiper/swiper-bundle.min.js', [], HS_VERSION);

    wp_register_script('ion-range-slider', 'https://cdnjs.cloudflare.com/ajax/libs/ion-rangeslider/2.3.1/js/ion.rangeSlider.min.js', ['jquery'], null);

    wp_register_script('hs-views', HS_INDEX . '/assets/js/views.js', ['jquery'], HS_VERSION);
}

/*
 * Register styles
 * ----------------------------------------
 */
add_action('init', 'hs_register_styles');

function hs_register_styles()
{
    wp_register_style('bootstrap-reboot', 'https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap-reboot.min.css', [], null);
    wp_register_style('bootstrap-grid', 'https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap-grid.min.css', [], null);
    wp_register_style('bootstrap-utils', 'https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap-utilities.min.css', [], null);
    wp_register_style('bootstrap', 'https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css', [], null);
    
    wp_register_style('swiper-7', HS_INDEX . '/assets/swiper/swiper-bundle.min.css', [], HS_VERSION);

    wp_register_style('ion-range-slider', 'https://cdnjs.cloudflare.com/ajax/libs/ion-rangeslider/2.3.1/css/ion.rangeSlider.min.css', [], null);

    wp_register_style('fontawesome-5-free', HS_INDEX . '/assets/fonts/fontawesome-5-free/css/all.min.css', [], null);

    wp_register_style('hs-admin', HS_INDEX . '/assets/css/admin.css', [], HS_VERSION);
    
    wp_register_style('hs-style', HS_INDEX . '/style.css', [], HS_VERSION);
    wp_register_style('hs-common', HS_INDEX . '/assets/css/common.css', [], HS_VERSION);
    wp_register_style('hs-views', HS_INDEX . '/assets/css/views.css', [], HS_VERSION);
    wp_register_style('hs-templates', HS_INDEX . '/assets/css/templates.css', [], HS_VERSION);

    wp_register_style('hs-midrocket', get_site_url() . '/resources/style.css', [], null);
}

/*
 * Enqueue scripts on admin
 * ----------------------------------------
 */
add_action('admin_enqueue_scripts', 'hs_enqueue_scripts_admin');

function hs_enqueue_scripts_admin()
{
    $screen = get_current_screen();
    
    if(isset($screen) && in_array($screen->id, ['page', 'wp_block', 'widgets']))
    {
        wp_enqueue_script('swiper-7');
        wp_enqueue_script('hs-views');
    }
}

/*
 * Enqueue styles on admin
 * ----------------------------------------
 */
add_action('admin_enqueue_scripts', 'hs_enqueue_styles_admin');

function hs_enqueue_styles_admin()
{
    $screen = get_current_screen();
    
    if(isset($screen) && in_array($screen->id, ['page', 'wp_block', 'widgets']))
    {
        wp_enqueue_style('bootstrap-grid');
        wp_enqueue_style('bootstrap-utils');
        wp_enqueue_style('bootstrap');
        wp_enqueue_style('swiper-7');

        wp_enqueue_style('fontawesome-5-free');
        
        wp_enqueue_style('hs-common');
        wp_enqueue_style('hs-views');
        wp_enqueue_style('hs-admin');
    }
}

/*
 * Enqueue scripts on front
 * ----------------------------------------
 */

add_action('wp_enqueue_scripts', 'hs_enqueue_scripts');

function hs_enqueue_scripts()
{
    wp_enqueue_script('bootstrap');
    wp_enqueue_script('swiper-7');
    // wp_enqueue_script('google-maps');

    wp_enqueue_script('ion-range-slider');

    wp_enqueue_script('hs-views');
    wp_localize_script('hs-views', 'hsViewsVars', apply_filters('hs_views_vars', [
        'ajaxurl' => admin_url('admin-ajax.php'),
        'googlemaps_api_key' => \HS\Utils::getThemeOption('property_googlemaps_api_key', ''),
        'googlemaps_marker_image' => HS_INDEX . '/assets/images/map-marker.png'
    ]));
}

/*
 * Enqueue styles on front
 * ----------------------------------------
 */

add_action('wp_enqueue_scripts', 'hs_enqueue_styles');

function hs_enqueue_styles()
{
    // wp_enqueue_style('bootstrap-reboot');
    // wp_enqueue_style('bootstrap-grid');
    // wp_enqueue_style('bootstrap-utils');
    wp_enqueue_style('bootstrap');
    wp_enqueue_style('swiper-7');

    wp_enqueue_style('ion-range-slider');

    wp_enqueue_style('fontawesome-5-free');
    
    wp_enqueue_style('hs-style');
    wp_enqueue_style('hs-common');
    wp_enqueue_style('hs-views');
    wp_enqueue_style('hs-templates');

    wp_enqueue_style('hs-midrocket');
}
