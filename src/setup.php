<?php

/*
 * Load theme textdomain
 * ----------------------------------------
 */
add_action('after_setup_theme', 'hs_load_textdomain');

function hs_load_textdomain()
{
    load_theme_textdomain('hs', HS_DIR . '/langs');
}

/*
 * Theme support
 * ----------------------------------------
 */
add_action('after_setup_theme', 'hs_add_theme_support');

function hs_add_theme_support()
{
    add_theme_support('title-tag');
    
    add_theme_support(
        'html5',
        array(
            'comment-form',
            'comment-list',
            'gallery',
            'caption',
            'style',
            'script',
            'navigation-widgets',
        )
    );
    
    add_theme_support('post-thumbnails');
    set_post_thumbnail_size(900, 600);

    add_theme_support(
        'custom-logo',
        array(
            'width'                => 300,
            'height'               => 100,
            'flex-width'           => true,
            'flex-height'          => true,
            'unlink-homepage-logo' => true
        )
    );

    add_theme_support('customize-selective-refresh-widgets');

    // add_image_size('thumbnail-hs', 640, 480, true);
}

/*
 * Register menus
 * ----------------------------------------
 */
add_action('after_setup_theme', 'hs_register_menus');

function hs_register_menus()
{
    register_nav_menus([
        'primary' => esc_html__('Primary', 'hs'),
        'mobile' => esc_html__('Mobile', 'hs')
    ]);
}

/*
 * Setup values for oboyda/wp-seed package
 * ----------------------------------------
 */
add_filter('wpseed_views_dir', 'hs_filter_views_dir');

function hs_filter_views_dir()
{
    return HS_DIR . '/src/views';
}

add_filter('wpseed_views_namespace', 'hs_filter_views_namespace');

function hs_filter_views_namespace()
{
    return '\HS\View';
}
