<?php

function hs_parse_post_block_content($post)
{
    $_post = is_int($post) ? get_post($post) : $post;

    if(is_a($_post, 'WP_Post') && $_post->post_content)
    {
        $blocks = parse_blocks($_post->post_content);
        foreach($blocks as $block)
        {
            echo render_block($block);
        }
    }
}