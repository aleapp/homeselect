<<?php echo $view->getViewTag(); ?> class="<?php echo $view->_getHtmlClass(); ?>" data-view="<?php echo $view->getName(); ?>">

    <?php echo $view->getAdminEditButton(); ?>

    <div class="agent-info">
        <?php if($view->has_image_html()): ?>
        <div class="agent-image">
            <?php echo $view->get_image_html(); ?>
        </div>
        <?php endif; ?>

        <?php if($view->has_full_name()): ?>
        <p class="agent-name">
            <?php echo $view->get_full_name(); ?>
        </p>
        <?php endif; ?>

        <?php if($view->has_contact_phone()): ?>
        <p class="agent-phone">
            <span><?php _e('Call me', 'hs'); ?>:</span> 
            <a href="tel:<?php echo $view->get_contact_phone_href(); ?>"><?php echo $view->get_contact_phone(); ?></a>
        </p>
        <?php endif; ?>

        <?php if($view->has_contact_email()): ?>
        <p class="agent-email">
            <a href="mailto:<?php echo $view->get_contact_email(); ?>"><?php echo $view->get_contact_email(); ?></a>
        </p>
        <?php endif; ?>
    </div>

    <?php if($view->has_contact_phone()): ?>
    <div class="agent-contact d-lg-none">
        <a href="tel:<?php echo $view->has_contact_phone_href(); ?>" class="btn-t btn-full-w btn-light-blue">
            <?php _e('Contact agent', 'hs'); ?>
        </a>
    </div>
    <?php endif; ?>
    <div class="agent-contact d-none d-lg-block">
        <button class="btn-contact-agent btn-t btn-full-w btn-white" data-contact_modal="#<?php echo $view->getId(); ?>-contact-modal">
            <?php _e('Contact agent', 'hs'); ?>
        </button>
    </div>

</<?php echo $view->getViewTag(); ?>>
