<<?php echo $view->getViewTag(); ?> class="<?php echo $view->_getHtmlClass(); ?>" 
    data-view="<?php echo $view->getName(); ?>" 
    data-show_pagination="<?php echo (int)$view->has_show_pagination(); ?>" 
    data-show_nav="<?php echo (int)$view->has_show_nav(); ?>" 
    >

    <?php echo $view->getAdminEditButton(); ?>
    
    <?php echo $view->getContainerTagOpen(); ?>
        
        <div class="banner-slider-cont">
            <?php if($view->has_slide_items()): ?>
            
            <div class="swiper">
                <div class="swiper-wrapper">
                    <?php foreach($view->get_slide_items() as $item): ?>
                    <div class="swiper-slide">
                        <div class="slide-banner abs-full bg-img-cover" style="<?php echo $item['banner_image'] ? 'background-image: url(' .  $item['banner_image'] . ')' : ''; ?>"></div>
                    </div>
                    <?php endforeach; ?>
                </div>
            </div>
            
            <?php if($view->has_show_pagination()): ?>
            <div class="swiper-pagination"></div>
            <?php endif; ?>

            <?php if($view->has_show_nav()): ?>
            <div class="swiper-button-prev"></div>
            <div class="swiper-button-next"></div>
            <?php endif; ?>

            <?php endif; ?>
        </div>
        
    <?php echo $view->getContainerTagClose(); ?>

</<?php echo $view->getViewTag(); ?>>
