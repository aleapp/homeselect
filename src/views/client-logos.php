<<?php echo $view->getViewTag(); ?> class="<?php echo $view->_getHtmlClass(); ?>">

    <?php echo $view->getAdminEditButton(); ?>
    
    <?php echo $view->getContainerTagOpen(); ?>

    <?php 
    if($view->has_title()):
        wpseed_print_view('title', [
            'title' => $view->get_title(),
            'alignment' => 'center',
            'variant' => 'section-title',
            'h_type' => 'h2'
        ]);
    endif;
    ?>
    
    <?php if($view->has_logos()): foreach($view->get_logos() as $logos_row): ?>
    <div class="logos-row">
        <?php foreach($logos_row as $logo_html): ?>
        <div class="client-logo"><?php echo $logo_html; ?></div>
        <?php endforeach; ?>
    </div>
    <?php endforeach; endif; ?>
        
    <?php echo $view->getContainerTagClose(); ?>
        
</<?php echo $view->getViewTag(); ?>>
