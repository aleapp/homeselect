<<?php echo $view->getViewTag(); ?> class="<?php echo $view->_getHtmlClass(); ?>">

    <?php echo $view->getAdminEditButton(); ?>

    <?php echo $view->getContainerTagOpen(); ?>

        <?php if($view->has_title()): ?>
            <?php if(in_array($view->get_variant(), ['widget', 'widget-footer'])): ?>
                <?php wpseed_print_view('widget-title', ['title' => $view->get_title()]); ?>
            <?php else: ?>
                <h4 class="address-title"><?php echo $view->get_title(); ?></h4>
            <?php endif; ?>
        <?php endif; ?>

        <?php if($view->has_address()): ?>
        <div class="address-info">
            <?php echo wpautop($view->get_address()); ?>
        </div>
        <?php endif; ?>

        <?php if($view->has_phones()): ?>
        <ul class="address-phones ns">
            <?php foreach($view->get_phones() as $phone): ?>
                <li>
                    <?php if($phone['number']): ?>
                        <?php if($phone['label']): ?>
                            <span class="phone-label meta-label"><?php echo $phone['label']; ?>:</span>
                        <?php endif; ?>
                        <a href="tel:<?php echo $phone['number_tel']; ?>" class="phone-number meta-value"><?php echo $phone['number']; ?></a>
                    <?php endif; ?>
                </li>
            <?php endforeach; ?>
        </ul>
        <?php endif; ?>

        <?php if($view->has_emails()): ?>
        <ul class="address-emails ns">
            <?php foreach($view->get_emails() as $email): ?>
                <li>
                    <?php if($email['email']): ?>
                        <?php if($email['label']): ?>
                            <span class="email-label meta-label"><?php echo $email['label']; ?>:</span>
                        <?php endif; ?>
                        <a href="mailto:<?php echo $email['email']; ?>" class="email meta-value"><?php echo $email['email']; ?></a>
                    <?php endif; ?>
                </li>
            <?php endforeach; ?>
        </ul>
        <?php endif; ?>

        <?php if($view->has_working_hours()): ?>
        <ul class="working-hours ns">
            <?php foreach($view->get_working_hours() as $hours): ?>
                <li>
                    <?php if($hours['hours']): ?>
                        <?php if($hours['day']): ?>
                            <span class="day-label meta-label"><?php echo $hours['day']; ?>:</span>
                        <?php endif; ?>
                        <span class="hours meta-value"><?php echo $hours['hours']; ?></span>
                    <?php endif; ?>
                </li>
            <?php endforeach; ?>
        </ul>
        <?php endif; ?>

    <?php echo $view->getContainerTagClose(); ?>

</<?php echo $view->getViewTag(); ?>>
