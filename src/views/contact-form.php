<<?php echo $view->getViewTag(); ?> class="<?php echo $view->_getHtmlClass(); ?>">

    <?php echo $view->getAdminEditButton(); ?>

    <?php echo $view->getContainerTagOpen(); ?>

        <?php 
        if($view->has_title()):
            wpseed_print_view('title', [
                'title' => $view->get_title(),
                'alignment' => 'center',
                'variant' => 'section-title',
                'h_type' => 'h2',
                'headline_text' => $view->get_description()
            ]);
        endif;
        ?>

        <?php if($view->has_address_info()): ?>
        <div class="row g-0">
            <div class="col-lg-9">
        <?php endif; ?>

        <div class="contact-form-cont">
            <?php
            if($view->has_cf7_id())
            {
                echo do_shortcode('[contact-form-7 id="' . $view->get_cf7_id() . '"]');
            }
            ?>
        </div>

        <?php if($view->has_address_info()): ?>
            </div>
            <div class="col-lg-3">
        <?php endif; ?>

        <div class="address-info-cont">
            <?php 
            if($view->get_address_info_shortcode()):
                echo do_shortcode($view->get_address_info_shortcode()); 
            else:
                echo wpautop($view->get_address_info()); 
            endif;
            ?>
        </div>

        <?php if($view->has_address_info()): ?>
            </div>
        </div>
        <?php endif; ?>

    <?php echo $view->getContainerTagClose(); ?>

</<?php echo $view->getViewTag(); ?>>
