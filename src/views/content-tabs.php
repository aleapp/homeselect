<<?php echo $view->getViewTag(); ?> class="<?php echo $view->_getHtmlClass(); ?>" data-view="<?php echo $view->getName(); ?>">

    <?php echo $view->getAdminEditButton(); ?>
    
    <?php echo $view->getContainerTagOpen(); ?>
        
        <?php 
        if($view->has_title()):
            wpseed_print_view('title', [
                'title' => $view->get_title(),
                'headline_text' => $view->get_headline_text(),
                'alignment' => 'center',
                'variant' => 'section-title',
                'h_type' => 'h4'
            ]);
        endif;
        ?>
        
        <ul class="tabs ns">
            <?php 
            foreach($view->get_tab_items() as $i => $tab_item): 
                if(!empty($tab_item['tab_title'])): 
                    $active_class = ($i === 0) ? ' active' : '';
                    ?>
            <li class="tab<?php echo $active_class; ?>"><?php echo $tab_item['tab_title']; ?></li>
            <?php endif; endforeach; ?>
        </ul>
        
        <div class="tabs-content">
            <?php 
            foreach($view->get_tab_items() as $i => $tab_item): 
                $active_class = ($i === 0) ? ' active visible' : '';
                ?>
            
            <div class="tab-content<?php echo $active_class; ?>">
                
                <?php if(!empty($tab_item['tab_intro'])): ?>
                <div class="intro">
                    <?php echo wpautop($tab_item['tab_intro']); ?>
                </div>
                <?php endif; ?>
                
                <div class="content">
                    <?php 
                    if(!empty($tab_item['tab_wp_block'])):
                        echo hs_parse_post_block_content((int)$tab_item['tab_wp_block']);
                    elseif(!empty($tab_item['tab_shortcode'])):
                        echo do_shortcode($tab_item['tab_shortcode']);
                    elseif(!empty($tab_item['tab_content'])):
                        echo wpautop($tab_item['tab_content']); 
                    endif;
                    ?>
                </div>

                <?php 
                $link_url = $tab_item['tab_more_url'] ? $tab_item['tab_more_url'] : ($tab_item['tab_more_page'] ? get_permalink((int)$tab_item['tab_more_page']) : '');
                if($link_url): ?>
                <div class="more ta-center">
                    <a class="btn-t btn-large btn-dark-blue" href="<?php echo $link_url; ?>"><?php echo $tab_item['tab_more_label']; ?></a>
                </div>
                <?php endif; ?>
            </div>
            <?php endforeach; ?>
        </div>
    
    <?php echo $view->getContainerTagClose(); ?>

</<?php echo $view->getViewTag(); ?>>
