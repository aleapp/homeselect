<<?php echo $view->getViewTag(); ?> class="<?php echo $view->_getHtmlClass(); ?>">

    <?php echo $view->getAdminEditButton(); ?>

    <?php echo $view->getContainerTagOpen(); ?>

    <?php if($view->has_copyright_text()): ?>

        <p class="copyright-text">
            <?php echo $view->get_copyright_text(); ?>
        </p>

    <?php endif; ?>

    <?php echo $view->getContainerTagClose(); ?>
        
</<?php echo $view->getViewTag(); ?>>
