<?php echo $view->getAdminEditButton(); ?>

<?php if($view->has_url()): ?>
<a href="<?php echo $view->get_url(); ?>" target="<?php echo $view->get_url_target(); ?>" class="<?php echo $view->_getHtmlClass(); ?>">
<?php else: ?>
<button type="button" class="<?php echo $view->_getHtmlClass(); ?>">
<?php endif; ?>
    <span class="btn-label"><?php echo $view->get_label(); ?></span>
<?php if($view->has_url()): ?>
</a>
<?php else: ?>
</button>
<?php endif; ?>