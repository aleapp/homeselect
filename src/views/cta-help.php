<<?php echo $view->getViewTag(); ?> class="<?php echo $view->_getHtmlClass(); ?>">

    <?php if($view->has_bg_image_url()): ?>
    <div class="abs-full bg-img-cover bg-image" style="<?php echo 'background-image: url(' . $view->get_bg_image_url() . ')'; ?>"></div>
    <div class="abs-full bg-overlay"></div>
    <?php endif; ?>
    
    <?php echo $view->getAdminEditButton(); ?>
    
    <?php echo $view->getContainerTagOpen(); ?>
    
        <div class="cta-content">
            
            <?php
            if($view->has_title()):
                wpseed_print_view('title', [
                    'title' => $view->get_title(),
                    'alignment' => 'center',
                    'h_type' => 'h2'
                ]);
            endif;
            ?>

            <?php if($view->has_description()): ?>
            <div class="description"><?php echo wpautop($view->get_description()); ?></div>
            <?php endif; ?>

            <?php if($view->has_more_url()): ?>
            <div class="more-info ta-center">
                <a class="btn-t btn-small btn-dark-blue" href="<?php echo $view->get_more_url(); ?>"><?php echo $view->get_more_label(); ?></a>
            </div>
            <?php endif; ?>
            
        </div>
        
    <?php echo $view->getContainerTagClose(); ?>
        
</<?php echo $view->getViewTag(); ?>>
