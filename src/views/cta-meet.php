<<?php echo $view->getViewTag(); ?> class="<?php echo $view->_getHtmlClass(); ?>">

    <?php echo $view->getAdminEditButton(); ?>
    
    <?php echo $view->getContainerTagOpen('fluid'); ?>
        
        <div class="row g-0">
            <div class="col-lg-5">
                <?php if($view->has_main_image_url()): ?>
                <div class="main-image h-full bg-img-cover d-none d-lg-block" style="<?php echo 'background-image: url(' . $view->get_main_image_url() . ')'; ?>"></div>
                <?php endif; ?>
            </div>
            <div class="col-lg-7">
                <div class="row g-0">
                    <div class="col-xl-6 col-xxl-4">
                        <div class="title-content">
                            <?php if($view->has_pre_title()): ?>
                            <p class="pre-title"><?php echo $view->get_pre_title(); ?></p>
                            <?php endif; ?>
                            <?php if($view->has_title()): ?>
                            <h2 class="title"><?php echo $view->get_title(); ?></h2>
                            <?php endif; ?>
                            <?php if($view->has_second_image_html()): ?>
                            <div class="second-imaged d-none d-lg-block"><?php echo $view->get_second_image_html(); ?></div>
                            <?php endif; ?>
                        </div>
                    </div>
                    <div class="col-xl-6 col-xxl-5">
                        <div class="description-content">
                            
                            <?php if($view->has_description()): ?>
                            <div class="description"><?php echo wpautop($view->get_description()); ?></div>
                            <?php endif; ?>

                            <?php 
                            $link_url = $view->has_more_url() ? $view->get_more_url() : ($view->has_more_page() ? get_permalink($view->get_more_page()) : '');
                            if($link_url): 
                                ?>
                            <div class="more-info">
                                <a class="btn-t btn-full-w btn-dark-blue" href="<?php echo $link_url; ?>"><?php echo $view->get_more_label(); ?></a>
                                
                                <?php if($view->has_main_image_url()): ?>
                                <div class="main-image bg-img-cover d-lg-none" style="<?php echo 'background-image: url(' . $view->get_main_image_url() . ')'; ?>"></div>
                                <?php endif; ?>
                            </div>
                            <?php endif; ?>
                        </div>
                    </div>
                    <div class="col-xl-12 col-xxl-3">
                        <div class="empty-content"></div>
                    </div>
                </div>
            </div>
        </div>
        
    <?php echo $view->getContainerTagClose(); ?>

</<?php echo $view->getViewTag(); ?>>
