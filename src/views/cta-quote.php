<<?php echo $view->getViewTag(); ?> class="<?php echo $view->_getHtmlClass(); ?>">

    <?php echo $view->getAdminEditButton(); ?>
    
    <?php echo $view->getContainerTagOpen(); ?>
    
        <div class="cta-content">
            
            <?php if($view->has_title()): ?>
            <h2 class="title"><?php echo nl2br($view->get_title()); ?></h2>
            <?php endif; ?>

            <?php if($view->has_description()): ?>
            <div class="description">
                <div class="quote"></div>
                <?php echo wpautop($view->get_description()); ?>
            </div>
            <?php endif; ?>

            <?php if($view->has_more_url() && $view->has_more_label()): ?>
            <div class="more-info">
                <a class="btn-t btn-large <?php echo $view->isBgColorDark() ? 'btn-soft-grey' : 'btn-dark-blue'; ?>" href="<?php echo $view->get_more_url(); ?>"><?php echo $view->get_more_label(); ?></a>
            </div>
            <?php endif; ?>
            
        </div>
        
    <?php echo $view->getContainerTagClose(); ?>
        
</<?php echo $view->getViewTag(); ?>>
