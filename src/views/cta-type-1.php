<<?php echo $view->getViewTag(); ?> class="<?php echo $view->_getHtmlClass(); ?>">

    <?php echo $view->getAdminEditButton(); ?>
    
    <?php echo $view->getContainerTagOpen(); ?>
    
        <div class="cta-content">
            
            <?php
            if($view->has_title()):
                wpseed_print_view('title', [
                    'pre_title' => $view->get_pre_title(),
                    'title' => $view->get_title(),
                    'alignment' => 'center',
                    // 'variant' => 'section-title',
                    'h_type' => 'h2'
                ]);
            endif;
            ?>

            <?php if($view->has_description()): ?>
            <div class="description"><?php echo wpautop($view->get_description()); ?></div>
            <?php endif; ?>

            <?php 
            $link_url = $view->has_more_url() ? $view->get_more_url() : ($view->has_more_page() ? get_permalink($view->get_more_page()) : '');
            if($link_url): 
                ?>
            <div class="more-info">
                <a class="btn-t btn-large <?php echo $view->isBgColorDark() ? 'btn-soft-grey' : 'btn-dark-blue'; ?>" href="<?php echo $link_url; ?>"><?php echo $view->get_more_label(); ?></a>
            </div>
            <?php endif; ?>
            
        </div>
        
    <?php echo $view->getContainerTagClose(); ?>
        
</<?php echo $view->getViewTag(); ?>>
