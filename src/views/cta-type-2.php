<<?php echo $view->getViewTag(); ?> class="<?php echo $view->_getHtmlClass(); ?>">

    <?php echo $view->getAdminEditButton(); ?>
    
    <div class="view-inner bg-img-cover" style="<?php echo $view->has_banner_image() ? 'background-image: url(' . $view->get_banner_image() . ')' : ''; ?>">
        
        <div class="cta-content">
            
            <div class="row">
                <div class="col-sm-8">
                    
                    <?php if($view->has_before_title()): ?>
                    <p class="before-title"><?php echo $view->get_before_title(); ?></p>
                    <?php endif; ?>
                    <?php if($view->has_title()): ?>
                    <h2 class="title"><?php echo nl2br($view->get_title()); ?></h2>
                    <?php endif; ?>
                    
                    <?php if($view->has_description()): ?>
                    <div class="description"><?php echo $view->get_description(); ?></div>
                    <?php endif; ?>
                </div>
                <div class="col-sm-4">
                    <?php 
                    $link_url = $view->has_more_url() ? $view->get_more_url() : ($view->has_more_page() ? get_permalink($view->get_more_page()) : '');
                    if($link_url): ?>
                    <div class="more-info">
                        <a class="btn-t btn-dark-blue" href="<?php echo $link_url; ?>"><?php echo $view->get_more_label(); ?></a>
                    </div>
                    <?php endif; ?>
                </div>
            </div>
            
        </div>
    </div>
    
</<?php echo $view->getViewTag(); ?>>
