<<?php echo $view->getViewTag(); ?> class="<?php echo $view->_getHtmlClass(); ?>">

    <?php echo $view->getAdminEditButton(); ?>
    
    <?php echo $view->getContainerTagOpen(); ?>

    <div class="row">
        <div class="col-lg-8">
            <div class="content-main">
                <?php if($view->has_title_main()): ?>
                <h3 class="title-main"><?php echo nl2br($view->get_title_main()); ?></h3>
                <?php endif; ?>

                <?php if($view->has_description_main()): ?>
                <div class="description-main"><?php echo wpautop($view->get_description_main()); ?></div>
                <?php endif; ?>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="content-second">
                <?php if($view->has_title_second()): ?>
                <h3 class="title-second"><?php echo nl2br($view->get_title_second()); ?></h3>
                <?php endif; ?>

                <?php if($view->has_description_second()): ?>
                <div class="description-second"><?php echo wpautop($view->get_description_second()); ?></div>
                <?php endif; ?>

                <?php if($view->has_more_url() && $view->has_more_label()): ?>
                <div class="more-info">
                    <a class="btn-t btn-large btn-dark-blue" href="<?php echo $view->get_more_url(); ?>" target="<?php echo $view->get_more_url_target(); ?>"><?php echo $view->get_more_label(); ?></a>
                </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
        
    <?php echo $view->getContainerTagClose(); ?>
        
</<?php echo $view->getViewTag(); ?>>
