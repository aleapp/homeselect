<footer class="<?php echo $view->getHtmlClass(); ?>">
    <div class="footer-area-1 bg-6">
        <?php echo $view->getContainerTagOpen(); ?>
            <div class="row">
                <div class="col-lg-2">
                    <div class="widgets-area footer-widgets-1">
                        <?php dynamic_sidebar('footer-widgets-1'); ?>
                    </div>
                </div>
                <div class="col-lg-2">
                    <div class="widgets-area footer-widgets-2">
                        <?php dynamic_sidebar('footer-widgets-2'); ?>
                    </div>
                </div>
                <div class="col-lg-2">
                    <div class="widgets-area footer-widgets-3">
                        <?php dynamic_sidebar('footer-widgets-3'); ?>
                    </div>
                </div>
                <div class="col-lg-2">
                    <div class="widgets-area footer-widgets-4">
                        <?php dynamic_sidebar('footer-widgets-4'); ?>
                    </div>
                </div>
                <div class="col-lg-2">
                    <div class="widgets-area footer-widgets-5">
                        <?php dynamic_sidebar('footer-widgets-5'); ?>
                    </div>
                </div>
                <div class="col-lg-2">
                    <div class="widgets-area footer-widgets-6">
                        <?php dynamic_sidebar('footer-widgets-6'); ?>
                    </div>
                </div>
            </div>
        <?php echo $view->getContainerTagClose(); ?>
    </div>
    <div class="footer-area-2 bg-8">
        <?php echo $view->getContainerTagOpen(); ?>
            <div class="row">
                <div class="col-lg-6">
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="widgets-area footer-bottom-1-1">
                                <?php dynamic_sidebar('footer-widgets-bottom-1-1'); ?>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="widgets-area footer-bottom-1-2">
                                <?php dynamic_sidebar('footer-widgets-bottom-1-2'); ?>
                            </div>
                        </div>
                        <div class="col-lg-4">
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="widgets-area footer-bottom-1-3">
                        <?php dynamic_sidebar('footer-widgets-bottom-1-3'); ?>
                    </div>
                </div>
            </div>
        <?php echo $view->getContainerTagClose(); ?>
    </div>
    <div class="footer-area-3 bg-7">
        <div class="widgets-area footer-bottom-2-1">
            <?php dynamic_sidebar('footer-widgets-bottom-2-1'); ?>
        </div>
        <div class="widgets-area footer-bottom-2-2">
            <?php dynamic_sidebar('footer-widgets-bottom-2-2'); ?>
        </div>
    </div>
</footer>
