<<?php echo $view->getViewTag(); ?> class="<?php echo $view->_getHtmlClass(); ?>" data-view="<?php echo $view->getName(); ?>">

    <?php if($view->has_images()): ?>

    <div class="modal">
        <div class="modal-dialog modal-fullscreen">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div class="gallery-slider-cont h-full">
                        <div class="swiper gallery-slider h-full">
                            <div class="swiper-wrapper h-full">
                                <?php 
                                $images_total = $view->get_images_num();
                                foreach($view->get_images() as $i => $image): 
                                    $_i = $i+1;
                                    $image_src = is_int($image) ? $view->getAttachmentImageSrc($image, 'full') : $image;
                                    ?>
                                <div class="swiper-slide h-full">
                                    <div class="gallery-image h-full" data-src="<?php echo $image_src; ?>">
                                        <img src="" alt="<?php printf(__('Gallery image %d', 'hs'), $_i); ?>" />
                                        <div class="counter"><?php echo $_i . '/' . $images_total; ?></div>
                                    </div>
                                </div>
                                <?php endforeach; ?>
                            </div>
                            <div class="swiper-nav swiper-button-prev"></div>
                            <div class="swiper-nav swiper-button-next"></div>
                        </div>

                        <?php if($view->has_gallery_title()): ?>
                        <div class="gallery-title">
                            <?php echo $view->get_gallery_title(); ?>
                        </div>
                        <?php endif; ?>

                        <div class="loader">
                            <div class="spinner-border" role="status">
                                <span class="sr-only">Loading...</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php endif; ?>

</<?php echo $view->getViewTag(); ?>>
