<<?php echo $view->getViewTag(); ?> class="<?php echo $view->_getHtmlClass(); ?>" data-view="<?php echo $view->getName(); ?>">

    <?php echo $view->getAdminEditButton(); ?>

    <?php if($view->has_image_url()): ?>
    <div class="abs-full bg-img-cover bg-image" style="<?php echo 'background-image: url(' . $view->get_image_url() . ')'; ?>"></div>
    <?php endif; ?>
    
    <?php if($view->has_shadow()): ?>
    <div class="abs-full bg-overlay"></div>
    <?php endif; ?>
    
    <div class="banner-content">
        
        <div class="<?php echo $view->get_container_class(); ?>">
            <?php if($view->has_title()): ?>
            <h1 class="banner-title">
                <?php echo $view->get_title(); ?>
            </h1>
            <?php endif; ?>
            
            <?php 
            wpseed_print_view('property-filters-simple', [
                'external_links' => $view->get_filter_external_links()
            ]); 
            ?>
        </div>
        
    </div>
    
</<?php echo $view->getViewTag(); ?>>
