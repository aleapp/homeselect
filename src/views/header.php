<header id="header" class="<?php echo $view->_getHtmlClass(); ?>" data-view="<?php echo $view->getName(); ?>">

    <button type="button" class="nav-toggle-btn d-lg-none"></button>

    <a href="<?php echo $view->get_home_url(); ?>" class="site-logo bg-img-contain"></a>

    <nav class="navbar-city-opts d-none d-lg-block" role="navigation">

        <?php if($view->has_city_options()): ?>
        <ul class="menu">
            <li class="menu-item menu-item-has-children">
                <a href="#"><span class="item-name"><?php echo $view->getDefaultCityName(); ?></span></a>
                <ul class="sub-menu">
                    <?php foreach($view->get_city_options() as $city_option): ?>
                    <li class="menu-item">
                        <a href="<?php echo add_query_arg(\HS\Localization::SLUG, $city_option['slug']); ?>">
                            <span class="item-name"><?php echo $city_option['name']; ?></span>
                        </a>
                    </li>
                    <?php endforeach; ?>
                </ul>
            </li>
        </ul>
        <?php endif; ?>

    </nav>
    
    <nav class="navbar-primary d-none d-lg-block" role="navigation">
        <?php
        wp_nav_menu(
            array(
                'theme_location'  => 'primary',
                'link_before' => '<span class="item-name">',
                'link_after' => '<span class="item-border"></span></span>',
                // 'after' => '<span class="item-bg"></span>',
                'items_wrap'      => '<ul class="%2$s">%3$s</ul>',
                'fallback_cb'     => false,
                'depth' => 2
            )
        );
        ?>
    </nav>

    <nav class="navbar-mobile d-lg-none" role="navigation">
        <?php
        wp_nav_menu(
            array(
                'theme_location'  => 'mobile',
                'container_class' => 'menu-cont',
                'link_before' => '<span class="item-name">',
                'link_after' => '<span class="item-border"></span></span>',
                // 'after' => '<span class="item-bg"></span>',
                'items_wrap'      => '<ul class="%2$s">%3$s</ul>',
                'fallback_cb'     => false,
                'depth' => 2
            )
        );
        ?>

        <div class="nav-close-area"></div>
    </nav>

    <a href="#" class="login-btn"><?php _e('Sign In', 'hs'); ?></a>

</header>
