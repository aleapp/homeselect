<<?php echo $view->getViewTag(); ?> id="<?php echo $view->getId(); ?>" class="<?php echo $view->_getHtmlClass(); ?>" data-view="<?php echo $view->getName(); ?>">

    <?php echo $view->getAdminEditButton(); ?>

    <?php echo $view->get_image_html(); ?>
    
    <?php if($view->has_shadow()): ?>
    <div class="abs-full bg-overlay"></div>
    <?php endif; ?>

</<?php echo $view->getViewTag(); ?>>
