<<?php echo $view->getViewTag(); ?> class="<?php echo $view->_getHtmlClass(); ?>">

<?php echo $view->getAdminEditButton(); ?>

<?php if($view->has_lang_options()): ?>

    <?php 
    wpseed_print_view('nice-dropdown', [

        // 'empty_name' => __('Select language', 'hs'),
        'selected' => $view->get_current_lang_url(),
        'options' => $view->get_lang_options()
    ]); 
    ?>

<?php endif; ?>
        
</<?php echo $view->getViewTag(); ?>>
