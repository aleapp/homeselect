<div class="<?php echo $view->_getHtmlClass(); ?>">
    <div class="loader-spinner">
        <div class="spinner-border" role="status">
            <span class="sr-only">Loading...</span>
        </div>
    </div>
</div>
