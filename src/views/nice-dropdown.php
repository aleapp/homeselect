<<?php echo $view->getViewTag(); ?> class="<?php echo $view->_getHtmlClass(); ?>" data-view="<?php echo $view->getName(); ?>">

<div class="selected-label<?php if($view->has_label()) echo ' has-label'; ?>">
    <span class="label-text" data-orig="<?php echo $view->get_label(); ?>">
        <?php echo $view->get_label(); ?>
    </span>
</div>

<ul>
    <?php 
    $input_type = $view->get_input_type();
    $input_name = ($view->has_input_name() && $input_type === 'checkbox') ? $view->get_input_name() . '[]' : $view->get_input_name();
    $input_class = $view->args['input_class'] ? (is_array($view->args['input_class']) ? implode(' ', $view->args['input_class']) : $view->args['input_class']) : '';
    
    if($view->has_empty_name()): ?>
        <li class="no-option">
            <?php if($input_name): ?>
            <input type="<?php echo $input_type; ?>" id="<?php echo $input_name . '_all'; ?>" class="<?php echo $input_class; ?>" name="<?php echo $input_name; ?>" value="" />
            <label for="<?php echo $input_name . '_all'; ?>" data-text="<?php echo $view->get_empty_name(); ?>">
                <span class="label-text"><?php echo $view->get_empty_name(); ?></span>
            </label>
            <?php else: ?>
            <label data-text="<?php echo $view->get_empty_name(); ?>">
                <span class="label-text"><?php echo $view->get_empty_name(); ?></span>
            </label>
            <?php endif; ?>
        </li>
    <?php endif; ?>

    <?php
    foreach($view->get_options() as $i => $option): 
        $li_class = [];
        if($option['url'])
        {
            $li_class[] = 'is-link';
            if($option['url'] == $view->get_selected())
            {
                $li_class[] = 'active';
            }
        }
        elseif($option['value']){
            $li_class[] = 'is-input';
        }
        ?>
    <li class="<?php echo implode(' ', $li_class); ?>">

        <?php 
        if($option['url']): 
            // $active_class = ($option['url'] == $view->get_selected()) ? ' selected' : '';
            ?>
        <label data-text="<?php echo $option['name']; ?>">
            <a href="<?php echo $option['url']; ?>" class="label-text" target="<?php echo $option['target']; ?>"><?php echo $option['name']; ?></a>
        </label>
        <?php 
        // elseif($option['value']): 
        else: 
            $option_id = $input_name . '_' . $option['value'];
            // $checked = (($option['value'] == $view->get_selected()) || (!$view->has_empty_name() && $i === 0)) ? ' checked' : '';
            $checked = ($option['value'] && $option['value'] == $view->get_selected()) ? ' checked' : '';
            ?>

        <input type="<?php echo $input_type; ?>" id="<?php echo $option_id; ?>" class="<?php echo $input_class; ?>" name="<?php echo $input_name; ?>" value="<?php echo $option['value']; ?>"<?php echo $checked; ?> />
        <label for="<?php echo $option_id; ?>" data-text="<?php echo $option['name']; ?>">
            <span class="label-text"><?php echo $option['name']; ?></span>
        </label>
        <?php endif; ?>
    </li>
    <?php endforeach; ?>
</ul>
        
</<?php echo $view->getViewTag(); ?>>
