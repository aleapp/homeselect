<<?php echo $view->getViewTag(); ?> class="<?php echo $view->_getHtmlClass(); ?>">

    <?php echo $view->getAdminEditButton(); ?>
    
    <?php echo $view->getContainerTagOpen(); ?>
        
        <?php 
        if($view->has_title()):
            wpseed_print_view('title', [
                'title' => $view->get_title(),
                'alignment' => 'center',
                'variant' => 'section-title',
                'h_type' => 'h4'
            ]);
        endif;
        ?>
    
        <?php if($view->has_list_items()): ?>
        <div class="services-list">
        <?php 
        $cols_class = 'col-lg-' . 12/$view->get_cols_num();
        foreach(array_chunk($view->get_list_items(), $view->get_cols_num()) as $items_row): ?>
            <div class="row g-0">
                <?php foreach($items_row as $item): ?>
                <div class="<?php echo $cols_class; ?>">
                    <a href="<?php echo $item['link']; ?>" class="service bg-img-cover" style="<?php if($item['image_src']) echo 'background-image: url(' . $item['image_src'] . ')'; ?>">
                        <h4 class="title"><?php echo $item['title']; ?></h4>
                    </a>
                </div>
                <?php endforeach; ?>
            </div>
        <?php endforeach; ?>
        </div>
        <?php endif; ?>
        
    <?php echo $view->getContainerTagClose(); ?>

</<?php echo $view->getViewTag(); ?>>
