<<?php echo $view->getViewTag(); ?> class="<?php echo $view->_getHtmlClass(); ?>">

    <?php echo $view->getContainerTagOpen(); ?>

        <?php if($view->has_image_html()): ?>
        <div class="person-image">
            <?php echo $view->get_image_html(); ?>
        </div>
        <?php endif; ?>

        <?php if($view->has_name()): ?>
        <h3 class="person-name"><?php echo $view->get_name(); ?></h3>
        <?php endif; ?>

        <?php if($view->has_position()): ?>
        <h4 class="person-position"><?php echo $view->get_position(); ?></h4>
        <?php endif; ?>

    <?php echo $view->getContainerTagClose(); ?>

</<?php echo $view->getViewTag(); ?>>
