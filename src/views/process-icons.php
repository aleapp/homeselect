<<?php echo $view->getViewTag(); ?> class="<?php echo $view->_getHtmlClass(); ?>">

    <?php echo $view->getAdminEditButton(); ?>

    <?php echo $view->getContainerTagOpen(); ?>

        <?php 
        if($view->has_title()):
            wpseed_print_view('title', [
                'title' => $view->get_title(),
                'alignment' => 'center',
                'variant' => 'section-title',
                'h_type' => 'h2'
            ]);
        endif;
        ?>

        <?php if($view->has_items()): ?>
        <ul class="icons-list ns">
            <?php foreach($view->get_items() as $item): ?>
            <li class="icon-item">
                <?php if(!empty($item['icon_type'])): ?>
                <span class="icon-type <?php echo $item['icon_type']; ?>"></span>
                <?php endif; ?>
                <?php if(!empty($item['icon_title'])): ?>
                <span class="icon-name"><?php echo nl2br($item['icon_title']); ?></span>
                <?php endif; ?>
            </li>
            <?php endforeach; ?>
        </ul>
        <?php endif; ?>

    <?php echo $view->getContainerTagClose(); ?>

</<?php echo $view->getViewTag(); ?>>
