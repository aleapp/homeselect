<<?php echo $view->getViewTag(); ?> class="<?php echo $view->_getHtmlClass(); ?>" data-view="<?php echo $view->getName(); ?>">

    <div class="view-inner">

        <?php if($view->has_featured_image()): ?>
        <div class="project-image">
            <?php echo $view->getAttachmentImageHtml($view->get_featured_image(), 'full', 'rect-150-100'); ?>
            <?php if($view->has_gallery_images_num()): ?>
            <span class="gallery-btn"><?php echo $view->get_gallery_images_num(); ?></span>
            <?php endif; ?>
        </div>
        <?php endif; ?>

        <div class="project-info">
            <div class="info-inner">
                <h3 class="title"><?php echo $view->get_title(); ?></h3>
                <div class="description">
                    <?php echo $view->get_description(); ?>
                </div>
                <div class="details">
                    <?php 
                    $details = $view->get_details();
                    $rows_num = count($details);
                    $details_num = $view->get_details_num();
                    $rows_count = 0;
                    $details_count = 0;
                    foreach($details as $ri => $details_row): 
                        $rows_count++;
                        $row_last = ($rows_count === $rows_num) ? ' row-last' : '';
                        ?>
                        <div class="details-row<?php echo $row_last; ?>">
                            <?php foreach($details_row as $key => $detail): 
                                $details_count++;
                                $detail_last = ($details_count === $details_num) ? ' detail-last' : '';
                                ?>
                            <div class="details-col<?php echo $detail_last; ?>">
                                <div class="detail <?php echo $detail['key']; ?>">
                                    <span class="detail-value"><?php echo $detail['value'] ? $detail['value_formatted'] : '-'; ?></span>
                                    <span class="detail-label"><?php echo $detail['label']; ?></span>
                                </div>
                            </div>
                            <?php endforeach; ?>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </div>

    <?php 
    if($view->has_gallery_images()): 
        wpseed_print_view('gallery-modal', [
            'images' => $view->get_gallery_images()
        ]);
    endif;
    ?>

</<?php echo $view->getViewTag(); ?>>
