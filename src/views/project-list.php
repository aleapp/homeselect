<<?php echo $view->getViewTag(); ?> class="<?php echo $view->_getHtmlClass(); ?>">

    <?php echo $view->getAdminEditButton(); ?>

    <?php if($view->has_items()): ?>
    <div class="list-items">
        <?php 
        $col_class = 'col-lg-' . (12/$view->get_cols_num());
        foreach($view->get_items() as $items_row): ?>
        <div class="row">
            <?php foreach($items_row as $item): ?>
            <div class="<?php echo $col_class; ?>">
                <?php 
                wpseed_print_view('project-item', [
                    'project' => $item
                ]);
                ?>
            </div>
            <?php endforeach; ?>
        </div>
        <?php endforeach; ?>
    </div>
    <?php endif; ?>

</<?php echo $view->getViewTag(); ?>>