<?php if(!$view->has_contact_form_id()) return; ?>

<div class="<?php echo $view->_getHtmlClass(); ?>">
    <div id="<?php echo $view->getId(); ?>-contact-modal" class="modal" tabindex="-1">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div class="contact-form white-bg-form">

                        <?php if($view->has_title()): ?>
                        <h4 class="form-title"><?php echo $view->get_title(); ?></h4>
                        <?php endif; ?>

                        <?php if($view->has_description()): ?>
                        <div class="form-description">
                            <?php echo wpautop($view->get_description()); ?>
                        </div>
                        <?php endif; ?>

                        <?php echo do_shortcode('[contact-form-7 id="'. $view->get_contact_form_id() .'" agent-id="' . $view->get_agent_id() . '" property-id="' . $view->get_property_id() . '"]'); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
