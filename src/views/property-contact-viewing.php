<<?php echo $view->getViewTag(); ?> class="<?php echo $view->_getHtmlClass(); ?>" data-view="<?php echo $view->getName(); ?>">

    <?php echo $view->getAdminEditButton(); ?>

    <?php if($view->has_title()): ?>
    <h4 class="form-title"><?php echo $view->get_title(); ?></h4>
    <?php endif; ?>

    <?php if($view->has_description()): ?>
    <div class="form-description">
        <?php echo wpautop($view->get_description()); ?>
    </div>
    <?php endif; ?>

    <?php if($view->has_contact_form_id()): ?>
        <?php echo do_shortcode('[contact-form-7 id="'. $view->get_contact_form_id() .'" agent-id="' . $view->get_agent_id() . '" property-id="' . $view->get_property_id() . '"]'); ?>
    <?php endif; ?>

</<?php echo $view->getViewTag(); ?>>
