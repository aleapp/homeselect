<<?php echo $view->getViewTag(); ?> class="<?php echo $view->_getHtmlClass(); ?>" data-view="<?php echo $view->getName(); ?>">

    <?php //echo $view->getAdminEditButton(); ?>

    <div class="content-slides">
        <div class="swiper">
            <div class="swiper-wrapper">
                <div class="swiper-slide slide-item slide-info">
                    <div class="slide-inner">
                        <?php if($args['property']->getPrice()): ?>
                        <div class="property-price">
                            <?php echo $args['property']->getPrice(true); ?>
                        </div>
                        <?php endif; ?>
                        <?php if($view->has_show_agent_meet_btn()): ?>
                        <div class="property-arrange d-lg-none">
                            <button class="btn-t btn-black btn-full-w goto-slide" data-slide="slide-form"><?php _e('Arrange a viewing', 'hs'); ?></button>
                        </div>
                        <div class="property-arrange d-none d-lg-block">
                            <button class="btn-t btn-light-blue btn-full-w goto-slide" data-slide="slide-form"><?php _e('Arrange a viewing', 'hs'); ?></button>
                        </div>
                        <?php endif; ?>
                        <?php if($args['agent']->get_id()): ?>
                        <div class="property-agent">
                            <?php
                            wpseed_print_view('agent-card', [
                                'agent' => $args['agent'],
                                'property' => $args['property'],
                                'contact_form_id' => $view->get_contact_form_id_agent()
                            ]);
                            ?>
                        </div>
                        <?php endif; ?>
                    </div>
                </div>

                <?php if($view->has_contact_form_id_viewing()): ?>
                <div class="swiper-slide slide-item slide-form">
                    <div class="slide-inner">
                        <button class="goto-slide btn-back" data-slide="slide-info"></button>

                        <?php 
                        wpseed_print_view('property-contact-viewing', [
                            // 'property' => $args['property'],
                            'agent_id' => $args['agent']->get_id(),
                            'property_id' => $args['property']->get_id(),
                            'contact_form_id' => $view->get_contact_form_id_viewing(),
                        ]);
                        ?>
                    </div>
                </div>
                <?php endif; ?>
            </div>
        </div>
    </div>

</<?php echo $view->getViewTag(); ?>>
