<<?php echo $view->getViewTag(); ?> id="<?php echo $view->getId(); ?>" class="<?php echo $view->_getHtmlClass('bg-grey-5'); ?>" data-view="<?php echo $view->getName(); ?>">

    <?php //echo $view->getAdminEditButton(); ?>

    <div class="info-container">

        <?php if($info = $args['property']->getDescription()): ?>
        <div class="detail-info description">
            <h4 class="detail-title"><?php _e('About the property', 'hs'); ?></h4>
            <?php echo wpautop($info); ?>
        </div>
        <?php endif; ?>

        <div id="<?php echo $view->get_cont_id_specs(); ?>" class="detail-info specs">
            <h4 class="detail-title"><?php _e('Specifications', 'hs'); ?></h4>

            <div class="row">
                <div class="col-sm-6">
                    <div class="info-item bedrooms">
                        <span class="info-label"><?php _e('Bedrooms', 'hs'); ?></span>
                        <span class="info-value"><?php echo $args['property']->getBedrooms(); ?></span>
                    </div>
                    <div class="info-item bathrooms">
                        <span class="info-label"><?php _e('Bathrooms', 'hs'); ?></span>
                        <span class="info-value"><?php echo $args['property']->getBathrooms(); ?></span>
                    </div>
                    <div class="info-item floor">
                        <span class="info-label"><?php _e('Floor', 'hs'); ?></span>
                        <span class="info-value"><?php echo $args['property']->getFloor(); ?></span>
                    </div>
                    <div class="info-item net-area">
                        <span class="info-label"><?php _e('Area', 'hs'); ?></span>
                        <span class="info-value"><?php echo $args['property']->getArea(true); ?></span>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="info-item type">
                        <span class="info-label"><?php _e('Type', 'hs'); ?></span>
                        <span class="info-value"><?php echo $args['property']->getType(true); ?></span>
                    </div>
                    <div class="info-item condition">
                        <span class="info-label"><?php _e('Condition', 'hs'); ?></span>
                        <span class="info-value"><?php echo $args['property']->getCondition(true); ?></span>
                    </div>
                </div>
            </div>
        </div>
        <div id="<?php echo $view->get_cont_id_amenities(); ?>" class="detail-info amenities">
            <h4 class="detail-title"><?php _e('Amenities', 'hs'); ?></h4>

        </div>
        <div id="<?php echo $view->get_cont_id_gallery(); ?>" class="detail-info gallery">
            <h4 class="detail-title"><?php _e('Photos', 'hs'); ?></h4>

            <?php 
            wpseed_print_view('property-gallery', [
                'id' => 'property-gallery',
                'property' => $args['property']
            ]);
            ?>
            
        </div>
    </div>

</<?php echo $view->getViewTag(); ?>>
