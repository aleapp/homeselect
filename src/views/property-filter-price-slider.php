<div class="<?php echo $view->_getHtmlClass(); ?>" data-view="<?php echo $view->getName(); ?>">
    <input type="text" class="range-slider" 
        data-type="double" 
        data-grid="false" 
        data-min="<?php echo $view->getPriceRangeMin(); ?>" 
        data-max="<?php echo $view->getPriceRangeMax(); ?>" 
        data-from="<?php echo $view->getDefaultValueRange($view->get_input_name(), 0); ?>" 
        data-to="<?php echo $view->getDefaultValueRange($view->get_input_name(), 1); ?>" 
        data-step="100" 
        data-skin="round" 
        data-hide_min_max="true" 
        data-prefix="<?php echo $view->getCurrencySymbol(); ?>" 
        name="<?php echo $view->get_input_name(); ?>" 
        value="<?php echo $view->getDefaultValue($view->get_input_name()); ?>" 
        />
</div>
