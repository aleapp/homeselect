<<?php echo $view->getViewTag(); ?> class="<?php echo $view->_getHtmlClass(); ?>" 
    data-view="<?php echo $view->getName(); ?>"
    data-list_cont="<?php echo $view->get_list_cont(); ?>" 
    >

    <?php echo $view->getContainerTagOpen(); ?>
    
    <form action="<?php echo admin_url('admin-ajax.php'); ?>" class="filters-form" method="POST">

        <div class="filters-block main">
            <div class="row">
                <div class="col-lg-3">
                    <div class="filter property_offer type-dropdown">
                        <?php 
                        wpseed_print_view('nice-dropdown', [
                            'label' => \HSP\Type\Property::getPropConfigData('property_offer', 'filter_label', __('Select', 'hs')),
                            // 'empty_name' => \HSP\Type\Property::getPropConfigData('property_offer', 'filter_empty_opt', __('All', 'hs')),
                            'selected' => $view->getDefaultValue('property_offer'),
                            'input_name' => 'property_offer',
                            'options' => $view->get_property_offer_options(),
                            'input_class' => 'change-submit'
                        ]);
                        ?>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="filter city type-dropdown">
                        <?php 
                        wpseed_print_view('nice-dropdown', [
                            'label' => \HSP\Type\Property::getPropConfigData('city', 'filter_label', __('Select', 'hs')),
                            // 'empty_name' => \HSP\Type\Property::getPropConfigData('city', 'filter_empty_opt', __('All', 'hs')),
                            'selected' => $view->getDefaultValue('city') ? $view->getDefaultValue('city') : $view->getDefaultCity(),
                            'input_name' => 'city',
                            'options' => $view->get_city_options(),
                            'input_class' => 'change-submit'
                        ]);
                        ?>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="filter parish type-dropdown">
                        <?php 
                        wpseed_print_view('nice-dropdown', [
                            'label' => \HSP\Type\Property::getPropConfigData('parish', 'filter_label_alt', __('Select', 'hs')),
                            'empty_name' => \HSP\Type\Property::getPropConfigData('parish', 'filter_empty_opt', __('All', 'hs')),
                            'selected' => $view->getDefaultValue('parish'),
                            'input_name' => 'parish',
                            'options' => $view->get_parish_options(),
                            'input_class' => 'change-submit'
                        ]);
                        ?>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="filter show-advanced mob d-lg-none">
                                <button type="button" class="btn-t btn-soft-grey btn-full-w btn-show-advanced"><?php _e('Filters', 'hs'); ?></button>
                            </div>
                            <div class="filter show-advanced d-none d-lg-block">
                                <button type="button" class="btn-t btn-soft-grey btn-full-w btn-show-advanced"><?php _e('Show filters', 'hs'); ?></button>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="filter d-none d-lg-block">
                                <button type="submit" class="btn-t btn-dark-blue btn-full-w"><?php _e('Search', 'hs'); ?></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="filters-block advanced">
            <div class="row">
                <div class="col-lg-4">
                    <label class="block-label"><?php _e('Price range', 'hs'); ?></label>
                    <div class="filter price_range type-range-slider">
                        <?php wpseed_print_view('property-filter-price-slider'); ?>
                    </div>
                </div>
                <div class="col-lg-8">
                    <label class="block-label"><?php _e('Dimensions', 'hs'); ?></label>
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="filter bedrooms type-dropdown">
                                <?php 
                                wpseed_print_view('nice-dropdown', [
                                    'empty_name' => __('Bedrooms', 'hs'),
                                    'selected' => $view->getDefaultValue('bedrooms'),
                                    'input_name' => 'bedrooms',
                                    'options' => $view->get_bedrooms_options(),
                                    'input_class' => 'change-submit'
                                ]);
                                ?>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="filter bathrooms type-dropdown">
                                <?php 
                                wpseed_print_view('nice-dropdown', [
                                    'empty_name' => __('Bathrooms', 'hs'),
                                    'selected' => $view->getDefaultValue('bathrooms'),
                                    'input_name' => 'bathrooms',
                                    'options' => $view->get_bathrooms_options(),
                                    'input_class' => 'change-submit'
                                ]);
                                ?>
                            </div>
                            <div class="filter text-end d-none d-lg-block">
                                <button type="button" class="btn-t btn-dark-blue btn-hide-advanced"><?php _e('Close filters', 'hs'); ?></button>
                            </div>
                            <div class="filter d-lg-none">
                                <button type="button" class="btn-t btn-soft-grey btn-hide-advanced btn-full-w"><?php _e('Close filters', 'hs'); ?></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="filters-block d-lg-none">
            <div class="filter">
                <button type="submit" class="btn-t btn-dark-blue btn-full-w"><?php _e('Search', 'hs'); ?></button>
            </div>
        </div>
        <div class="filters-block ordering">
            <div class="row">
                <div class="col-6 col-lg-8">
                    <div class="filter">
                        <div class="found-summary">
                            <?php echo $view->getFoundSummaryText($view->get_items_total()); ?>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-lg-4">
                    <div class="filter orderby">
                        <?php 
                        wpseed_print_view('nice-dropdown', [
                            'empty_name' => __('Order by', 'hs'),
                            'selected' => $view->getDefaultValue('orderby', 'price_high_low'),
                            'input_name' => 'orderby',
                            'options' => $view->getOrderingOptions(),
                            'input_class' => 'change-submit'
                        ]);
                        ?>
                    </div>
                </div>
            </div>
        </div>

        <input type="hidden" name="action" value="filter_properties" />
        <input type="hidden" class="change-submit" name="paged" value="<?php echo $view->get_paged(); ?>" />

    </form>

    <?php echo $view->getContainerTagClose(); ?>
    
</<?php echo $view->getViewTag(); ?>>
