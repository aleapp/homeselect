<<?php echo $view->getViewTag(); ?> class="<?php echo $view->_getHtmlClass('property-filters'); ?>" data-view="<?php echo $view->getName(); ?>">
    
    <form action="<?php echo $view->getListPageUrl(); ?>" class="filters-form" method="GET">

        <div class="filters-block-top">
            <div class="filter property_offer type-inline">
                <ul class="options ns">
                    <?php foreach($view->get_property_offer_options() as $i => $option): ?>
                    <li>
                        <?php if(!empty($option['url'])): ?>
                        <label><a href="<?php echo $option['url']; ?>" target="<?php echo $option['target']; ?>"><?php echo $option['name']; ?></a></label>
                        <?php 
                        else:
                            $input_name = 'property_offer';
                            $input_id = $input_name . '_' . $option['value'];
                            $checked = ($i === 0) ? ' checked' : '';
                            ?>
                        <input type="radio" id="<?php echo $input_id; ?>" name="<?php echo $input_name; ?>" value="<?php echo $option['value']; ?>"<?php echo $checked; ?> />
                        <label for="<?php echo $input_id; ?>"><?php echo $option['name']; ?></label>
                        <?php endif; ?>
                    </li>
                    <?php endforeach; ?>
                </ul>
            </div>
        </div>

        <div class="filters-block-main">
            <div class="row">
                <div class="col-lg-6">
                    <div class="filter city type-dropdown">
                        <?php 
                        wpseed_print_view('nice-dropdown', [
                            'label' => \HSP\Type\Property::getPropConfigData('city', 'filter_label', __('Select', 'hs')),
                            // 'empty_name' => \HSP\Type\Property::getPropConfigData('city', 'filter_empty_opt', __('All', 'hs')),
                            'selected' => $view->getDefaultCity(),
                            'input_name' => 'city',
                            'options' => $view->get_city_options(),
                            'input_class' => 'change-submit'
                        ]);
                        ?>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="filter parish type-dropdown has-submit">
                        <?php 
                        wpseed_print_view('nice-dropdown', [
                            'label' => \HSP\Type\Property::getPropConfigData('parish', 'filter_label', __('Select', 'hs')),
                            'empty_name' => \HSP\Type\Property::getPropConfigData('parish', 'filter_empty_opt', __('All', 'hs')),
                            'selected' => $view->getDefaultValue('parish'),
                            'input_name' => 'parish',
                            'options' => $view->get_parish_options(),
                            'input_class' => 'change-submit'
                        ]);
                        ?>
                        <a href="#" class="btn-submit ns"></a>
                    </div>
                </div>
            </div>
        </div>

    </form>
    
</<?php echo $view->getViewTag(); ?>>
