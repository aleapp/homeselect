<<?php echo $view->getViewTag(); ?> id="<?php echo $view->getId(); ?>" class="<?php echo $view->_getHtmlClass(); ?>" data-view="<?php echo $view->getName(); ?>">

    <?php //echo $view->getAdminEditButton(); ?>

    <?php 
    $images_rows = array_chunk($args['property']->getGalleryImagesSrc('Thumbnail'), 2);
    if($images_rows): ?>
    
    <div class="gallery-thumbnails">
        <?php foreach($images_rows as $i => $images): ?>
        <div class="row">
            <?php foreach($images as $ii => $image_src): 
                $_ii = ($i * ($ii+1))+1;
                ?>
            <div class="col-6">
                <div class="gallery-thumbnail" data-i="<?php echo $_ii; ?>">
                    <?php echo $view->getAttachmentImageHtml($image_src, 'thumbnail', 'rect-150-100', 'cover'); ?>
                    <!-- <img src="<?php echo $image_src; ?>" class="img-fluid" alt="<?php printf(__('Gallery image %d', 'hs'), $_ii); ?>" /> -->
                </div>
            </div>
            <?php endforeach; ?>
        </div>
        <?php endforeach; ?>
    </div>

    <?php endif; ?>

    <?php 
    $images_src = $args['property']->getGalleryImagesSrc('Thumbnail_1280x960');
    if($images_src): 
        wpseed_print_view('gallery-modal', [
            'images' => $images_src,
            'gallery_title' => $view->args['property']->getReference()
        ]);
    endif;
    ?>

</<?php echo $view->getViewTag(); ?>>
