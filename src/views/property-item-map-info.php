<<?php echo $view->getViewTag(); ?> class="<?php echo $view->_getHtmlClass(); ?>">

    <?php if($view->has_featured_image()): ?>
    <div class="property-image">
        <?php echo $view->getBgImageHtml($view->get_featured_image(), 'rect-150-100'); ?>
    </div>
    <?php endif; ?>

    <div class="property-info">
        <div class="row">
            <div class="col-7">
                <?php if($view->has_reference()): ?>
                <div class="reference"><?php echo $view->get_reference(); ?></div>
                <?php endif; ?>
                <?php if($view->has_location()): ?>
                <div class="location"><?php echo $view->get_location(); ?></div>
                <?php endif; ?>
            </div>
            <div class="col-5">
                <?php if($view->has_price_raw()): ?>
                <div class="price"><?php echo $view->get_price(); ?></div>
                <?php endif; ?>
            </div>
        </div>
    </div>

</<?php echo $view->getViewTag(); ?>>
