<<?php echo $view->getViewTag(); ?> class="<?php echo $view->_getHtmlClass(); ?>" 
    data-view="<?php echo $view->getName(); ?>"
    data-ref="<?php echo $view->get_reference(); ?>" 
    data-lat="<?php echo $view->get_gps_lat(); ?>" 
    data-lon="<?php echo $view->get_gps_lon(); ?>"
    >

    <div class="property-item-cont">
        
        <?php //if($view->has_gallery_images()): ?>
        <div class="property-gallery">
            <?php if($view->get_gallery_images_num() > 1): ?>
            <div class="swiper">
                <div class="swiper-wrapper">
                    <?php foreach($view->get_gallery_images() as $image): ?>
                    <div class="swiper-slide">
                        <?php //echo $view->getAttachmentImageHtml($image, 'full', 'rect-150-100', 'cover'); ?>
                        <div class="gallery-image bg-img-cover" style="<?php echo $image ? 'background-image: url(' .  $image . ')' : ''; ?>"></div>
                    </div>
                    <?php endforeach; ?>
                </div>
                <div class="swiper-button-prev"></div>
                <div class="swiper-button-next"></div>
            </div>
            <?php elseif($view->has_featured_image()): ?>
            <div class="property-image bg-img-cover" style="<?php echo 'background-image: url(' . $view->get_featured_image() . ');' ?>"></div>
            <?php else: ?>
            <div class="property-image no-image"></div>
            <?php endif; ?>
            
            <?php if($view->has_offer()): ?>
            <div class="deal-type">
                <span class="deal-tag"><?php echo $view->get_offer(); ?></span>
            </div>
            <?php endif; ?>
            <a href="<?php echo $args['property']->getLink(); ?>" class="property-link ns">&nbsp;</a>
        </div>
        <?php //endif; ?>
        
        <div class="property-info">
            <a href="<?php echo $args['property']->getLink(); ?>" class="property-link ns">
                <?php if($view->has_location()): ?>
                <div class="property-location  icon-before-abs">
                    <span class="location-info"><?php echo $view->get_location(); ?></span>
                    <?php if($view->has_reference()): ?>
                    <span class="location-ref-info"><?php echo $view->get_reference(); ?></span>
                    <?php endif; ?>
                </div>
                <?php endif; ?>
                <div class="property-specs">
                    <div class="row">
                        <div class="col-8">
                            <div class="specs-list">
                                <?php //foreach($view->get_specs() as $sk => $spec): ?>
                                <?php //endforeach; ?>
                                <?php if($view->has_area()): ?>
                                <span class="spec area"><?php echo $view->get_area(); ?></span>
                                <?php endif; ?>
                                <?php if($view->has_bedrooms()): ?>
                                <span class="spec icon-before bedrooms"><?php echo $view->get_bedrooms(); ?></span>
                                <?php endif; ?>
                                <?php if($view->has_bathrooms()): ?>
                                <span class="spec icon-before bathrooms"><?php echo $view->get_bathrooms(); ?></span>
                                <?php endif; ?>
                            </div>
                        </div>
                        <div class="col-4">
                            <?php if($view->has_price_raw()): ?>
                            <div class="price-info">
                                <span class="sale-price"><?php echo $view->get_price(); ?></span>
                            </div>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </a>
        </div>
        
    </div>

    <?php 
    wpseed_print_view('property-item-map-info', [
        'property' => $view->args['property']
    ]);
    ?>

</<?php echo $view->getViewTag(); ?>>
