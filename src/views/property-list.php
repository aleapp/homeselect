<?php if(!$view->has_render_items_only()): ?>

<<?php echo $view->getViewTag(); ?> class="<?php echo $view->_getHtmlClass(); ?>" 
    data-view="<?php echo $view->getName(); ?>" 
    data-map_cont="<?php echo $view->get_map_cont(); ?>" 
    data-map_zoom="<?php echo $view->get_map_zoom(); ?>" 
    >

    <?php echo $view->getAdminEditButton(); ?>

    <?php //echo $view->getContainerTagOpen(); ?>

    <div class="conts">
            <div class="list-cont">

                <?php 
                if($view->has_show_filters()):
                    wpseed_print_view('property-filters-advanced', [
                        'items_total' => $view->get_items_total()
                    ]);
                endif; 
                ?>

                <div class="list">
                    <div class="list-items">
<?php endif; ?>

                    <?php 
                    $col_class = 'col-md-' . (12/$view->get_cols_num());
                    if($view->has_items()): foreach($view->get_items() as $items_row): ?>
                    <div class="row g-0">
                        <?php foreach($items_row as $item): ?>
                        <div class="<?php echo $col_class; ?>">
                            <?php
                            wpseed_print_view('property-item', [
                                'property' => $item
                            ]);
                            ?>
                        </div>
                        <?php endforeach; ?>
                    </div>
                    <?php endforeach; elseif($view->has_no_found_text()): ?>
                    <div class="no-found ta-center"><p><?php echo $view->get_no_found_text(); ?></p></div>
                    <?php endif; ?>

<?php if(!$view->has_render_items_only()): ?>
                    </div><!-- .list-items -->

                    <?php wpseed_print_view('loader'); ?>

                </div><!-- .list -->

                <?php 
                if($view->has_show_list_pager()): 
                    wpseed_print_view('list-pager', wp_parse_args($view->args['list_pager_args'], [
                        'paged' => $view->args['q_args']['paged'],
                        'items_total' =>  $view->get_items_total(),
                        'items_per_page' => $view->args['q_args']['posts_per_page']
                    ]));
                endif; 
                ?>

            </div><!-- .list-cont -->

            <?php if($view->has_show_map()): ?>
            <div class="map-cont">
                <div class="map"></div>
                <div class="map-controls">
                    <button class="btn-map-expand"><span class="btn-label"><?php _e('Expand', 'hs'); ?></span></button>
                    <button class="btn-map-expand btn-alt"><span class="btn-label"><?php _e('Collapse', 'hs'); ?></span></button>
                </div>
            </div><!-- .map-cont -->
            <?php endif; ?>
            
        </div><!-- .conts -->

    <?php //echo $view->getContainerTagClose(); ?>

</<?php echo $view->getViewTag(); ?>>
<?php endif; ?>