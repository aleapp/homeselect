<<?php echo $view->getViewTag(); ?> id="<?php echo $view->getId(); ?>" class="<?php echo $view->_getHtmlClass(); ?>" 
    data-view="<?php echo $view->getName(); ?>" 
    data-center_lat="<?php echo $view->args['property']->getGpsLat(); ?>" 
    data-center_lon="<?php echo $view->args['property']->getGpsLon(); ?>" 
    data-zoom="<?php echo $view->get_zoom(); ?>"
    >

    <?php //echo $view->getAdminEditButton(); ?>

    <div class="map"></div>

    <?php if($view->has_show_area_info()): ?>
    <div class="area-info">
        <div class="info-name"><?php _e('The Area', 'hs'); ?></div>
        <div class="info-content">

            <?php if($view->has_area_display_title()): ?>
            <h4 class="title"><?php echo $view->get_area_display_title(); ?></h4>
            <?php endif; ?>

            <?php if($view->has_area_display_subtitle()): ?>
            <h5 class="subtitle"><?php echo $view->get_area_display_subtitle(); ?></h5>
            <?php endif; ?>

            <?php if($view->has_area_description()): ?>
            <div class="descripiton">
                <?php echo $view->get_area_description(); ?>
            </div>
            <?php endif; ?>
        </div>
    </div>
    <?php endif; ?>

</<?php echo $view->getViewTag(); ?>>
