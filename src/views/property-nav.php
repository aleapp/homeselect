<<?php echo $view->getViewTag(); ?> class="<?php echo $view->_getHtmlClass(); ?>" data-view="<?php echo $view->getName(); ?>">

    <div class="fixed-cont bg-grey-6">

        <?php if($view->has_back_link()): ?>
        <div class="back-link">
            <a href="<?php echo $view->get_back_link(); ?>">
                <i class="fas fa-arrow-left"></i>
                <span><?php echo $view->get_back_label(); ?></span>
            </a>
        </div>
        <?php endif; ?>

        <div class="container">
            <div class="">
                
            </div>
            <?php if($view->has_items()): ?>
            <ul class="nav-links ns">
                <?php foreach($view->get_items() as $item): 
                    $link = isset($item['link']) ? $item['link'] : '';
                    $label = isset($item['label']) ? $item['label'] : '';
                    ?>
                <li><a href="<?php echo $link; ?>"><?php echo $label; ?></a></li>
                <?php endforeach; ?>
            </ul>
            <?php endif; ?>
        </div>
    </div>

</<?php echo $view->getViewTag(); ?>>
