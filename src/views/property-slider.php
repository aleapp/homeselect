<<?php echo $view->getViewTag(); ?> class="<?php echo $view->_getHtmlClass(); ?>" data-view="<?php echo $view->getName(); ?>"
    data-show_pagination="<?php echo (int)$view->has_show_pagination(); ?>" 
    data-show_nav="<?php echo (int)$view->has_show_nav(); ?>" 
    >

    <?php echo $view->getAdminEditButton(); ?>
    
    <?php echo $view->getContainerTagOpen(); ?>
    
        <div class="property-slider-cont">
            <?php if($view->has_items()): ?>

            <div class="swiper property-slider">
                <div class="swiper-wrapper property-slides">
                    <?php foreach($view->get_items() as $item): ?>
                    <div class="swiper-slide">
                        <?php wpseed_print_view('property-item', ['property' => $item]); ?>
                    </div>
                    <?php endforeach; ?>
                </div>
            </div>
            
            <?php if($view->has_show_pagination()): ?>
            <div class="swiper-pagination"></div>
            <?php endif; ?>

            <?php if($view->has_show_nav()): ?>
            <div class="swiper-button-prev"></div>
            <div class="swiper-button-next"></div>
            <?php endif; ?>

            <?php endif; ?>
        </div>
        
    <?php echo $view->getContainerTagClose(); ?>

</<?php echo $view->getViewTag(); ?>>
