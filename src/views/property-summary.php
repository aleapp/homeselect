<<?php echo $view->getViewTag(); ?> id="<?php echo $view->getId(); ?>" class="<?php echo $view->_getHtmlClass(); ?>" data-view="<?php echo $view->getName(); ?>">

    <?php echo $view->getAdminEditButton(); ?>

    <div class="info-container">
        <div class="summary-info info-1">
            <?php if($info = $args['property']->getAddressFormatted()): ?>
            <span class="info-item address has-icon tc-blue-1"><?php echo $info; ?></span>
            <?php endif; ?>
            <?php if($info = $args['property']->getReference()): ?>
            <span class="info-item reference"><?php echo sprintf(__('REF: %s', 'hs'), $info); ?></span>
            <?php endif; ?>
        </div>
        <div class="summary-info info-2">
            <?php if($args['property']->getArea()): ?>
            <span class="info-item net-area"><?php echo $args['property']->getArea(true); ?></span>
            <?php endif; ?>
            <?php if($info = $args['property']->getBedrooms()): ?>
            <span class="info-item bedrooms has-icon"><?php echo $info; ?></span>
            <?php endif; ?>
            <?php if($info = $args['property']->getBathrooms()): ?>
            <span class="info-item bathrooms has-icon"><?php echo $info; ?></span>
            <?php endif; ?>
        </div>
    </div>

</<?php echo $view->getViewTag(); ?>>
