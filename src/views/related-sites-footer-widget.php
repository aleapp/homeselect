<<?php echo $view->getViewTag(); ?> class="<?php echo $view->_getHtmlClass(); ?>">

    <?php echo $view->getAdminEditButton(); ?>

    <?php echo $view->getContainerTagOpen(); ?>

    <?php if($view->has_related_sites()): ?>

        <div class="related-sites">
            <div class="row">
            <?php foreach($view->get_related_sites() as $i => $related_site): 
                $_i = $i+1;
                $site_num_class = ($_i == 1) ? 'site-first' : (($_i == $view->get_related_sites_num()) ? 'site-last' : '');
                ?>
                <div class="col-lg-<?php echo 12/$view->get_related_sites_num(); ?>">
                    <div class="related-site <?php echo $site_num_class; ?>">
                        <a href="<?php echo $related_site['site_url']; ?>" target="<?php echo $related_site['site_url_target']; ?>" class="related-site-link">
                            <?php echo $related_site['site_name']; ?>
                        </a>
                    </div>
                </div>
            <?php endforeach; ?>
            </div>
        </div>

    <?php endif; ?>

    <?php 
    $link_url = $view->has_more_url() ? $view->get_more_url() : ($view->has_more_page() ? get_permalink($view->get_more_page()) : '');
    if($link_url): ?>
    <div class="learn-more">
        <div class="cross-line"></div>
        <a href="<?php echo $link_url; ?>" class="learn-more-link bg-7">
            <?php echo $view->get_more_label(); ?>
        </a>
    </div>
    <?php endif; ?>

    <?php echo $view->getContainerTagClose(); ?>
        
</<?php echo $view->getViewTag(); ?>>
