<<?php echo $view->getViewTag(); ?> class="<?php echo $view->_getHtmlClass(); ?>">

    <?php echo $view->getAdminEditButton(); ?>

    <?php echo $view->getContainerTagOpen(); ?>

        <?php 
        if($view->has_title()):
            wpseed_print_view('title', [
                'title' => $view->get_title(),
                // 'headline_text' => $view->get_description(),
                'alignment' => 'center',
                'variant' => 'section-title',
                'h_type' => 'h2'
            ]);
        endif;
        ?>

        <?php if($view->has_description()): ?>
        <div class="description">
            <?php echo wpautop($view->get_description()); ?>
        </div>
        <?php endif; ?>

        <?php if($view->has_items()): ?>
        <div class="items">
            <?php 
            $col_class = 'col-lg-' . (12/$view->get_cols_num());
            $gx = ($view->get_cols_num() === 2) ? ' gx-5' : '';
            foreach($view->get_items() as $items_row): ?>
            <div class="row<?php echo $gx; ?>">
                <?php foreach($items_row as $item): ?>
                <div class="<?php echo $col_class; ?>">
                    <?php 
                    wpseed_print_view('service-type-1', array_merge([
                        'variant' => $view->get_variant(),
                        'align' => $view->get_align_items_content(),
                        'description_color' => $view->get_items_description_color()
                    ], $item));
                    ?>
                </div>
                <?php endforeach; ?>
            </div>
            <?php endforeach; ?>
        </div>
        <?php endif; ?>

        <?php if($view->has_more_url() && $view->has_more_label()): ?>
        <div class="more-info ta-center">
            <a class="btn-t btn-<?php echo $view->get_more_bg_color(); ?>" href="<?php echo $view->get_more_url(); ?>"><?php echo $view->get_more_label(); ?></a>
        </div>
        <?php endif; ?>

    <?php echo $view->getContainerTagClose(); ?>

</<?php echo $view->getViewTag(); ?>>
