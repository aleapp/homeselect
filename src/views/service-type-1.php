<<?php echo $view->getViewTag(); ?> class="<?php echo $view->_getHtmlClass(); ?>">

    <?php echo $view->getContainerTagOpen(); ?>

        <div class="service-inner">

            <?php if($view->has_image_html()): ?>
            <div class="service-image">
                <?php echo $view->get_image_html(); ?>
            </div>
            <?php endif; ?>

            <?php if($view->has_title()): ?>
            <h3 class="service-title"><?php echo $view->get_title(); ?></h3>
            <?php endif; ?>

            <?php if($view->has_description()): ?>
            <div class="service-description tc-<?php echo $view->get_description_color(); ?>">
                <?php echo wpautop($view->get_description()); ?>
            </div>
            <?php endif; ?>

            <?php if($view->has_more_url()): ?>
            <div class="service-more">
                <a href="<?php echo $view->get_more_url(); ?>" class="more-link"><?php _e('View more', 'hs'); ?></a>
            </div>
            <?php endif; ?>

        </div>

    <?php echo $view->getContainerTagClose(); ?>

</<?php echo $view->getViewTag(); ?>>
