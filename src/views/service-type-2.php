<<?php echo $view->getViewTag(); ?> class="<?php echo $view->_getHtmlClass('px-0'); ?>">

    <?php echo $view->getAdminEditButton(); ?>

    <div class="container-fluid g-0">

        <div class="row g-0">

        <?php if($view->get_image_pos() == 'right'): ?>

            <div class="offset-lg-1 col-lg-4">
                <div class="service-content">

                    <?php if($view->has_image_src()): ?>
                    <div class="service-image-mob bg-img-cover abs-full" style="background-image: url(<?php echo $view->get_image_src(); ?>);"></div>
                    <?php endif; ?>

                    <div class="content-inner">
                        <?php if($view->has_title()): ?>
                        <h3 class="title"><?php echo $view->get_title(); ?></h3>
                        <?php endif; ?>

                        <?php if($view->has_description()): ?>
                        <div class="description"><?php echo wpautop($view->get_description()); ?></div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
            <div class="col-lg-7">
                <?php if($view->has_image_src()): ?>
                <div class="service-image bg-img-cover h-full" style="background-image: url(<?php echo $view->get_image_src(); ?>);"></div>
                <?php endif; ?>
            </div>

        <?php elseif($view->get_image_pos() == 'left'): ?>

            <div class="col-lg-7">
                <?php if($view->has_image_src()): ?>
                <div class="service-image bg-img-cover h-full" style="background-image: url(<?php echo $view->get_image_src(); ?>);"></div>
                <?php endif; ?>
            </div>
            <div class="col-lg-4">
                <div class="service-content">

                    <?php if($view->has_image_src()): ?>
                    <div class="service-image-mob bg-img-cover abs-full" style="background-image: url(<?php echo $view->get_image_src(); ?>);"></div>
                    <?php endif; ?>

                    <div class="content-inner">
                        <?php if($view->has_title()): ?>
                        <h3 class="title"><?php echo $view->get_title(); ?></h3>
                        <?php endif; ?>

                        <?php if($view->has_description()): ?>
                        <div class="description"><?php echo wpautop($view->get_description()); ?></div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
            
        <?php endif; ?>

        </div>


    </div>

</<?php echo $view->getViewTag(); ?>>
