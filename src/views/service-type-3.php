<<?php echo $view->getViewTag(); ?> class="<?php echo $view->_getHtmlClass(); ?>">

    <?php echo $view->getContainerTagOpen(); ?>

        <?php if($view->has_link()): ?>
        <a href="<?php echo $view->get_link(); ?>" class="service-inner">
        <?php else: ?>
        <div class="service-inner">
        <?php endif; ?>

            <?php echo $view->get_image_html(); ?>
            <?php if($view->has_title()): ?>
            <h4 class="title"><?php echo $view->get_title(); ?></h4>
            <?php endif; ?>

        <?php if($view->has_link()): ?>
        </a>
        <?php else: ?>
        </div>
        <?php endif; ?>

    <?php echo $view->getContainerTagClose(); ?>

</<?php echo $view->getViewTag(); ?>>
