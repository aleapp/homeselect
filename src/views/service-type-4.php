<<?php echo $view->getViewTag(); ?> class="<?php echo $view->_getHtmlClass(); ?>">

    <?php echo $view->getAdminEditButton(); ?>

    <?php echo $view->getContainerTagOpen(); ?>

        <?php if($view->has_link()): ?>
        <a href="<?php echo $view->get_link(); ?>" class="service-inner">
        <?php else: ?>
        <div class="service-inner">
        <?php endif; ?>

            <?php if($view->has_counter_num()): ?>
            <div class="counter-num ta-center">
                <span><?php echo ($view->get_counter_num() < 10) ? '0' . $view->get_counter_num() : $view->get_counter_num(); ?></span>
            </div>
            <?php endif; ?>

            <?php if($view->has_title()): ?>
            <h4 class="title ta-center"><?php echo nl2br($view->get_title()); ?></h4>
            <?php endif; ?>

            <?php if($view->has_description()): ?>
            <div class="description ta-center">
                <?php echo wpautop($view->get_description()); ?>
            </div>
            <?php endif; ?>

        <?php if($view->has_link()): ?>
        </a>
        <?php else: ?>
        </div>
        <?php endif; ?>

    <?php echo $view->getContainerTagClose(); ?>

</<?php echo $view->getViewTag(); ?>>
