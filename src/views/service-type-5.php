<<?php echo $view->getViewTag(); ?> class="<?php echo $view->_getHtmlClass(); ?>">

    <?php echo $view->getAdminEditButton(); ?>

    <?php echo $view->getContainerTagOpen(); ?>

        <?php 
        if($view->has_title()):
            wpseed_print_view('title', [
                'title' => $view->get_title(),
                'alignment' => 'center',
                'variant' => 'section-title',
                'h_type' => 'h2'
            ]);
        endif;
        ?>

        <?php if($view->has_description()): ?>
        <div class="service-description">
            <?php echo wpautop($view->get_description()); ?>
        </div>
        <?php endif; ?>

        <?php if($view->has_image_html()): ?>
        <div class="service-image">
            <?php echo wpautop($view->get_image_html()); ?>
        </div>
        <?php endif; ?>

        <?php if($view->has_tagline()): ?>
        <h3 class="service-tagline"><?php echo nl2br($view->get_tagline()); ?></h3>
        <?php endif; ?>

        <?php if($view->has_more_url() && $view->has_more_label()): ?>
        <div class="service-more">
            <?php
            wpseed_print_view('cta-button', [
                'label' => $view->get_more_label(),
                'url' => $view->get_more_url(),
                'url_target' => $view->get_more_url_target(),
                'size' => 'large',
                'color' => 'dark-blue',
                'align'
            ]);
            ?>
        </div>
        <?php endif; ?>

    <?php echo $view->getContainerTagClose(); ?>

</<?php echo $view->getViewTag(); ?>>
