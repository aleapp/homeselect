<<?php echo $view->getViewTag(); ?> class="<?php echo $view->_getHtmlClass('bg-grey-4'); ?>">

    <?php echo $view->getAdminEditButton(); ?>

    <?php echo $view->getContainerTagOpen(); ?>

        <?php 
        if($view->has_title()):
            wpseed_print_view('title', [
                'title' => $view->get_title(),
                'alignment' => 'center',
                'variant' => 'section-title',
                'h_type' => 'h2'
            ]);
        endif;
        ?>

        <?php if($view->has_description()): ?>
        <div class="group-description"><?php echo wpautop($view->get_description()); ?></div>
        <?php endif; ?>

        <?php if($view->has_items()): ?>
        <div class="items">
            <?php 
            $col_class = 'col-lg-' . (12/$view->get_cols_num());
            foreach($view->get_items() as $items_row): ?>
            <div class="row">
                <?php foreach($items_row as $item): ?>
                <div class="<?php echo $col_class; ?>">
                    <?php wpseed_print_view('service-type-6', $item); ?>
                </div>
                <?php endforeach; ?>
            </div>
            <?php endforeach; ?>
        </div>
        <?php endif; ?>

    <?php echo $view->getContainerTagClose(); ?>

</<?php echo $view->getViewTag(); ?>>
