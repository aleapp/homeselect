<<?php echo $view->getViewTag(); ?> class="<?php echo $view->_getHtmlClass(); ?>">

    <?php echo $view->getContainerTagOpen(); ?>

        <?php if($view->has_image_html()): ?>
        <div class="service-image"><?php echo $view->get_image_html(); ?></div>
        <?php endif; ?>

        <?php if($view->has_title()): ?>
        <h4 class="service-title"><?php echo $view->get_title(); ?></h4>
        <?php endif; ?>

        <?php if($view->has_description()): ?>
        <div class="service-description"><?php echo wpautop($view->get_description()); ?></div>
        <?php endif; ?>

    <?php echo $view->getContainerTagClose(); ?>

</<?php echo $view->getViewTag(); ?>>
