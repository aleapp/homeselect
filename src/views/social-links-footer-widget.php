<<?php echo $view->getViewTag(); ?> class="<?php echo $view->_getHtmlClass(); ?>">

    <?php echo $view->getAdminEditButton(); ?>

    <?php echo $view->getContainerTagOpen(); ?>

    <?php if($view->has_links()): ?>

        <ul class="social-links ns">
            <?php foreach($view->get_links() as $i => $link): ?>
                <li class="item-<?php echo $link['type']; ?>">
                    <a href="<?php echo $link['url']; ?>" class="social-link <?php echo $link['type']; ?>" target="_blank"></a>
                </li>
            <?php endforeach; ?>
        </ul>

    <?php endif; ?>

    <?php echo $view->getContainerTagClose(); ?>
        
</<?php echo $view->getViewTag(); ?>>
