<<?php echo $view->getViewTag(); ?> class="<?php echo $view->_getHtmlClass(); ?>">

    <?php echo $view->getAdminEditButton(); ?>

    <?php echo $view->getContainerTagOpen(); ?>

        <?php 
        if($view->has_title()):
            wpseed_print_view('title', [
                'pre_title' => $view->get_pre_title(),
                'title' => $view->get_title(),
                // 'headline_text' => $view->get_description(),
                'alignment' => 'center',
                'variant' => 'section-title',
                'h_type' => 'h2'
            ]);
        endif;
        ?>

        <?php if($view->has_items()): ?>
        <div class="step-items cont-narrow">
            <?php 
            foreach($view->get_items() as $i => $item): 
                $_i = $i+1;
                ?>
            <div class="step-item">
                <div class="row g-0">
                    <div class="col-4">
                        <div class="step-label">
                            <?php 
                            if(!empty($item['label']))
                            {
                                echo $item['label'];
                            }
                            ?>
                        </div>
                    </div>
                    <div class="col-8">
                        <div class="step-description">
                            <span class="step-num"><?php echo $_i; ?></span>

                            <?php 
                            if(!empty($item['description']))
                            {
                                echo wpautop($item['description']);
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
            <?php endforeach; ?>
        </div>
        <?php endif; ?>

    <?php echo $view->getContainerTagClose(); ?>

</<?php echo $view->getViewTag(); ?>>
