<<?php echo $view->getViewTag(); ?> class="<?php echo $view->_getHtmlClass(); ?>">

    <?php echo $view->getAdminEditButton(); ?>

    <?php echo $view->getContainerTagOpen(); ?>

        <?php 
        if($view->has_title()):
            wpseed_print_view('title', [
                'title' => $view->get_title(),
                'alignment' => 'center',
                'variant' => 'section-title',
                'h_type' => 'h2'
            ]);
        endif;
        ?>

        <?php if($view->has_items()): ?>
        <div class="items">
            <?php 
            $col_class = 'col-lg-' . (12/$view->get_cols_num());
            foreach($view->get_items() as $items_row): ?>
            <div class="row">
                <?php foreach($items_row as $item): ?>
                <div class="<?php echo $col_class; ?>">
                    <?php 
                    wpseed_print_view('testimonial', [
                        'customer_image_id' => isset($item['customer_image']) ? $item['customer_image'] : 0,
                        'customer_name' => isset($item['customer_name']) ? $item['customer_name'] : '',
                        'customer_title' => isset($item['customer_title']) ? $item['customer_title'] : '',
                        'customer_quote' => isset($item['customer_quote']) ? $item['customer_quote'] : ''
                    ]);
                    ?>
                </div>
                <?php endforeach; ?>
            </div>
            <?php endforeach; ?>
        </div>
        <?php endif; ?>

    <?php echo $view->getContainerTagClose(); ?>

</<?php echo $view->getViewTag(); ?>>
