<<?php echo $view->getViewTag(); ?> class="<?php echo $view->_getHtmlClass(); ?>">

    <?php echo $view->getContainerTagOpen(); ?>

        <div class="testimonial-inner">

            <?php if($view->has_customer_quote()): ?>
            <div class="customer-quote">
                <?php echo wpautop($view->get_customer_quote()); ?>
            </div>
            <?php endif; ?>

            <div class="row g-0">
                <div class="col-2">
                    <?php if($view->has_customer_image_html()): ?>
                    <div class="customer-image">
                        <?php echo $view->get_customer_image_html(); ?>
                    </div>
                    <?php endif; ?>
                </div>
                <div class="col-10">
                    <?php if($view->has_customer_name()): ?>
                    <div class="customer-name">
                        <span><?php echo $view->get_customer_name(); ?></span>
                    </div>
                    <?php endif; ?>
                    <?php if($view->has_customer_title()): ?>
                    <div class="customer-title">
                        <span><?php echo $view->get_customer_title(); ?></span>
                    </div>
                    <?php endif; ?>
                </div>
            </div>

        </div>

    <?php echo $view->getContainerTagClose(); ?>

</<?php echo $view->getViewTag(); ?>>
