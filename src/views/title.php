<<?php echo $view->getViewTag(); ?> class="<?php echo $view->_getHtmlClass(); ?>">

    <?php echo $view->getAdminEditButton(); ?>

    <?php echo $view->getContainerTagOpen(); ?>

        <div class="cont-narrow">
            <?php if($view->has_pre_title()): ?>
            <p class="pre-title">
                <?php echo nl2br($view->get_pre_title()); ?>
            </p>
            <?php endif; ?>

            <?php if($view->has_title()): ?>
            <<?php echo $view->get_h_type(); ?> class="title-tag">
                <?php echo nl2br($view->get_title()); ?>
            </<?php echo $view->get_h_type(); ?>>
            <?php endif; ?>

            <?php if($view->has_headline_text()): ?>
            <div class="headline-text">
                <?php echo wpautop($view->get_headline_text()); ?>
            </div>
            <?php endif; ?>
        </div>

    <?php echo $view->getContainerTagClose(); ?>

</<?php echo $view->getViewTag(); ?>>
