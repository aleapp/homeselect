<<?php echo $view->getViewTag(); ?> class="<?php echo $view->_getHtmlClass(); ?>">

    <?php echo $view->getAdminEditButton(); ?>

    <?php echo $view->getContainerTagOpen(); ?>

    <div class="table-cont">
        <?php 
        $num_cols = $view->get_num_cols();
        if($num_cols): ?>
        <table>
            
            <?php 
            $header_row = $view->get_header_row();
            if(!empty($header_row)): ?>
            <thead>
                <tr>
                    <?php for($i=0; $i < $num_cols; $i++): 
                        $has_value = (isset($header_row[$i]) && $header_row[$i]['label'] !== '');
                        $cell_classes = ['tw-bold'];
                        if(!$has_value)
                        {
                            $cell_classes[] = 'empty-cell';
                        }
                        ?>
                    <th class="<?php echo implode(' ', $cell_classes); ?>"><?php echo $has_value ? $header_row[$i]['label'] : ''; ?></th>
                    <?php endfor; ?>
                </tr>
            </thead>
            <?php endif; ?>
            
            <tbody>
            <?php 
            foreach($view->get_data_rows() as $data_row): 
                if(isset($data_row['data_row'])): 
                    $_data_row = $data_row['data_row'];
                    ?>
                <tr>
                    <?php 
                    for($i=0; $i < $num_cols; $i++): 
                        
                        $data_value = isset($_data_row[$i]) ? (!empty($_data_row[$i]['data_check']) ? '<span class="value-check">;</span>' : $_data_row[$i]['data_value']) : '';

                        $cell_classes = ['c-' . $i];
                        // if(empty($data_value))
                        // {
                        //     $cell_classes[] = 'empty-cell';
                        // }
                        if(isset($header_row[$i]['bg_color']))
                        {
                            $cell_classes[] = 'bg-' . $header_row[$i]['bg_color'];
                        }
                        ?>
                    <td class="<?php echo implode(' ', $cell_classes); ?>">
                        <?php echo $data_value; ?>
                    </td>
                    <?php endfor; ?>
                </tr>
            <?php 
                endif;
            endforeach; ?>
            </tbody>
        </table>
        <?php endif; ?>
    </div>

    <?php echo $view->getContainerTagClose(); ?>

</<?php echo $view->getViewTag(); ?>>
