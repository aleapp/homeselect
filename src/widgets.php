<?php

add_action('widgets_init', 'hs_register_widgets');

function hs_register_widgets()
{
    register_sidebar([
        'id' => 'footer-widgets-1',
        'name' => __('Footer #1', 'hs'),
        'before_widget' => '<div id="%1$s" class="%2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h4 class="widget-title">',
        'after_title' => '</h4>'
    ]);
    register_sidebar([
        'id' => 'footer-widgets-2',
        'name' => __('Footer #2', 'hs'),
        'before_widget' => '<div id="%1$s" class="%2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h4 class="widget-title">',
        'after_title' => '</h4>'
    ]);
    register_sidebar([
        'id' => 'footer-widgets-3',
        'name' => __('Footer #3', 'hs'),
        'before_widget' => '<div id="%1$s" class="%2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h4 class="widget-title">',
        'after_title' => '</h4>'
    ]);
    register_sidebar([
        'id' => 'footer-widgets-4',
        'name' => __('Footer #4', 'hs'),
        'before_widget' => '<div id="%1$s" class="%2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h4 class="widget-title">',
        'after_title' => '</h4>'
    ]);
    register_sidebar([
        'id' => 'footer-widgets-5',
        'name' => __('Footer #5', 'hs'),
        'before_widget' => '<div id="%1$s" class="%2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h4 class="widget-title">',
        'after_title' => '</h4>'
    ]);
    register_sidebar([
        'id' => 'footer-widgets-6',
        'name' => __('Footer #6', 'hs'),
        'before_widget' => '<div id="%1$s" class="%2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h4 class="widget-title">',
        'after_title' => '</h4>'
    ]);
    register_sidebar([
        'id' => 'footer-widgets-bottom-1-1',
        'name' => __('Footer Bottom 1 - 1', 'hs'),
        'before_widget' => '<div id="%1$s" class="%2$s">',
        'after_widget' => '</div>',
        'before_title' => '',
        'after_title' => ''
    ]);
    register_sidebar([
        'id' => 'footer-widgets-bottom-1-2',
        'name' => __('Footer Bottom 1 - 2', 'hs'),
        'before_widget' => '<div id="%1$s" class="%2$s">',
        'after_widget' => '</div>',
        'before_title' => '',
        'after_title' => ''
    ]);
    register_sidebar([
        'id' => 'footer-widgets-bottom-1-3',
        'name' => __('Footer Bottom 1 - 3', 'hs'),
        'before_widget' => '<div id="%1$s" class="%2$s">',
        'after_widget' => '</div>',
        'before_title' => '',
        'after_title' => ''
    ]);
    register_sidebar([
        'id' => 'footer-widgets-bottom-2-1',
        'name' => __('Footer Bottom 2 - 1', 'hs'),
        'before_widget' => '<div id="%1$s" class="%2$s">',
        'after_widget' => '</div>',
        'before_title' => '',
        'after_title' => ''
    ]);
    register_sidebar([
        'id' => 'footer-widgets-bottom-2-2',
        'name' => __('Footer Bottom 2 - 2', 'hs'),
        'before_widget' => '<div id="%1$s" class="%2$s">',
        'after_widget' => '</div>',
        'before_title' => '',
        'after_title' => ''
    ]);
    // register_sidebar([
    //     'id' => 'footer-widgets-bottom-2-3',
    //     'name' => __('Footer Bottom 2 - 3', 'hs'),
    //     'before_widget' => '<div id="%1$s" class="%2$s">',
    //     'after_widget' => '</div>',
    //     'before_title' => '',
    //     'after_title' => ''
    // ]);
}
