<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInita709df215949d853145810d42c5d6d78
{
    public static $prefixLengthsPsr4 = array (
        'H' => 
        array (
            'HS\\' => 3,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'HS\\' => 
        array (
            0 => __DIR__ . '/../..' . '/src/classes',
        ),
    );

    public static $classMap = array (
        'Composer\\InstalledVersions' => __DIR__ . '/..' . '/composer/InstalledVersions.php',
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInita709df215949d853145810d42c5d6d78::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInita709df215949d853145810d42c5d6d78::$prefixDirsPsr4;
            $loader->classMap = ComposerStaticInita709df215949d853145810d42c5d6d78::$classMap;

        }, null, ClassLoader::class);
    }
}
