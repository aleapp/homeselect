<?php return array(
    'root' => array(
        'pretty_version' => '1.4.0',
        'version' => '1.4.0.0',
        'type' => 'library',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'reference' => NULL,
        'name' => 'hs/wp-theme',
        'dev' => true,
    ),
    'versions' => array(
        'hs/wp-theme' => array(
            'pretty_version' => '1.4.0',
            'version' => '1.4.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'reference' => NULL,
            'dev_requirement' => false,
        ),
    ),
);
